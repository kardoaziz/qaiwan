<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Title
    |--------------------------------------------------------------------------
    |
    | Here you can change the default title of your admin panel.
    |
    | For more detailed instructions you can look here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/#61-title
    |
    */

    'title' => 'بەڕێوەبەرایەتی',
    'title_prefix' => '',
    'title_postfix' => '',

    /*
    |--------------------------------------------------------------------------
    | Favicon
    |--------------------------------------------------------------------------
    |
    | Here you can activate the favicon.
    |
    | For more detailed instructions you can look here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/#62-favicon
    |
    */

    'use_ico_only' => false,
    'use_full_favicon' => false,

    /*
    |--------------------------------------------------------------------------
    | Logo
    |--------------------------------------------------------------------------
    |
    | Here you can change the logo of your admin panel.
    |
    | For more detailed instructions you can look here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/#63-logo
    |
    */

    'logo' => 'بەڕێوەبەرایەتی',
    'logo_img' => 'img/logo_small.png',
    'logo_img_class' => 'brand-image-xl',
    'logo_img_xl' => null,
    'logo_img_xl_class' => 'brand-image-xs',
    'logo_img_alt' => 'AdminLTE',

    /*
    |--------------------------------------------------------------------------
    | Layout
    |--------------------------------------------------------------------------
    |
    | Here we change the layout of your admin panel.
    |
    | For more detailed instructions you can look here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/#64-layout
    |
    */

    'layout_topnav' => null,
    'layout_boxed' => null,
    'layout_fixed_sidebar' => null,
    'layout_fixed_navbar' => null,
    'layout_fixed_footer' => null,

    /*
    |--------------------------------------------------------------------------
    | Extra Classes
    |--------------------------------------------------------------------------
    |
    | Here you can change the look and behavior of the admin panel.
    |
    | For more detailed instructions you can look here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/#65-classes
    |
    */

    'classes_body' => '',
    'classes_brand' => '',
    'classes_brand_text' => '',
    'classes_content_header' => 'container-fluid',
    'classes_content' => 'container-fluid',
    'classes_sidebar' => 'sidebar-dark-primary ',
    'classes_sidebar_nav' => 'sidebar-collapse',
    'classes_topnav' => 'navbar-white navbar-light',
    'classes_topnav_nav' => 'navbar-expand-md',
    'classes_topnav_container' => 'container',

    /*
    |--------------------------------------------------------------------------
    | Sidebar
    |--------------------------------------------------------------------------
    |
    | Here we can modify the sidebar of the admin panel.
    |
    | For more detailed instructions you can look here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/#66-sidebar
    |
    */

    'sidebar_mini' => true,
    'sidebar_collapse' => false,
    'sidebar_collapse_auto_size' => false,
    'sidebar_collapse_remember' => false,
    'sidebar_collapse_remember_no_transition' => true,
    'sidebar_scrollbar_theme' => 'os-theme-light',
    'sidebar_scrollbar_auto_hide' => 'l',
    'sidebar_nav_accordion' => true,
    'sidebar_nav_animation_speed' => 300,

    /*
    |--------------------------------------------------------------------------
    | Control Sidebar (Right Sidebar)
    |--------------------------------------------------------------------------
    |
    | Here we can modify the right sidebar aka control sidebar of the admin panel.
    |
    | For more detailed instructions you can look here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/#67-control-sidebar-right-sidebar
    |
    */

    'right_sidebar' => false,
    'right_sidebar_icon' => 'fas fa-cogs',
    'right_sidebar_theme' => 'dark',
    'right_sidebar_slide' => true,
    'right_sidebar_push' => true,
    'right_sidebar_scrollbar_theme' => 'os-theme-light',
    'right_sidebar_scrollbar_auto_hide' => 'l',

    /*
    |--------------------------------------------------------------------------
    | URLs
    |--------------------------------------------------------------------------
    |
    | Here we can modify the url settings of the admin panel.
    |
    | For more detailed instructions you can look here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/#68-urls
    |
    */

    'use_route_url' => false,

    'dashboard_url' => '/',

    'logout_url' => 'logout',

    'login_url' => 'login',

    'register_url' => 'register',

    'password_reset_url' => 'password/reset',

    'password_email_url' => 'password/email',

    /*
    |--------------------------------------------------------------------------
    | Laravel Mix
    |--------------------------------------------------------------------------
    |
    | Here we can enable the Laravel Mix option for the admin panel.
    |
    | For more detailed instructions you can look here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/#69-laravel-mix
    |
    */

    'enabled_laravel_mix' => false,

    /*
    |--------------------------------------------------------------------------
    | Menu Items
    |--------------------------------------------------------------------------
    |
    | Here we can modify the sidebar/top navigation of the admin panel.
    |
    | For more detailed instructions you can look here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/#610-menu
    |
    */

    'menu' => [

        [
            'text'        => 'پەڕەی سەرەکی',
            'url'         => '/home',
            'icon'        => 'fa fa-fw fa-home',
        ],
        // [
        //     'text' => 'شۆفێر',
        //     'url'  => 'drivers',
        //     'icon' => 'fas fa-fw fa-male',
        // ],
        // [
        //     'text' => 'بارهەڵگر',
        //     'url'  => 'trucks',
        //     'icon' => 'fas fa-fw fa-truck',
        // ],
        [
            'text' => 'بارهەڵگرەکان',
            'url'  => 'trucks',
            'icon' => 'fas fa-fw fa-truck',
            'submenu' => [
                [
                    'text' => 'بارهەڵگر',
                    'url'  => 'trucks',
                    'icon' => 'fas fa-fw fa-truck',
                    'can' => 'view_truck',
                ],
                [
                    'text' => 'شۆفێر',
                    'url'  => 'drivers',
                    'icon' => 'fas fa-fw fa-male',
                    'can' => 'view_driver',
                ],
                [
                    'text' => 'شۆفێری بارهەڵگر',
                    'url'  => 'drives',
                    'icon' => 'fas fa-fw  fa-road',
                    'can' => 'view_assign_truck',
                ]
            ],
        ],

        // [
        //     'text' => 'شۆفێری بارهەڵگر',
        //     'url'  => 'drives',
        //     'icon' => 'fas fa-fw  fa-road',
        // ],
        [
            'text' => 'حسابات',
            'url'  => 'accounting',
            'icon' => 'fa fa-fw  fa-calculator',
            'can' => 'view_accounting',
        ],
        [
            'text' => 'پارەدان بە شۆفێر',
            'url'  => 'drive_payments',
            'icon' => 'fas fa-fw  fa-credit-card',
            'can' => 'view_payment',
        ],
        [
            'text' => 'پارچە',
            'url'  => 'items',
            'icon' => 'fas fa-fw fa-wrench',
            'submenu' =>[
                [
                    'text' => 'فرۆشیار',
                    'url'  => 'suppliers',
                    'icon' => 'fas fa-fw fa-cart-arrow-down',
                    'can' => 'view_supplier',
                ],
                [
                    'text' => 'پارچە',
                    'url'  => 'items',
                    'icon' => 'fas fa-fw fa-wrench',
                    'can' => 'view_item',
                ],
                [
                    'text' => 'جۆری پارچە',
                    'url'  => 'itemModel',
                    'icon' => 'fas fa-fw fa-list-alt',
                ],
                [
                    'text' => 'قیاسی پارچە',
                    'url'  => 'itemSize',
                    'icon' => 'fas fa-fw fa-dot-circle',
                ],
                 [
                    'text' => 'گەنجینە',
                    'url'  => 'warehouses',
                    'icon' => 'fas fa-fw fa-building',
                    'can' => 'view_warehouse',
                ],
                 [
                    'text' => 'کرینی پارچە',
                    'url'  => 'purchases',
                    'icon' => 'fas fa-fw  fa-cart-arrow-down',
                    'can' => 'view_buy_item',
                ]
            ]
        ],
        // [
        //     'text' => 'پارچە',
        //     'url'  => 'items',
        //     'icon' => 'fas fa-fw fa-wrench',
        // ],
        // [
        //     'text' => 'گەنجینە',
        //     'url'  => 'warehouses',
        //     'icon' => 'fas fa-fw fa-building',
        // ],
        // [
        //     'text' => 'کرینی پارچە',
        //     'url'  => 'purchases',
        //     'icon' => 'fas fa-fw  fa-cart-arrow-down',
        // ],
        [
            'text' => 'ڕێکخستنەکان',
            'url'  => 'services',
            'icon' => 'fas fa-fw fa-cog',
            'submenu'=>[
                    [
                        'text' => 'شوێنەکان',
                        'url'  => 'locations',
                        'icon' => 'fas fa-fw fa-road',
                        'can' => 'view_location',
                    ],
                     [
                        'text' => 'نرخی گواستنەوە',
                        'url'  => 'locationCost',
                        'icon' => 'fas fa-fw fa-money-bill-alt',
                        'can' => 'view_trip_cost',
                    ],
                    [
                        'text' => 'نرخی سزا',
                        'url'  => 'fines',
                        'icon' => 'fa fa-fw fa-ticket',
                        'can' => 'view_fine_price',
                    ],
                    [
                        'text' => 'جۆری خەرجی',
                        'url'  => 'expense_type',
                        'icon' => 'fas fa-fw fa-rocket',
                        'can' => 'view_expense_type',
                    ],[
                        'text' => 'بەرهەمەکان',
                        'url'  => 'materials',
                        'icon' => 'fas fa-fw fa-anchor',
                        'can' => 'view_material',
                    ],
                    [
                        'text' => 'نرخی دۆلار',
                        'url'  => 'dollars',
                        'icon' => 'fas fa-fw fa-anchor',
                        'can' => 'view_dollar',
                    ],

            ]
        ],
        [
            'text' => 'خەرجی',
            'url'  => 'expenses',
            'icon' => 'fas fa-fw fa-money-bill-alt',
            'can' => 'view_expense',
        ],
        [
            'text' => 'خزمەتگوزاری',
            'url'  => 'services',
            'icon' => 'fa fa-fw  fa-magnet',
            'can' => 'view_service',
        ],
        [
            'text' => 'خزمەتگوزاری پرۆژەکان',
            'url'  => 'projects',
            'icon' => 'fa fa-fw  fa-magnet',
            'can' => 'view_project_service',
        ],
        [
            'text' => 'پشکنین',
            'url'  => 'checks',
            'icon' => 'fa fa-fw  fa-heartbeat',
            'can' => 'view_check',
        ],

        [
            'text' => 'بارەکان',
            'url'  => 'trips',
            'icon' => 'fas fa-fw fa-road',
            'can' => 'view_trip',
        ],
        [
            'text' => 'سندوق',
            'url'  => '#',
            'icon' => 'fas fa-university',
            'can' => 'view_snduq',
            'submenu' =>[
                [
                    'text' => 'سندوق',
                    'icon' => 'fas fa-university',
                    'route' => 'snduq.index',
                    'can' =>'view_snduq'
                ],
                [
                    'text' => 'سلفە',
                    'icon' => 'fas fa-fw fa-download',
                    'route' => 'slfa.index',
                    'can' =>'view_slfa',
                ],
                [
                    'text' => 'گواستنەوەی پارە',
                    'icon' => 'fa fa-fw fa-random',
                    'route' => 'transfers.index',
                    'can' =>'view_transfer',
                ],
            ]
        ],
        [
            'text' => 'ڕاپۆرت',
            'url'  => '#',
            'icon' => 'fas fa-fw fa-file',
            'submenu' =>[
                [
                    'text' => 'ڕاپۆرتی مانگانە',
                    'icon' => 'fas fa-fw fa-lock',
                    'route' => 'reports.monthly',
                ],
            ]
        ],
        [
            'text' => 'گاز',
            'url'  => '#',
            'icon' => 'fas fa-fw fa-fire',
            'can' => 'view_gas_purchase',
            'submenu' =>[
                [
                    'text' => 'کڕینی گاز',
                    'icon' => 'fas fa-fw fa-lock',
                    'route' => 'gass_purchase.index',
                    'can' => 'view_gas_purchase',
                ],
                [
                    'text' => 'گازی شۆفێرەکان',
                    'icon' => 'fas fa-fw fa-lock',
                    'route' => 'gass_to_trucks.index',
                    'can' => 'view_gas_driver',
                ],
            ]
        ],
        [
            'text' => 'بەنزین',
            'url'  => '#',
            'icon' => 'fas fa-fw fa-fire',
            'can' => 'view_banzin_purchase',
            'submenu' =>[
                [
                    'text' => 'کڕینی بەنزین',
                    'icon' => 'fas fa-fw fa-lock',
                    'route' => 'banzin_purchase.index',
                    'can' => 'view_banzin_purchase',
                ],
                [
                    'text' => 'بەنزینی پرۆژەکان',
                    'icon' => 'fas fa-fw fa-lock',
                    'route' => 'banzin_to_trucks.index',
                    'can' => 'view_banzin_driver',
                ],
            ]
        ],
        ['header' => 'ڕێکخستنی'],
        [
            'text' => 'پڕۆفایل',
            'url'  => 'admin/settings',
            'icon' => 'fas fa-fw fa-user',
            'can' => 'profile',
        ],
        [
            'text' => 'گۆڕینی ووشەی نهێنی',
            'url'  => 'admin/settings',
            'icon' => 'fas fa-fw fa-lock',

        ],
        [
            'text'    => 'بەکارهێنەران',
            'icon'    => 'fas fa-fw fa-share',
            'submenu' => [
                [
                    'text' => 'بەکارهێنەران',
                    'url'  => 'users',
                    'can' => 'view_users',
                ],
                [
                    'text' => 'زیادکردنی بەکارهێنەر',
                    'url'  => 'users/create',
                    'can' => 'add_users',
                ],
            ],
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Menu Filters
    |--------------------------------------------------------------------------
    |
    | Here we can modify the menu filters of the admin panel.
    |
    | For more detailed instructions you can look here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/#611-menu-filters
    |
    */

    'filters' => [
        Hsy\LaravelAdminLte\Menu\Filters\HrefFilter::class,
        Hsy\LaravelAdminLte\Menu\Filters\SearchFilter::class,
        Hsy\LaravelAdminLte\Menu\Filters\ActiveFilter::class,
        Hsy\LaravelAdminLte\Menu\Filters\SubmenuFilter::class,
        Hsy\LaravelAdminLte\Menu\Filters\ClassesFilter::class,
        Hsy\LaravelAdminLte\Menu\Filters\GateFilter::class,
        Hsy\LaravelAdminLte\Menu\Filters\LangFilter::class,
    ],

    /*
    |--------------------------------------------------------------------------
    | Plugins Initialization
    |--------------------------------------------------------------------------
    |
    | Here we can modify the plugins used inside the admin panel.
    |
    | For more detailed instructions you can look here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/#611-plugins
    |
    */

    'plugins' => [
        [
            'name' => 'Datatables',
            'active' => true,
            'files' => [
                [
                    'type' => 'js',
                    'asset' => true,
                    'location' => '//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js',
                ],
                [
                    'type' => 'js',
                    'asset' => true,
                    'location' => '//cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js',
                ],
                [
                    'type' => 'css',
                    'asset' => true,
                    'location' => '//cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css',
                ],
            ],
        ],
        [
            'name' => 'Select2',
            'active' => true,
            'files' => [
                [
                    'type' => 'js',
                    'asset' => false,
                    'location' => '//cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js',
                ],
                [
                    'type' => 'css',
                    'asset' => true,
                    'location' => '//cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.css',
                ],
            ],
        ],
        [
            'name' => 'Chartjs',
            'active' => true,
            'files' => [
                [
                    'type' => 'js',
                    'asset' => true,
                    'location' => '//cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.0/Chart.bundle.min.js',
                ],
            ],
        ],
        [
            'name' => 'Sweetalert2',
            'active' => false,
            'files' => [
                [
                    'type' => 'js',
                    'asset' => false,
                    'location' => '//cdn.jsdelivr.net/npm/sweetalert2@8',
                ],
            ],
        ],
        [
            'name' => 'Pace',
            'active' => false,
            'files' => [
                [
                    'type' => 'css',
                    'asset' => false,
                    'location' => '//cdnjs.cloudflare.com/ajax/libs/pace/1.0.2/themes/blue/pace-theme-center-radar.min.css',
                ],
                [
                    'type' => 'js',
                    'asset' => false,
                    'location' => '//cdnjs.cloudflare.com/ajax/libs/pace/1.0.2/pace.min.js',
                ],
            ],
        ],
    ],
];
