<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Trucks extends Model
{
    //
    public function driven()
    {
    	return $this->hasMany('App\Drive','truck_id');
    }
     
}
