<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Warehouse extends Model
{
    //
    public function purchases()
    {
        return $this->hasMany('App\Purchase','warehouse_id');
    }
}
