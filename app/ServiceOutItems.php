<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceOutItems extends Model
{
    //
    public function service()
    {
        return $this->belongsTo('App\ServiceOut','id_service');
    }

    public function item()
    {
        return $this->belongsTo('App\Items','item_id');
    }
}
