<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Trip extends Model
{
    public function from_location()
    {
        return $this->belongsTo('App\locations','from_id');
    }
    public function to_location()
    {
        return $this->belongsTo('App\locations','to_id');
    }
    public function drive()
    {
        return $this->belongsTo('App\Drive','drive_id');
    }
    public function expenses()
    {
        return $this->hasMany('App\TripExpense');
    }
    public function fines()
    {
        return $this->hasMany('App\TripFine');
    }
}
