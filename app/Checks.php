<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Checks extends Model
{
    //
    public function drive()
    {
        return $this->belongsTo('App\Drive','drive_id');
    }
    public function items()
    {
        return $this->hasMany('App\CheckItems');
    }
}
