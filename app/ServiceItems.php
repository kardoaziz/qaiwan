<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceItems extends Model
{
    //
    public function service()
    {
        return $this->belongsTo('App\Service','service_id');
    }
    public function item()
    {
        return $this->belongsTo('App\Items','item_id');
    }
}
