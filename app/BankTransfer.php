<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BankTransfer extends Model
{
    //
     public function from_bank()
    {
        return $this->belongsTo('App\Snduq','bank_id_from');
    }
    public function to_bank()
    {
        return $this->belongsTo('App\Snduq','bank_id_to');
    }
}
