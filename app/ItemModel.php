<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemModel extends Model
{
    public function items()
    {
        return $this->hasMany('App\Items','model_id');
    }
}
