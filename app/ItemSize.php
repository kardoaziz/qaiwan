<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemSize extends Model
{
    public function items()
    {
        return $this->hasMany('App\Items','size_id');
    }
}
