<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectPayment extends Model
{
    public function project()
    {
        return $this->belongsTo('App\Projects','id_project');
    }
}
