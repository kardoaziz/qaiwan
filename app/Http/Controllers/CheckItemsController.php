<?php

namespace App\Http\Controllers;

use App\CheckItems;
// use App\ServiceItems;
use App\Checks;
use Illuminate\Http\Request;
use DB;
class CheckItemsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        // $total = PurchaseItem::where('item_id',$request->item_id)->select(DB::raw('sum(qty*price) as total'),DB::raw('sum(qty*dollar) as totald'),DB::raw('sum(qty) as qty'))->first();
        $service= new CheckItems();
        $service->check_id = $request->check_id;
        $service->item_id = $request->item_id;
        // $service->qty = $request->qty;
        $service->status = "";
        // $service->dollar = ($total->totald/$total->qty);
        // $service->dollar_price = ($total->totald/$total->qty);
        $service->note = $request->note;
        $service->save();
        return "done";
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PurchaseItem  $purchaseItem
     * @return \Illuminate\Http\Response
     */
    public function show(CheckItems $ServiceItems)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ServiceItems  $ServiceItems
     * @return \Illuminate\Http\Response
     */
    public function edit(CheckItems $ServiceItems)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ServiceItems  $ServiceItems
     * @return \Illuminate\Http\Response
     */
    public function updatee(Request $request)
    {
        //
        $p = CheckItems::find($request->id);
        if($request->field == "next_check") $p->next_check = $request->val;
        if($request->field == "status") $p->status = $request->val;
        if($request->field == "note") $p->note = $request->val;
        $p->save();
        // $pr = ServiceItems::where('service_id',$p->service_id)->select(DB::raw('sum(qty*price) as total'),DB::raw('sum(qty) as total_qty'))->first();
        return $request;

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PurchaseItem  $purchaseItem
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $serviceitem = ServiceItems::find($id);
        $service_id = $serviceitem->service_id;
        $serviceitem->destroy($id);
        // $purchase = Purchase::find($purchase_id);
        return redirect(route('services.items',$service_id))->with('success',trans('main.Item Deleted Successfully'));

    }
}
