<?php

namespace App\Http\Controllers;

use App\TripFine;
use Illuminate\Http\Request;

class TripFineController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TripFine  $tripFine
     * @return \Illuminate\Http\Response
     */
    public function show(TripFine $tripFine)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TripFine  $tripFine
     * @return \Illuminate\Http\Response
     */
    public function edit(TripFine $tripFine)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TripFine  $tripFine
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TripFine $tripFine)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TripFine  $tripFine
     * @return \Illuminate\Http\Response
     */
    public function destroy(TripFine $tripFine)
    {
        //
    }
}
