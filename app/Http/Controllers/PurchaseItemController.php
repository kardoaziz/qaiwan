<?php

namespace App\Http\Controllers;

use App\PurchaseItem;
use App\DollarPrice;
use App\Purchase;
use Illuminate\Http\Request;
use DB;
class PurchaseItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
// 
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $purchase= new PurchaseItem();
        $purchase->purchase_id = $request->purchase_id;
        $purchase->item_id = $request->item_id;
        $purchase->qty = $request->qty;
        $purchase->price = $request->price;
        $purchase->dollar = $request->dollar;
        $purchase->dollar_price = DollarPrice::orderby('id','desc')->first()->price;
        $purchase->note = $request->note;
        $purchase->save();
        return "done";
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PurchaseItem  $purchaseItem
     * @return \Illuminate\Http\Response
     */
    public function show(PurchaseItem $purchaseItem)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PurchaseItem  $purchaseItem
     * @return \Illuminate\Http\Response
     */
    public function edit(PurchaseItem $purchaseItem)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PurchaseItem  $purchaseItem
     * @return \Illuminate\Http\Response
     */
    public function updatee(Request $request)
    {
        //
        $p = PurchaseItem::find($request->id);
        $dollar = DollarPrice::orderby('id','desc')->first();
        if($request->field == "qty") $p->qty = $request->val;
        if($request->field == "price") 
            {$p->price = $request->val;
                $p->dollar = $request->val/$dollar->price;
            }
        if($request->field == "dollar") 
            {$p->dollar = $request->val;
                $p->price = $request->val*$dollar->price;
            }
        $p->save();
        $pr = PurchaseItem::where('purchase_id',$p->purchase_id)->select(DB::raw('sum(qty*price) as total'),DB::raw('sum(qty) as total_qty'))->first();
        // if($p->qty>0 && isset($p->price))
        // {
        //     $item = Items::find($p->item_id);
        //     $stock = PurchaseItem::where('item_id',$p->item_id)->where('id','<>',$p->id)->sum('qty')-ServiceItems::where('item_id',$p->item_id)->sum('qty');
        //     $newcost = ($stock*$item->cost+$p->qty*$p->price)/($p->qty+$stock);
        //     // return $newcost;
        // }
        return $pr;

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PurchaseItem  $purchaseItem
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $purchaseItem = PurchaseItem::find($id);
        $purchase_id = $purchaseItem->purchase_id;
        $purchaseItem->destroy($id);
        // $purchase = Purchase::find($purchase_id);
        return redirect(route('purchases.items',$purchase_id))->with('success',trans('main.Item Deleted Successfully'));

    }
}
