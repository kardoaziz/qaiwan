<?php

namespace App\Http\Controllers;

use App\Snduq;
use App\Slfa;
use App\DollarPrice;
use Illuminate\Http\Request;

class SnduqController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view("snduq.index");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $s = new Snduq();
        $s->name = $request->name;
        $s->description = $request->description;
        $s->initial_balance = $request->initial_balance;
        $s->dollar = $request->dollar;
        $s->default = $request->default;
        $s->dollar_price = DollarPrice::orderby('created_at','desc')->first()->price;
        $s->save();
        if($s->default==1)
        {
            foreach (Snduq::where('id','<>',$s->id)->get() as $ss) {
                $ss->default = 0;
                $ss->save();
            }
        }
        return redirect(route('snduq.index'))->with('success', trans('main.Snduq Added Successfully'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Snduq  $snduq
     * @return \Illuminate\Http\Response
     */
    public function show(Snduq $snduq)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Snduq  $snduq
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $snduq= Snduq::find($id);
        return view('snduq.edit',compact('snduq'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Snduq  $snduq
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
         $s =  Snduq::find($id);
        $s->name = $request->name;
        $s->description = $request->description;
        $s->initial_balance = $request->initial_balance;
        $s->dollar = $request->dollar;
        $s->default = $request->default;
        // $s->dollar_price = $request->dollar_price;
        $s->save();
        if($s->default==1)
        {
            foreach (Snduq::where('id','<>',$s->id)->get() as $ss) {
                $ss->default = 0;
                $ss->save();
            }
        }
        return redirect(route('snduq.index'))->with('success', trans('main.Snduq Updated Successfully'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Snduq  $snduq
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $s= Snduq::find($id);
        Slfa::where('snduq_id',$id)->delete();
        $s->destroy($id);
        return redirect(route('snduq.index'))->with('success', trans('main.Snduq Deleted Successfully'));
    }
}
