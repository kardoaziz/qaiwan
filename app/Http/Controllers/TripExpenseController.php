<?php

namespace App\Http\Controllers;

use App\TripExpense;
use Illuminate\Http\Request;

class TripExpenseController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TripExpense  $tripExpense
     * @return \Illuminate\Http\Response
     */
    public function show(TripExpense $tripExpense)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TripExpense  $tripExpense
     * @return \Illuminate\Http\Response
     */
    public function edit(TripExpense $tripExpense)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TripExpense  $tripExpense
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TripExpense $tripExpense)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TripExpense  $tripExpense
     * @return \Illuminate\Http\Response
     */
    public function destroy(TripExpense $tripExpense)
    {
        //
    }
}
