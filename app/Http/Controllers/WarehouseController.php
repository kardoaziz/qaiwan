<?php

namespace App\Http\Controllers;

use App\Warehouse;
use Illuminate\Http\Request;

class WarehouseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        //
        return view('warehouses.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('warehouses.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $warehouse = new Warehouse();
        $warehouse->name = $request->name;
        $warehouse->description = $request->description;
        // $warehouse->code = $request->code;
        // $warehouse->location = $request->location;
        // $warehouse->email = $request->email;
        // $warehouse->address = $request->address;
        // $warehouse->note = $request->note;
        $warehouse->save();
        return redirect(route('warehouses.index'))->with('success', trans('main.Warehouse Added Successfully'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Items  $items
     * @return \Illuminate\Http\Response
     */
    public function show(Warehouse $warehouses)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Items  $items
     * @return \Illuminate\Http\Response
     */
   public function edit($id)
    {
        //
        $warehouse = Warehouse::find($id);
        return view('warehouses.edit',compact('warehouse'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Items  $items
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
         $warehouse =  Warehouse::find($id);
        $warehouse->name = $request->name;
        $warehouse->description = $request->description;
        // $warehouse->code = $request->code;
        // $warehouse->location = $request->location;
        // $warehouse->email = $request->email;
        // $warehouse->address = $request->address;
        // $warehouse->note = $request->note;
        $warehouse->save();
        return redirect(route('warehouses.index'))->with('success', trans('main.Warehouse Updated Successfully'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Items  $items
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $warehouse = Warehouse::find($id);
        if(sizeof($warehouse->purchases)>0)
        {
            return redirect(route('warehouses.index'))->with('warning', trans('main.warehouse can not be Deleted '));
        }
        $warehouse->destroy($id);
        return redirect(route('warehouses.index'))->with('success', trans('main.Warehouse Deleted Successfully'));
    }
}
