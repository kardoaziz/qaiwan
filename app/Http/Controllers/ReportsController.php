<?php

namespace App\Http\Controllers;
use Carbon\Carbon;
use App\Trip;
use App\TripMaterial;
use App\TripExpense;
use App\TripGass;
use App\TripFine;
use Illuminate\Http\Request;

class ReportsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function monthly(Request $request){

        //one month / 30 days
        if($request->from_date==null || $request->to_date==null)
        {
        $date = Carbon::now()->subDays(30)->startOfDay();

        $trips=Trip::where('date', '>=', $date)->count();
        $trip_cost=Trip::where('date', '>=', $date)->sum('price');
        $tripGass=TripGass::where('date', '>=', $date)->sum('price');
        $tripExpenses=TripExpense::where('date', '>=', $date)->sum('cost');
        $tripFine=TripFine::where('date', '>=', $date)->sum('amount');
        }
        else{
            $fdate = $request->from_date;
            $tdate = $request->to_date;

        $trips=Trip::where('date', '>=', $fdate)->where('date', '<=', $tdate)->count();
        $trip_cost=Trip::where('date', '>=', $fdate)->where('date', '<=', $tdate)->sum('price');
        $tripGass=TripGass::where('date', '>=', $fdate)->where('date', '<=', $tdate)->sum('price');
        $tripExpenses=TripExpense::where('date', '>=', $fdate)->where('date', '<=', $tdate)->sum('cost');
        $tripFine=TripFine::where('date', '>=', $fdate)->where('date', '<=', $tdate)->sum('amount');
        }
        return view('reports.monthlyReport',['trips'=>$trips,'trip_cost'=>$trip_cost,'tripGass'=>$tripGass,
        'tripExpense'=>$tripExpenses,'tripFine'=>$tripFine]);

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
