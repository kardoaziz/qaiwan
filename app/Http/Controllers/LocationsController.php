<?php

namespace App\Http\Controllers;

use App\locations;
use Illuminate\Http\Request;

class LocationsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    {
        return view('locations.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('locations.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $location = new locations();
        $location->name = $request->name;

        $location->save();
        return redirect(route('locations.index'))->with('success', trans('main.location Added Successfully'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\locations  $locations
     * @return \Illuminate\Http\Response
     */
    public function show(locations $locations)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\locations  $locations
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $location = Locations::find($id);
        return view('locations.edit',compact('location'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\locations  $locations
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
         $location =  Locations::find($id);
        $location->name = $request->name;

        $location->save();
        return redirect(route('locations.index'))->with('success', trans('main.price Added Successfully'));
    } 
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\locations  $locations
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = Locations::find($id);
        if($item->locationCost->count()>0)
        {
            return redirect(route('itemModel.index'))->with('warning', trans('main.location model can not be Deleted '));
        }
        $item->destroy($id);
        return redirect(route('locations.index'))->with('success', trans('main.location Deleted Successfully'));
    }
}
