<?php

namespace App\Http\Controllers;

use App\Checks;
use App\CheckItems;
use Illuminate\Http\Request;
use auth;
class ChecksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        //
        return view('checks.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('checks.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        // $destinationPath = 'invoices/';
        $service = new Checks();
        $service->drive_id = $request->drive_id;
        $service->date = $request->date;
        // $service->invoice_no = $request->invoice_no;
        $service->mileage = $request->mileage;
        $service->note = $request->note;
        $service->user_id = auth()->user()->id;
        // if ($files = $request->file('file'))
        // {
        //         $img = date('YmdHis') . "." . $files->getClientOriginalExtension();
        //         $files->move($destinationPath, $img);
        //         $service->invoice=$img;
        // }
        $service->save();
        return view('check_items.index',compact('service'))->with('success', trans('main.Check Added Successfully'));;
        // return redirect(route('checks.index'))->with('success', trans('main.Check Added Successfully'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Items  $items
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Items  $items
     * @return \Illuminate\Http\Response
     */
   public function edit($id)
    {
        //
        $service = Checks::find($id);
        return view('checks.edit',compact('service'));
    }
    public function items($id)
    {
        //
        $service = Checks::find($id);
        return view('check_items.index',compact('service'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Items  $items
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
         $service =  Checks::find($id);
        $service->drive_id = $request->drive_id;
        $service->date = $request->date;
        $service->invoice_no = $request->invoice_no;
        $service->mileage = $request->mileage;
        $service->note = $request->note;
       
        $service->save();
        return redirect(route('checks.index'))->with('success', trans('main.Check Updated Successfully'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Items  $items
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $service = Checks::find($id);
        CheckItems::where('check_id',$id)->delete();
        $service->destroy($id);
        return redirect(route('checks.index'))->with('success', trans('main.Check Deleted Successfully'));
    }
}
