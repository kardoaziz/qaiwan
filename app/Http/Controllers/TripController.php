<?php

namespace App\Http\Controllers;

use App\Trip;
use App\LocationCost;
use App\CostDate;
use Carbon\Carbon;
use App\TripMaterial;
use App\TripExpense;
use App\TripGass;
use App\TripFine;
use App\FineDates;
use App\Fines;
use App\DollarPrice;
use Illuminate\Http\Request;

class TripController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('trips.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('trips.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function getPrice(Request $request)
    {
      if($request->id>0){
        // return "not";

        $lo = LocationCost::find($request->id);
        $date=Carbon::parse($request->date)->format('Y-m-d H:i:s');
        // $price = CostDate::where('created_at','<=',$date)->orderby('created_at','desc')->first();
        $p = LocationCost::join('cost_dates','location_costs.id_date','cost_dates.id')->where('location_costs.from_id',$lo->from_id)->where('location_costs.to_id',$lo->to_id)->where('cost_dates.created_at',"<=",$date)->orderby('location_costs.id','desc')->first();
      }else{
        // return "yes";
          $p = LocationCost::where('from_id',$request->from_id)->where('to_id',$request->to_id)->orderby('id_date','desc')->first();
      }
  return $p;
    }   
    public function store(Request $request)
    {
      // return $request;
        $trip =new Trip();
        $trip->invoice_no=$request->invoice_no;
        $trip->drive_id=$request->driver_id;
        $trip->dollar_price=DollarPrice::orderby('created_at','desc')->first()->price;
        $trip->date=Carbon::parse($request->t_date)->format('Y-m-d H:i:s');
        $trip->note=$request->note;
        if ($request->from_to) {
           $locationCost=LocationCost::find($request->from_to);
           // return $locationCost;
           $trip->from_id=$locationCost->from_id;
           $trip->to_id=$locationCost->to_id;
           $trip->price=$locationCost->price;
        }
        $trip->save();
        $fine_date = FineDates::orderby('id','desc')->first();
        // $finesprice = Fines::where('date_id',$fine_date->id)->get();
        if(isset($request->material_id) && count($request->material_id)>0){
            foreach($request->material_id as $key=> $m){
                $data=array(
                    'trip_id' => $trip->id,
                    'material_id' => $request->material_id[$key] ,
                    'invoice_no' => $request->m_invoice_no[$key] ,
                    'qty_barkrdn' => $request->m_qty_barkrdn[$key] ,
                    'qty_dagrtn' => $request->m_qty_dagrtn[$key] ,
                    'date_barkrdn' => Carbon::parse($request->m_date_barkrdn[$key])->format('Y-m-d H:i:s') ,
                    'date_dagrtn' => Carbon::parse($request->m_date_dagrtn[$key])->format('Y-m-d H:i:s')  ,
                    'note' => $request->m_note[$key] ,
                    'invoice_no_to' => $request->m_invoice_no_to[$key] ,
                );

                TripMaterial::insert($data);
                $finesprice = Fines::where('date_id',$fine_date->id)->where('unit',$request->unit[$key])->first();
                if(($request->m_qty_barkrdn[$key]-$request->m_qty_dagrtn[$key])>$finesprice->normal)
                {
                    $data4=array(
                        'trip_id' => $trip->id,
                        'invoice_no' => '-1' ,
                        'amount' => (($request->m_qty_barkrdn[$key]-$request->m_qty_dagrtn[$key])-$finesprice->normal)*$finesprice->fine ,
                        'note' => 'نقس لە بارەکە هەیە بە بڕی '.($request->m_qty_barkrdn[$key]-$request->m_qty_dagrtn[$key]).' '.$request->unit[$key].' سماح '.$finesprice->normal.' '.$request->unit[$key].' ە' 
                    );
                    TripFine::insert($data4);
                    // return  ($request->m_qty_barkrdn[$key]-$request->m_qty_dagrtn[$key])*$finesprice->fine;
                }

        }

    }

    if(isset($request->expense_type_id) && count($request->expense_type_id)>0){
        foreach($request->expense_type_id as $key2 => $ex){
            $data2=array(
                'trip_id' => $trip->id,
                'expense_type_id' => $request->expense_type_id[$key2] ,
                'invoice_no' => $request->ex_invoice_no[$key2] ,
                'cost' => $request->ex_cost[$key2] ,
                'dollar' => $request->ex_dollar[$key2] ,
                'dollar_price' => DollarPrice::orderby('created_at','desc')->first()->price ,
                'note' => $request->ex_note[$key2] ,
            );
            TripExpense::insert($data2);
        }

    }

    if(isset($request->g_invoice_no) && count($request->g_invoice_no)>0){
        foreach($request->g_invoice_no as $key3 => $g){
            $data3=array(
                'trip_id' => $trip->id,
                'invoice_no' => $request->g_invoice_no[$key3] ,
                'qty' => $request->g_qty[$key3] ,
                'price' => $request->g_price[$key3] ,
                'date' => Carbon::parse($request->g_date[$key3])->format('Y-m-d H:i:s') ,
                'location' => $request->g_location[$key3] ,
                'note' => $request->g_note[$key3] ,
            );
            TripGass::insert($data3);
        }

    }

    if(isset($request->f_invoice_no) && count($request->f_invoice_no)>0){
        foreach($request->f_invoice_no as $key4 => $ex){
            $data4=array(
                'trip_id' => $trip->id,
                'invoice_no' => $request->f_invoice_no[$key4] ,
                'amount' => $request->f_amount[$key4] ,
                'note' => $request->f_note[$key4] ,
                'dollar' => $request->f_dollar[$key2] ,
                'dollar_price' => DollarPrice::orderby('created_at','desc')->first()->price ,
            );
            TripFine::insert($data4);
        }

    }
      return  redirect()->back()->with('success','data inserted');
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Trip  $trip
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $trip=Trip::find($id);
        $tripMaterials=TripMaterial::where('trip_id',$id)->get();
        $tripGass=TripGass::where('trip_id',$id)->get();
        $tripExpenses=TripExpense::where('trip_id',$id)->get();
        $tripFine=TripFine::where('trip_id',$id)->get();

        return view('trips.show',['trip'=>$trip,'tripMaterials'=>$tripMaterials,'tripGass'=>$tripGass,'tripExpense'=>$tripExpenses,'tripFine'=>$tripFine]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Trip  $trip
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       $trip= Trip::find($id);
       $tripMaterials= TripMaterial::where('trip_id',$id)->get();
       $tripExpenses= TripExpense::where('trip_id',$id)->get();
       $tripGass= TripGass::where('trip_id',$id)->get();
       $tripFine= TripFine::where('trip_id',$id)->get();

       return view('trips.edit',['trip'=>$trip,'tripMaterials'=>$tripMaterials,'tripGass'=>$tripGass,'tripExpense'=>$tripExpenses,'tripFine'=>$tripFine]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Trip  $trip
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $trip =Trip::find($id);
        $trip->invoice_no=$request->invoice_no;
        $trip->drive_id=$request->driver_id;
        $trip->date=Carbon::parse($request->t_date)->format('Y-m-d H:i:s');
        $trip->note=$request->note;
        if ($request->from_to) {
           $locationCost=LocationCost::find($request->from_to);
           $trip->from_id=$locationCost->from_id;
           $trip->to_id=$locationCost->to_id;
           $trip->price=$locationCost->price;
        }
        $trip->save();
        TripMaterial::where('trip_id',$id)->delete();
        TripExpense::where('trip_id',$id)->delete();
        TripGass::where('trip_id',$id)->delete();
        TripFine::where('trip_id',$id)->delete();
        if(isset($request->material_id) && count($request->material_id)>0){
            foreach($request->material_id as $key=> $m){
                $data=array(
                    'trip_id' => $trip->id,
                    'material_id' => $request->material_id[$key] ,
                    'invoice_no' => $request->m_invoice_no[$key] ,
                    'qty_barkrdn' => $request->m_qty_barkrdn[$key] ,
                    'qty_dagrtn' => $request->m_qty_dagrtn[$key] ,
                    'date_barkrdn' => Carbon::parse($request->m_date_barkrdn[$key])->format('Y-m-d H:i:s') ,
                    'date_dagrtn' => Carbon::parse($request->m_date_dagrtn[$key])->format('Y-m-d H:i:s')  ,
                    'note' => $request->m_note[$key] ,
                    'invoice_no_to' => $request->m_invoice_no_to[$key] ,
                );
                TripMaterial::insert($data);
        }

    }

    if(isset($request->expense_type_id) && count($request->expense_type_id)>0){
        foreach($request->expense_type_id as $key2 => $ex){
            $data2=array(
                'trip_id' => $trip->id,
                'expense_type_id' => $request->expense_type_id[$key2] ,
                'invoice_no' => $request->ex_invoice_no[$key2] ,
                'cost' => $request->ex_cost[$key2] ,
                'dollar' => $request->ex_dollar[$key2] ,
                'dollar_price' => DollarPrice::orderby('created_at','desc')->first()->price ,
                'note' => $request->ex_note[$key2] ,
            );
            TripExpense::insert($data2);
        }

    }

    if(isset($request->g_invoice_no) && count($request->g_invoice_no)>0){
        foreach($request->g_invoice_no as $key3 => $g){
            $data3=array(
                'trip_id' => $trip->id,
                'invoice_no' => $request->g_invoice_no[$key3] ,
                'qty' => $request->g_qty[$key3] ,
                'price' => $request->g_price[$key3] ,
                'date' => Carbon::parse($request->g_date[$key3])->format('Y-m-d H:i:s') ,
                'location' => $request->g_location[$key3] ,
                'note' => $request->g_note[$key3] ,
            );
            TripGass::insert($data3);
        }

    }

    if(isset($request->f_invoice_no) && count($request->f_invoice_no)>0){
        foreach($request->f_invoice_no as $key4 => $ex){
            $data4=array(
                'trip_id' => $trip->id,
                'invoice_no' => $request->f_invoice_no[$key4] ,
                'amount' => $request->f_amount[$key4] ,
                'dollar_price' =>DollarPrice::orderby('created_at','desc')->first()->price ,
                'dollar' => $request->f_dollar[$key4] ,
                'amount' => $request->f_amount[$key4] ,
                'note' => $request->f_note[$key4] ,
            );
            TripFine::insert($data4);
        }

    }
    return redirect(route('trips.index'))->with('success', trans('main.Trip Updated Successfully'));
}

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Trip  $trip
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Trip::where('id',$id)->delete();
        TripMaterial::where('trip_id',$id)->delete();
        TripExpense::where('trip_id',$id)->delete();
        TripGass::where('trip_id',$id)->delete();
        TripFine::where('trip_id',$id)->delete();

        return  redirect()->back()->with('danger','data deleted');
    }
}
