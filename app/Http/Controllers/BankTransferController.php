<?php

namespace App\Http\Controllers;

use App\BankTransfer;
use App\DollarPrice;
use Illuminate\Http\Request;

class BankTransferController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view("transfers.index");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $t = new BankTransfer();
        $t->bank_id_from = $request->snduq_id_from;
        $t->bank_id_to = $request->snduq_id_to;
        $t->amount = $request->amount;
        $t->dollar = $request->dollar;
        $t->dollar_price = DollarPrice::orderby('created_at','desc')->first()->price;
        $t->date = $request->date;
        $t->status = "pending";
        $t->note = $request->note;
        $t->save();

        return redirect(route('transfers.index'))->with('success', trans('main.Transfer Added Successfully'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\BankTransfer  $bankTransfer
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\BankTransfer  $bankTransfer
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $transfer = BankTransfer::find($id);
        return view('transfers.edit',compact('transfer'));
        // return redirect(route('transfers.index'))->with('success', trans('main.Transfer Added Successfully'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\BankTransfer  $bankTransfer
     * @return \Illuminate\Http\Response
     */
    public function updatee(Request $request)
    {
        // return "yes";
        $t = BankTransfer::find($request->id);
        $t->status = $request->status;
        $t->save();
        return "done";
    }
    public function update(Request $request, $id)
    {
        //
        $t =  BankTransfer::find($id);
        $t->bank_id_from = $request->snduq_id_from;
        $t->bank_id_to = $request->snduq_id_to;
        $t->amount = $request->amount;
        $t->dollar = $request->dollar;
        $t->dollar_price = DollarPrice::orderby('created_at','desc')->first()->price;
        $t->date = $request->date;
        // $t->status = $request->status;
        $t->note = $request->note;
        $t->save();

        return redirect(route('transfers.index'))->with('success', trans('main.Transfer Updated Successfully'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\BankTransfer  $bankTransfer
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $transfer = BankTransfer::find($id);
        $transfer->destroy($id);
        return redirect(route('transfers.index'))->with('success', trans('main.Transfer Delete Successfully'));
    }
}
