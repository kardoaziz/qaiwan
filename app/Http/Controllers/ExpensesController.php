<?php

namespace App\Http\Controllers;

use App\Expenses;
use App\DollarPrice;
use Illuminate\Http\Request;

class ExpensesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('expenses.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $e = new Expenses();
        $e->drive_id = $request->drive_id;
        $e->date = $request->date;
        $e->invoice_no = $request->invoice_no;
         $e->snduq_id = $request->snduq_id;
        $e->cost = $request->cost;
        $e->note = $request->note;
        $e->dollar = $request->dollar;
        $e->dollar_price = DollarPrice::orderby('created_at','desc')->first()->price;
        $e->save();
        return redirect(route('expenses.index'))->with('success', trans('main.Expenses Added Successfully'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Expenses  $expenses
     * @return \Illuminate\Http\Response
     */
    public function show(Expenses $expenses)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Expenses  $expenses
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
         $expense= Expenses::find($id);
         return view('expenses.edit',compact('expense'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Expenses  $expenses
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $e =  Expenses::find($id);
        $e->drive_id = $request->drive_id;
        $e->date = $request->date;
        $e->invoice_no = $request->invoice_no;
        $e->snduq_id = $request->snduq_id;
        $e->cost = $request->cost;
        $e->note = $request->note;
        $e->dollar = $request->dollar;
        $e->dollar_price = DollarPrice::orderby('created_at','desc')->first()->price;
        $e->save();
        return redirect(route('expenses.index'))->with('success', trans('main.Expenses Updated Successfully'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Expenses  $expenses
     * @return \Illuminate\Http\Response
     */
    public function destroy(Expenses $expenses)
    {
        //
        $e= Expenses::find($id);
        $e->destroy($id);

        return redirect(route('expenses.index'))->with('success', trans('main.Expenses Deleted Successfully'));
    }
}
