<?php

namespace App\Http\Controllers;

use App\banzinQtyToTrucks;
use Illuminate\Http\Request;

class BanzinQtyToTrucksController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('banzin_to_trucks.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('banzin_to_trucks.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        // return $request;
        $dp = new banzinQtyToTrucks();
        $dp->project_id = $request->project_id;
        $dp->qty = $request->amount;
        $dp->price = $request->price;
        $dp->name = $request->name;
        $dp->plate = $request->plate;
        $dp->date = $request->date;
        $dp->note = $request->note;
        $dp->save();
        return redirect(route('banzin_to_trucks.index'))->with('success', trans('main.banzin to truck Added Successfully'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\DrivePayment  $drivePayment
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $payment = banzinQtyToTrucks::find($id);
        return view("banzin_to_trucks.print",compact('payment'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\DrivePayment  $drivePayment
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $payment = banzinQtyToTrucks::find($id);
        return view("banzin_to_trucks.edit",compact('payment'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\DrivePayment  $drivePayment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $dp =  banzinQtyToTrucks::find($id);
        $dp->project_id = $request->project_id;
        $dp->qty = $request->amount;
        $dp->price = $request->price;
        $dp->name = $request->name;
        $dp->plate = $request->plate;
        $dp->date = $request->date;
        $dp->note = $request->note;
        $dp->save();
        return redirect(route('banzin_to_trucks.index'))->with('success', trans('main.banzin to truck Updated Successfully'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\DrivePayment  $drivePayment
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $d = banzinQtyToTrucks::find($id);
        $d->destroy($id);
        return redirect(route('banzin_to_trucks.index'))->with('success', trans('main.banzin to truck Deleted Successfully'));
    }
}
