<?php

namespace App\Http\Controllers;

use App\DrivePayment;
use App\DollarPrice;
use Illuminate\Http\Request;

class DrivePaymentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('drive_payments.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('drive_payments.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        // return $request;
        $dp = new DrivePayment();
        $dp->drive_id = $request->drive_id;
        $dp->amount = $request->amount;
        $dp->dollar = $request->dollar;
        $dp->dollar_price = DollarPrice::orderby('created_at','desc')->first()->price;
        $dp->snduq_id = $request->snduq_id;
        $dp->date = $request->date;
        $dp->note = $request->note;
        $dp->save();
        return redirect(route('drive_payments.index'))->with('success', trans('main.Payment Added Successfully'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\DrivePayment  $drivePayment
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $payment = DrivePayment::find($id);
        return view("drive_payments.print",compact('payment'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\DrivePayment  $drivePayment
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $payment = DrivePayment::find($id);
        return view("drive_payments.edit",compact('payment'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\DrivePayment  $drivePayment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        // return $request;
        $dp =  DrivePayment::find($id);
        $dp->drive_id = $request->drive_id;
        $dp->amount = $request->amount;
        $dp->snduq_id = $request->snduq_id;
        $dp->date = $request->date;
        $dp->note = $request->note;
        $dp->save();
        return redirect(route('drive_payments.index'))->with('success', trans('main.Payment Updated Successfully'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\DrivePayment  $drivePayment
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $d = DrivePayment::find($id);
        $d->destroy($id);
        return redirect(route('drive_payments.index'))->with('success', trans('main.Payment Deleted Successfully'));
    }
}
