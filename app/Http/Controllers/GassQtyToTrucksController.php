<?php

namespace App\Http\Controllers;

use App\gassQtyToTrucks;
use Illuminate\Http\Request;

class GassQtyToTrucksController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('gass_to_trucks.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('gass_to_trucks.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        // return $request;
        $dp = new gassQtyToTrucks();
        $dp->drive_id = $request->drive_id;
        $dp->qty = $request->amount;
        $dp->date = $request->date;
        $dp->note = $request->note;
        $dp->save();
        return redirect(route('gass_to_trucks.index'))->with('success', trans('main.gass to truck Added Successfully'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\DrivePayment  $drivePayment
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $payment = gassQtyToTrucks::find($id);
        return view("gass_to_trucks.print",compact('payment'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\DrivePayment  $drivePayment
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $payment = gassQtyToTrucks::find($id);
        return view("gass_to_trucks.edit",compact('payment'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\DrivePayment  $drivePayment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $dp =  gassQtyToTrucks::find($id);
        $dp->drive_id = $request->drive_id;
        $dp->qty = $request->amount;
        $dp->date = $request->date;
        $dp->note = $request->note;
        $dp->save();
        return redirect(route('gass_to_trucks.index'))->with('success', trans('main.gass to truck Updated Successfully'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\DrivePayment  $drivePayment
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $d = gassQtyToTrucks::find($id);
        $d->destroy($id);
        return redirect(route('gass_to_trucks.index'))->with('success', trans('main.gass to truck Deleted Successfully'));
    }
}
