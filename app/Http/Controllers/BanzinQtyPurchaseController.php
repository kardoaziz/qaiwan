<?php

namespace App\Http\Controllers;

use App\banzinQtyPurchase;
use Illuminate\Http\Request;

class BanzinQtyPurchaseController extends Controller
{
     public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('banzin_purchase.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('banzin_purchase.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        // return $request;
        $dp = new banzinQtyPurchase();
        $dp->qty = $request->amount;
        $dp->price = $request->price;
        $dp->date = $request->date;
        $dp->note = $request->note;
        $dp->save();
        return redirect(route('banzin_purchase.index'))->with('success', trans('main.banzin to truck Added Successfully'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\DrivePayment  $drivePayment
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $payment = banzinQtyPurchase::find($id);
        return view("banzin_purchase.print",compact('payment'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\DrivePayment  $drivePayment
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $payment = banzinQtyPurchase::find($id);
        return view("banzin_purchase.edit",compact('payment'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\DrivePayment  $drivePayment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $dp =  banzinQtyPurchase::find($id);
        $dp->qty = $request->amount;
        $dp->price = $request->price;
        $dp->date = $request->date;
        $dp->note = $request->note;
        $dp->save();
        return redirect(route('banzin_purchase.index'))->with('success', trans('main.banzin to truck Updated Successfully'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\DrivePayment  $drivePayment
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $d = banzinQtyPurchase::find($id);
        $d->destroy($id);
        return redirect(route('banzin_purchase.index'))->with('success', trans('main.banzin to truck Deleted Successfully'));
    }
}
