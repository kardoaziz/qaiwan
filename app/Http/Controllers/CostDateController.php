<?php

namespace App\Http\Controllers;

use App\CostDate;
use Illuminate\Http\Request;

class CostDateController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CostDate  $costDate
     * @return \Illuminate\Http\Response
     */
    public function show(CostDate $costDate)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CostDate  $costDate
     * @return \Illuminate\Http\Response
     */
    public function edit(CostDate $costDate)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CostDate  $costDate
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CostDate $costDate)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CostDate  $costDate
     * @return \Illuminate\Http\Response
     */
    public function destroy(CostDate $costDate)
    {
        //
    }
}
