<?php

namespace App\Http\Controllers;

use App\Material;
use Illuminate\Http\Request;

class MaterialController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    {
        return view('materials.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('materials.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $location = new Material();
        $location->name = $request->name;

        $location->save();
        return redirect(route('materials.index'))->with('success', trans('main.location Added Successfully'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\locations  $locations
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\locations  $locations
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $location = Material::find($id);
        return view('materials.edit',compact('location'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\locations  $locations
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
         $location =  Material::find($id);
        $location->name = $request->name;

        $location->save();
        return redirect(route('materials.index'))->with('success', trans('main.price Added Successfully'));
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\locations  $locations
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = Material::find($id);
        if(sizeof($item->trips)>0)
        {
            return redirect(route('materials.index'))->with('warning', trans('main.material can not be Deleted '));
        }
        $item->destroy($id);
        return redirect(route('materials.index'))->with('success', trans('main.location Deleted Successfully'));
    }
}
