<?php

namespace App\Http\Controllers;

use App\ProjectPayment;
use App\DollarPrice;
use Illuminate\Http\Request;

class ProjectPaymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('project_payments.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $projects = new ProjectPayment();
        $projects->id_project = $request->id_project;
        $projects->date = $request->date;
        $projects->invoice_no = $request->invoice_no;
        $projects->amount = $request->amount;
        $projects->note = $request->note;
        $projects->dollar_price = DollarPrice::orderby('created_at','desc')->first()->price;
        $projects->snduq_id = $request->snduq_id;
        $projects->dollar = $request->dollar;


        $projects->save();
        return redirect()->route('projectpayments.payments',$request->id_project)->with('success', trans('main.projects Added Successfully'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ProjectPayment  $projectPayment
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       $payment=ProjectPayment::find($id);
       return view('project_payments.print',compact('payment'));

    }

    public function payments($id)
    {
        $payments=ProjectPayment::where('id_project',$id)->orderby('created_at','desc')->get();
        return view('project_payments.index',compact('payments'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ProjectPayment  $projectPayment
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $payment = ProjectPayment::find($id);
        return view('project_payments.edit',compact('payment'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ProjectPayment  $projectPayment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $projects = ProjectPayment::find($id);
        $projects->id_project = $request->id_project;
        $projects->date = $request->date;
        $projects->invoice_no = $request->invoice_no;
        $projects->amount = $request->amount;
        $projects->note = $request->note;
        $projects->dollar_price = DollarPrice::orderby('created_at','desc')->first()->price;
        $projects->snduq_id = $request->snduq_id;
        $projects->dollar = $request->dollar;


        $projects->save();
        return redirect()->route('projectpayments.payments',$request->id_project)->with('success', trans('main.projects Added Successfully'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ProjectPayment  $projectPayment
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $payment = ProjectPayment::find($id);
        $payment->destroy($id);
        return redirect()->back()->with('danger', trans('main.projects Deleted Successfully'));
    }
}
