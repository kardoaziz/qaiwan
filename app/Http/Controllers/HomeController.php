<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Charts\Growth;
use Carbon\Carbon;
use App\Trip;
use App\Services;
use DB;
use Auth;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        // Auth::logout();
        // return redirect('/login')->with('success', 'تکاتە مانگانەکەت تازە بکەرەوە');
        $tripschart = new Growth;
        $tripschart->title('ژ.بارەکان بە پێی بەروار ', 30, "black", true, 'Helvetica Neue');
        $tripschart->displaylegend(false);
       
        $dates = [];
        $trips = [];
        for ($i=30; $i >=0 ; $i--) { 
            $d = Carbon::today()->subDays($i);
            array_push($dates,  $d->month.'-'.$d->day);
            array_push($trips,Trip::where('date',$d)->count());
        }
         $tripschart->options([

                            'scales' => [
                                'xAxes' => [
                                    [ 
                                        'ticks' => [
                                            'autoSkip'=> 'true',
                                            "maxRotation"=> 90,
                                            "minRotation"=> 90
                                        ],

                                       
                                    ],
                               ],
                                'yAxes' => [
                                    [ 
                                        'ticks' => [
                                            'beginAtZero'=> 'true',
                                            'min' => 0,
                                            'max' => max($trips)+10
                                        ],

                                        'scaleLabel'=> [
                                        'display'=> 'true',
                                        'labelString'=> 'ژ.بار'
                                      ]
                                    ],
                               ],
                            ],
                        ]);
        $tripschart->labels($dates);
        // $tripschart->dataset('trips', 'bar', )->color('rgb(255,255,255,0.0)')->backgroundColor('rgb(255,255,255,0.0)')->options(["pointStyle"=>"line","radius"=>1]);
        $tripschart->dataset('ژ.بار', 'bar', $trips)->color('green')->backgroundColor('rgb(0,100,0,1)')->options(["pointStyle"=>'bar',"radius"=>1,'labelsRotation'=>90]);



        $serviceschart = new Growth;
        $serviceschart->title('تێچووی خزمەتگوزاری $ ', 30, "black", true, 'Helvetica Neue');
        $serviceschart->displaylegend(false);
       
        $dates = [];
        $services = [];
        for ($i=30; $i >=0 ; $i--) { 
            $d = Carbon::today()->subDays($i);
            array_push($dates, $d->month.'-'.$d->day);
            array_push($services,Services::join('service_items','service_items.service_id','services.id')->where('services.date',$d)->select(DB::raw('IFNULL(sum(service_items.qty*service_items.dollar),0) as total'))->first()->total);
        }
         $serviceschart->options([
                            'scales' => [
                                'xAxes' => [
                                    [ 
                                        'ticks' => [
                                            'autoSkip'=> 'false',
                                            "maxRotation"=> 90,
                                            "minRotation"=> 90
                                        ],

                                       
                                    ],
                               ],
                                'yAxes' => [
                                    [ 
                                        'ticks' => [
                                            'beginAtZero'=> 'true',
                                            'min' => 0,
                                            'max' => max($services)+10
                                        ],
                                        'scaleLabel'=> [
                                        'display'=> 'true',
                                        'labelString'=> 'تێچوو $'
                                      ]
                                    ],
                               ],
                            ],
                        ]);
        $serviceschart->labels($dates);
        // return $services;
        // $serviceschart->dataset('trips', 'bar', )->color('rgb(255,255,255,0.0)')->backgroundColor('rgb(255,255,255,0.0)')->options(["pointStyle"=>"line","radius"=>1]);
        $serviceschart->dataset('تێچوو', 'bar', $services)->color('red')->backgroundColor('rgb(220,0,0,1)')->options(["pointStyle"=>'bar',"radius"=>1]);


        return view('home',["chart"=>$tripschart,'chart2'=>$serviceschart]);
    }
}
