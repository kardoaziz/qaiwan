<?php

namespace App\Http\Controllers;

use App\LocationCost;
use App\locations;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\CostDate;
// 
class LocationCostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $CostDate=CostDate::orderby('id','desc')->get();
        return view('location_cost.index',['costDate'=>$CostDate]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $locations=locations::all();

        return view('location_cost.create',['locations'=>$locations]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $costDate=new CostDate();
        $costDate->created_at=Carbon::now();
        $costDate->save();
        // return $request;
            foreach($request->from as $key => $val){
                $data =array(
                    'from_id' => $request->from[$key],
                    'to_id' => $request->to[$key],
                    'price' => $request->price[$key],
                    'status' => true,
                    'created_at' => Carbon::now(),
                    'id_date' => $costDate->id,
                );
                LocationCost::insert($data);
            }
            return redirect(route('locationCost.index'))->with('success', trans('main.price Deleted Successfully'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\LocationCost  $locationCost
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       $data=CostDate::find($id);
       $prices=LocationCost::where('id_date',$id)->get();
       return view('location_cost.show',['data'=>$data,'prices'=>$prices]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\LocationCost  $locationCost
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\LocationCost  $locationCost
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, LocationCost $locationCost)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\LocationCost  $locationCost
     * @return \Illuminate\Http\Response
     */
    public function destroy(LocationCost $locationCost)
    {
        //
    }
}
