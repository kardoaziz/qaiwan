<?php

namespace App\Http\Controllers;

use App\Purchase;
use App\PurchaseItem;
use Illuminate\Http\Request;

class PurchaseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        //
        return view('purchases.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('purchases.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $destinationPath = 'invoices/';
        $purchase = new Purchase();
        $purchase->warehouse_id = $request->warehouse_id;
        $purchase->supplier_id = $request->supplier_id;
        $purchase->date = $request->date;
        $purchase->discount = $request->discount;
        $purchase->invoice_no = $request->invoice_no;
        $purchase->note = $request->note;
        if ($files = $request->file('file'))
        {
                $img = date('YmdHis') . "." . $files->getClientOriginalExtension();
                $files->move($destinationPath, $img);
                $purchase->invoice=$img;
        }
        $purchase->save();
        return redirect(route('purchases.index'))->with('success', trans('main.Purchase Added Successfully'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Items  $items
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Items  $items
     * @return \Illuminate\Http\Response
     */
   public function edit($id)
    {
        //
        $purchase = Purchase::find($id);
        return view('purchases.edit',compact('purchase'));
    }
    public function items($id)
    {
        //
        $purchase = Purchase::find($id);
        return view('purchase_items.index',compact('purchase'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Items  $items
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $destinationPath = "invoices/";
         $purchase =  Purchase::find($id);
        $purchase->warehouse_id = $request->warehouse_id;
        $purchase->supplier_id = $request->supplier_id;
        $purchase->date = $request->date;
        $purchase->discount = $request->discount;
        $purchase->invoice_no = $request->invoice_no;
        $purchase->note = $request->note;
        if ($files = $request->file('file'))
        {
                $img = date('YmdHis') . "." . $files->getClientOriginalExtension();
                $files->move($destinationPath, $img);
                $purchase->invoice=$img;
        }
        $purchase->save();
        return redirect(route('purchases.index'))->with('success', trans('main.Purchase Updated Successfully'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Items  $items
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $purchase = Purchase::find($id);
        PurchaseItem::where('purchase_id',$id)->delete();
        $purchase->destroy($id);
        return redirect(route('purchases.index'))->with('success', trans('main.Purchase Deleted Successfully'));
    }
}
