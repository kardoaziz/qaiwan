<?php

namespace App\Http\Controllers;

use App\ServiceOut;
use App\ServiceOutItems;
use Illuminate\Http\Request;

class ServiceOutController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('service_out.index');
    }
     public function services($id)
    {
        //
        return view('service_out.index',compact('id'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $s = new ServiceOut();
        $s->id_project= $request->project_id;
        $s->date= $request->date;
        $s->plate= $request->plate;
        $s->fee= $request->fee;
        $s->invoice_no= $request->invoice_no;
        $s->note= $request->note;
        $s->save();

        return redirect(route('serviceOut.index'))->with('success', trans('main.Service Added Successfully'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ServiceOut  $serviceOut
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ServiceOut  $serviceOut
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $service = ServiceOut::find($id);
        return view('service_out.edit',compact('service'));
    }
public function items($id)
    {
        //
        $service = ServiceOut::find($id);
        return view('service_out.items',compact('service'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ServiceOut  $serviceOut
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
         $s =  ServiceOut::find($id);
        $s->id_project= $request->project_id;
        $s->date= $request->date;
        $s->plate= $request->plate;
        $s->fee= $request->fee;
        $s->invoice_no= $request->invoice_no;
        $s->note= $request->note;
        $s->save();

        return redirect(route('serviceOut.index'))->with('success', trans('main.Service Updated Successfully'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ServiceOut  $serviceOut
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $service = ServiceOut::find($id);
        ServiceOutItems::where('id_service',$id)->delete();
        $service->destroy($id);
        return redirect(route('serviceOut.index'))->with('success', trans('main.Service Deleted Successfully'));
    }
}
