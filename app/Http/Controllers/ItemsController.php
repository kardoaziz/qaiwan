<?php

namespace App\Http\Controllers;

use App\Items;
use Illuminate\Http\Request;

class ItemsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        //
        return view('items.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function alertItems($id)
    {
        // return "yes";
         return view('items.index',['alert'=>true]);
    }public function projectItems($id)
    {
        // return "yes";
         return view('items.index',['projectparts'=>true]);
    }
    public function create()
    {
        //
        return view('items.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $driver = new Items();
        $driver->name = $request->name;
        $driver->model_id = $request->model_id;
        $driver->size_id = $request->size_id;
        $driver->description = $request->description;
        $driver->code = $request->code;
        $driver->sell_price = $request->sell_price;
        $driver->location = $request->location;
        $driver->alert_qty = $request->alert_qty;
        // $driver->email = $request->email;
        // $driver->address = $request->address;
        // $driver->note = $request->note;
        $driver->save();
        return redirect(route('items.index'))->with('success', trans('main.Item Added Successfully'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Items  $items
     * @return \Illuminate\Http\Response
     */
    public function show(Items $items)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Items  $items
     * @return \Illuminate\Http\Response
     */
   public function edit($id)
    {
        //
        $item = Items::find($id);
        return view('items.edit',compact('item'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Items  $items
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
         $driver =  Items::find($id);
        $driver->name = $request->name;
        $driver->model_id = $request->model_id;
        $driver->size_id = $request->size_id;
        $driver->description = $request->description;
        $driver->code = $request->code;
        $driver->sell_price = $request->sell_price;
        $driver->location = $request->location;
        $driver->alert_qty = $request->alert_qty;
        // $driver->email = $request->email;
        // $driver->address = $request->address;
        // $driver->note = $request->note;
        $driver->save();
        return redirect(route('items.index'))->with('success', trans('main.Item Updated Successfully'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Items  $items
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $item = Items::find($id);
        //return $item->services;
        if(sizeof($item->services)>0 || sizeof($item->checks)>0 || sizeof($item->serviceOuts)>0)
        {
            return redirect(route('items.index'))->with('warning', trans('main.Item  can not be Deleted '));
        }
        $item->destroy($id);
        return redirect(route('items.index'))->with('success', trans('main.Item Deleted Successfully'));
    }
}
