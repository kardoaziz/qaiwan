<?php

namespace App\Http\Controllers;

use App\Slfa;
use App\DollarPrice;
use Illuminate\Http\Request;

class SlfaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('slfa.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $s = new Slfa();
        $s->date = $request->date;
        $s->amount = $request->amount;
        $s->snduq_id = $request->snduq_id;
        $s->note = $request->note;
        $s->dollar = $request->dollar;
        $s->dollar_price = DollarPrice::orderby('created_at','desc')->first()->price;
        $s->save();
        return redirect(route('slfa.index'))->with('success', trans('main.Slfa Added Successfully'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Slfa  $slfa
     * @return \Illuminate\Http\Response
     */
    public function show(Slfa $slfa)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Slfa  $slfa
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $slfa = Slfa::find($id);
        return view('slfa.edit',compact('slfa'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Slfa  $slfa
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $s =  Slfa::find($id);
        $s->date = $request->date;
        $s->amount = $request->amount;
        $s->snduq_id = $request->snduq_id;
        $s->note = $request->note;
        $s->dollar = $request->dollar;
        $s->dollar_price = DollarPrice::orderby('created_at','desc')->first()->price;
        $s->save();
        return redirect(route('slfa.index'))->with('success', trans('main.Slfa Updated Successfully'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Slfa  $slfa
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $s = Slfa::find($id);
        $s->destroy($id);

        return redirect(route('slfa.index'))->with('success', trans('main.Slfa Deleted Successfully'));
    }
}
