<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Drive;
use App\Trip;
class AccountingController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('accounting.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function showaccounting(Request $request)
    {
        return $request;
        $ids = $request->selected;
        return view('accounting.accounting',compact('ids'));

    }
    public function finishaccounting(Request $request)
    {
        // return $request;
        $ids = $request->ids;
        // return $ids;
        $trips = Trip::whereIn('id',$ids)->get();
        foreach ($trips as $t) {
            # code...
            $t->finished = 1;
            $t->save();
        }
        // $ids = $request->selected;
        return redirect(route('accounting.index'))->with('success', trans('main.account marked as finished'));

    }
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function details($id)
    {

    }
    public function show($id)
    {
        //
        $drive = Drive::find($id);
        return view('accounting.detail',compact('drive'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
