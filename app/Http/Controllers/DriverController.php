<?php

namespace App\Http\Controllers;

use App\Driver;
use Illuminate\Http\Request;
use Carbon\Carbon;
class DriverController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        //
        $drivers = Driver::all();
        return view('drivers.index',compact('drivers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('drivers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $driver = new Driver();
        $driver->name = $request->name;
        $driver->phone = $request->phone;
        $driver->email = $request->email;
        $driver->kafil_name = $request->kafil_name;
        $driver->kafil_phone = $request->kafil_phone;
        $driver->address = $request->address;
        $driver->note = $request->note;
        $driver->license_no = $request->license_no;
        $driver->license_expire = $request->license_expire;
        $driver->save();
        return redirect(route('drivers.index'))->with('success', trans('main.Driver Added Successfully'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Driver  $driver
     * @return \Illuminate\Http\Response
     */
    public function show(Driver $driver)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Driver  $driver
     * @return \Illuminate\Http\Response
     */
   public function edit($id)
    {
        //
        $driver = Driver::find($id);
        return view('drivers.edit',compact('driver'));
    }
    public function expired()
    {
        //
        $today_date = Carbon::today('Asia/Baghdad');
        $drivers = Driver::whereNotNull('license_expire')->where('license_expire','<=',$today_date)->get();
        // return $drivers;
        return view('drivers.index',compact('drivers'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Driver  $driver
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $driver =  Driver::find($id);
        $driver->name = $request->name;
        $driver->phone = $request->phone;
        $driver->email = $request->email;
        $driver->address = $request->address;
        $driver->kafil_name = $request->kafil_name;
        $driver->kafil_phone = $request->kafil_phone;
        $driver->license_no = $request->license_no;
        $driver->license_expire = $request->license_expire;
        $driver->note = $request->note;
        $driver->save();
        return redirect(route('drivers.index'))->with('success', trans('main.Driver Updated Successfully'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Driver  $driver
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $item = Driver::find($id);
        // return $item->drive;
        if(sizeof($item->drive)>0)
        {
            return redirect(route('drivers.index'))->with('warning', trans('main.Driver can not be Deleted '));
        }
        $item->destroy($id);
        return redirect(route('drivers.index'))->with('success', trans('main.Driver Deleted Successfully'));
    }
}
