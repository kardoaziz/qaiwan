<?php

namespace App\Http\Controllers;

use App\ExpenseType;
use Illuminate\Http\Request;

class ExpenseTypeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    {
        return view('expense_type.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('expense_type.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $location = new ExpenseType();
        $location->name = $request->name;

        $location->save();
        return redirect(route('expense_type.index'))->with('success', trans('main.location Added Successfully'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\locations  $locations
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\locations  $locations
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $location = ExpenseType::find($id);
        return view('expense_type.edit',compact('location'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\locations  $locations
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
         $location =  ExpenseType::find($id);
        $location->name = $request->name;

        $location->save();
        return redirect(route('expense_type.index'))->with('success', trans('main.expense type Successfully'));
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\locations  $locations
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = ExpenseType::find($id);
        if(sizeof($item->trips)>0)
        {
            return redirect(route('expense_type.index'))->with('warning', trans('main.expense type can not be Deleted '));
        }
        $item->destroy($id);
        return redirect(route('expense_type.index'))->with('success', trans('main.expense type Deleted Successfully'));
    }
}
