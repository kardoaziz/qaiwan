<?php

namespace App\Http\Controllers;

use App\FineDates;
use Illuminate\Http\Request;

class FineDatesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\FineDates  $fineDates
     * @return \Illuminate\Http\Response
     */
    public function show(FineDates $fineDates)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\FineDates  $fineDates
     * @return \Illuminate\Http\Response
     */
    public function edit(FineDates $fineDates)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\FineDates  $fineDates
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, FineDates $fineDates)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\FineDates  $fineDates
     * @return \Illuminate\Http\Response
     */
    public function destroy(FineDates $fineDates)
    {
        //
    }
}
