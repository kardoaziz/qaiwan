<?php

namespace App\Http\Controllers;

use App\DollarPrice;
use Illuminate\Http\Request;

class DollarPriceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('dollars.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $d = new DollarPrice();
        $d->date=$request->date;
        $d->price=$request->price;
        $d->note=$request->note;
        $d->save();
        return redirect(route('dollars.index'))->with('success', trans('main.New Dollar Price added Successfully'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\DollarPrice  $dollarPrice
     * @return \Illuminate\Http\Response
     */
    public function show(DollarPrice $dollarPrice)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\DollarPrice  $dollarPrice
     * @return \Illuminate\Http\Response
     */
    public function edit(DollarPrice $dollarPrice)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\DollarPrice  $dollarPrice
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DollarPrice $dollarPrice)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\DollarPrice  $dollarPrice
     * @return \Illuminate\Http\Response
     */
    public function destroy(DollarPrice $dollarPrice)
    {
        //
    }
}
