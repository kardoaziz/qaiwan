<?php

namespace App\Http\Controllers;

use App\Trucks;
use Illuminate\Http\Request;

class TrucksController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('trucks.index');
    }
     public function expired_registeration()
    {
        //
        return view('trucks.index',['expired_registeration'=>1]);
    }
    public function expired_permit()
    {
        //
        return view('trucks.index',['expired_permit'=>1]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('trucks.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $truck = new Trucks();
        $truck->code = $request->code;
        $truck->color = $request->color;
        $truck->capacity = $request->capacity;
        $truck->year = $request->year;
        $truck->vin = $request->vin;
        $truck->registeration_no = $request->registeration_no;
        $truck->registeration_expire = $request->registeration_expire;
        $truck->permit_no = $request->permit_no;
        $truck->permit_expire = $request->permit_expire;
        $truck->plate = $request->plate;
        $truck->description = $request->description;
        $truck->save();
        return redirect(route('trucks.index'))->with('success', trans('main.Truck Added Successfully'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Trucks  $trucks
     * @return \Illuminate\Http\Response
     */
    public function show(Trucks $trucks)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Trucks  $trucks
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $truck = Trucks::find($id);
        return view('trucks.edit',compact('truck'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Trucks  $trucks
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        //
        $truck =  Trucks::find($id);
        $truck->code = $request->code;
        $truck->color = $request->color;
        $truck->year = $request->year;
        $truck->capacity = $request->capacity;
        $truck->vin = $request->vin;
         $truck->registeration_no = $request->registeration_no;
        $truck->registeration_expire = $request->registeration_expire;
        $truck->permit_no = $request->permit_no;
        $truck->permit_expire = $request->permit_expire;
        $truck->plate = $request->plate;
        $truck->description = $request->description;
        $truck->save();
        return redirect(route('trucks.index'))->with('success', trans('main.Truck Updated Successfully'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Trucks  $trucks
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //

        $item = Trucks::find($id);
        if(sizeof($item->driven)>0)
        {
            return redirect(route('trucks.index'))->with('warning', trans('main.Truck can not be Deleted '));
        }
        $item->destroy($id);
        return redirect(route('trucks.index'))->with('success', trans('main.Truck Deleted Successfully'));
    }
}
