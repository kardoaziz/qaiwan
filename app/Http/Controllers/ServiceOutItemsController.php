<?php

namespace App\Http\Controllers;

use App\ServiceOutItems;
use App\PurchaseItem;
use App\DollarPrice;
use App\Items;
use Illuminate\Http\Request;
use DB;
class ServiceOutItemsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        // $total = PurchaseItem::where('item_id',$request->item_id)->select(DB::raw('sum(qty*price) as total'),DB::raw('IFNULL(sum(qty*dollar),0) as totald'),DB::raw('IFNULL(sum(qty),1) as qty'))->first();
        $item = Items::find($request->item_id);
        $service= new ServiceOutItems();
        $service->id_service = $request->service_id;
        $service->item_id = $request->item_id;
        $service->qty = 0;//$request->qty;
        $service->price = $item->sell_price;
        $service->dollar = $item->sell_price/DollarPrice::orderby('created_at','desc')->first()->price;
        $service->dollar_price = DollarPrice::orderby('created_at','desc')->first()->price;
        $service->note = $request->note;
        $service->save();
        return "done";
    }
     public function updatee(Request $request)
    {
        //
        $p = ServiceOutItems::find($request->id);
        if($request->field == "qty") $p->qty = $request->val;
        if($request->field == "price") {
            $p->price = $request->val;
            $p->dollar = $p->price/$p->dollar_price;
        }
        if($request->field == "note") $p->note = $request->val;
        $p->save();
        // $pr = ServiceItems::where('service_id',$p->service_id)->select(DB::raw('sum(qty*price) as total'),DB::raw('sum(qty) as total_qty'))->first();
        return $request;

    }
    /**
     * Display the specified resource.
     *
     * @param  \App\ServiceOutItems  $serviceOutItems
     * @return \Illuminate\Http\Response
     */
    public function show(ServiceOutItems $serviceOutItems)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ServiceOutItems  $serviceOutItems
     * @return \Illuminate\Http\Response
     */
    public function edit(ServiceOutItems $serviceOutItems)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ServiceOutItems  $serviceOutItems
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ServiceOutItems $serviceOutItems)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ServiceOutItems  $serviceOutItems
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
         $serviceitem = ServiceOutItems::find($id);
        $service_id = $serviceitem->id_service;
        $serviceitem->destroy($id);
        // $purchase = Purchase::find($purchase_id);
        return redirect(route('serviceOut.items',$service_id))->with('success',trans('main.Item Deleted Successfully'));
    }
}
