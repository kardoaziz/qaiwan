<?php

namespace App\Http\Controllers;

use App\ItemSize;
use Illuminate\Http\Request;

class ItemSizeController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        // return "sizes";
        return view('item_size.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('item_size.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $itemSize = new ItemSize();
        $itemSize->name = $request->name;

        $itemSize->save();
        return redirect(route('itemSize.index'))->with('success', trans('main.item size Added Successfully'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ItemModel  $itemModel
     * @return \Illuminate\Http\Response
     */
    public function show(ItemModel $itemModel)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ItemModel  $itemModel
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $itemSize = ItemSize::find($id);
        return view('item_size.edit',compact('itemSize'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ItemModel  $itemModel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $itemSize =  ItemSize::find($id);
        $itemSize->name = $request->name;

        $itemSize->save();
        return redirect(route('itemSize.index'))->with('success', trans('main.item size updated Successfully'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ItemModel  $itemModel
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $itemSize = ItemSize::find($id);
        if(sizeof($itemSize->items)>0)
        {
            return redirect(route('itemSize.index'))->with('warning', trans('main.item model can not be Deleted '));
        }
        $itemSize->destroy($id);
        return redirect(route('itemSize.index'))->with('success', trans('main.item size Deleted Successfully'));
    }
}
