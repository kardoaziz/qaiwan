<?php

namespace App\Http\Controllers;

use App\Projects;
use App\ServiceOut;
use App\ServiceOutItems;
use App\ProjectPayment;
use Illuminate\Http\Request;

class ProjectsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('projects.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('projects.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $projects = new Projects();
        $projects->name = $request->name;
        $projects->phone = $request->phone;
        $projects->manager = $request->manager;
        $projects->location = $request->location;
        $projects->note = $request->note;

        $projects->save();
        return view('projects.index')->with('success', trans('main.Projects Added Successfully'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Projects  $projects
     * @return \Illuminate\Http\Response
     */
    public function show(Projects $projects)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Projects  $projects
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $projects = Projects::find($id);
        return view('projects.edit',compact('projects'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Projects  $projects
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $projects =  projects::find($id);
        $projects->name = $request->name;
        $projects->manager = $request->manager;
        $projects->phone = $request->phone;
        $projects->location = $request->location;
        $projects->note = $request->note;

        $projects->save();
        return redirect(route('projects.index'))->with('success', trans('main.Projects Updated Successfully'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Projects  $projects
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Projects::where('id',$id)->delete();
        $serviceout=ServiceOut::where('id_project',$id)->get();
        foreach($serviceout as $so){
            // 
            ServiceOutItems::where('id_project',$so->id)->delete();
        }
        ServiceOut::where('id_projects',$id)->delete();
        ProjectPayment::where('id_project',$id)->delete();



        return  redirect()->back()->with('danger','data deleted');
    }
}
