<?php

namespace App\Http\Controllers;

use App\ItemModel;
use Illuminate\Http\Request;

class ItemModelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('item_model.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('item_model.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $itemModel = new ItemModel();
        $itemModel->name = $request->name;

        $itemModel->save();
        return redirect(route('itemModel.index'))->with('success', trans('main.item model Added Successfully'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ItemModel  $itemModel
     * @return \Illuminate\Http\Response
     */
    public function show(ItemModel $itemModel)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ItemModel  $itemModel
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $itemModel = ItemModel::find($id);
        return view('item_model.edit',compact('itemModel'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ItemModel  $itemModel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $itemModel =  ItemModel::find($id);
        $itemModel->name = $request->name;

        $itemModel->save();
        return redirect(route('itemModel.index'))->with('success', trans('main.item model updated Successfully'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ItemModel  $itemModel
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $itemModel = ItemModel::find($id);
        if(sizeof($itemModel->items)>0)
        {
            return redirect(route('itemModel.index'))->with('warning', trans('main.item model can not be Deleted '));
        }
        else{$itemModel->destroy($id);
        return redirect(route('itemModel.index'))->with('success', trans('main.item model Deleted Successfully'));
    }
    }
}
