<?php

namespace App\Http\Controllers;

use App\Drive;
use Illuminate\Http\Request;

class DriveController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        //
        return view('drives.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('drives.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $old_drive = Drive::where('driver_id',$request->driver_id)->where('status','active')->first();
        if($old_drive){
            $old_drive->status='replaced';
            $old_drive->save();
        }

        // $old_drive = Drive::where('truck_id',$request->truck_id)->where('status','active')->first();
        // if($old_drive){
        //     $old_drive->status='replaced';
        //     $old_drive->save();
        // }

        $drive = new Drive();
        $drive->driver_id = $request->driver_id;
        $drive->truck_id = $request->truck_id;
        $drive->old_balance = $request->old_balance;
        $drive->date = $request->date;
        $drive->note = $request->note;
        $drive->save();
        return redirect(route('drives.index'))->with('success', trans('main.Driver Added Successfully'));

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Drive  $drive
     * @return \Illuminate\Http\Response
     */
    public function show(Drive $drive)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Drive  $drive
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $drive = Drive::find($id);
        return view('drives.edit',compact('drive'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Drive  $drive
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $drive =  Drive::find($id);
        $old_drive = Drive::where('driver_id',$request->driver_id)->where('status','active')->where('id','<>',$id)->first();
        if($old_drive){
            $old_drive->status='replaced';
            $old_drive->save();
        }

        // $old_drive = Drive::where('truck_id',$request->truck_id)->where('status','active')->where('id','<>',$drive->id)->first();
        // if($old_drive){
        //     $old_drive->status='replaced';
        //     $old_drive->save();
        // }


        $drive->driver_id = $request->driver_id;
        $drive->truck_id = $request->truck_id;
        $drive->old_balance = $request->old_balance;
        $drive->date = $request->date;
        $drive->note = $request->note;
        $drive->status = $request->status;
        $drive->save();
        return redirect(route('drives.index'))->with('success', trans('main.Driver Updated Successfully'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Drive  $drive
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //

        $item = Drive::find($id);
        if(sizeof($item->trips)>0 || sizeof($item->services)>0 || sizeof($item->gass)>0 || sizeof($item->checks)>0 || sizeof($item->payments)>0)
        {
            return redirect(route('drives.index'))->with('warning', trans('main.Driver can not be Deleted '));
        }
        $item->destroy($id);
        return redirect(route('drives.index'))->with('success', trans('main.Driver Deleted Successfully'));
    }
}
