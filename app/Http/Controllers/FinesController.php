<?php

namespace App\Http\Controllers;

use App\Fines;
use App\FineDates;
use Carbon\Carbon;
use Illuminate\Http\Request;

class FinesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('fines.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
                return view('fines.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        // return $request;
        $fineDate=new FineDates();
        $fineDate->date=Carbon::now();
        $fineDate->save();

            foreach($request->unit as $key => $val){
                $data =array(
                    'unit' => $request->unit[$key],
                    'normal' => $request->normal[$key],
                    'fine' => $request->fine[$key],
                    'date_id' => $fineDate->id,
                );
                Fines::insert($data);
            }
            return redirect(route('fines.index'))->with('success', trans('main.Fines added Successfully'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Fines  $fines
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $data=FineDates::find($id);
       $fines=Fines::where('date_id',$id)->get();
       return view('fines.show',['data'=>$data,'fines'=>$fines]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Fines  $fines
     * @return \Illuminate\Http\Response
     */
    public function edit(Fines $fines)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Fines  $fines
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Fines $fines)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Fines  $fines
     * @return \Illuminate\Http\Response
     */
    public function destroy(Fines $fines)
    {
        //
    }
}
