<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Suppliers extends Model
{
    //
    public function purchases()
    {
        return $this->hasMany('App\Purchase','supplier_id');
    }
}
