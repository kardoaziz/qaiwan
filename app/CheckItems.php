<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CheckItems extends Model
{
    //
    public function check()
    {
        return $this->belongsTo('App\Check','check_id');
    }
    public function item()
    {
        return $this->belongsTo('App\Items','item_id');
    }
}
