<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExpenseType extends Model
{
    //
     public function trips()
    {
        return $this->hasMany('App\TripExpense','expense_type_id');
    }
}
