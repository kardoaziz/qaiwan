<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Projects extends Model
{
    //
    public function service()
    {
        return $this->hasMany('App\ServiceOut','id_project');
    }
}
