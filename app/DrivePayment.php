<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DrivePayment extends Model
{
    //
     public function driven()
    {
    	return $this->belongsTo('App\Drive','drive_id');
    }
}
