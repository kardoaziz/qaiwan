<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TripExpense extends Model
{
    //
    public function type()
    {
    	return $this->belongsTo('App\ExpenseType','expense_type_id');
    }
}
