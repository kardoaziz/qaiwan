<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LocationCost extends Model
{
    public function costDate()
    {
        return $this->belongsTo('App\CostDate','id_date');
    }
    public function from_location()
    {
        return $this->belongsTo('App\locations','from_id');
    }
    public function to_location()
    {
        return $this->belongsTo('App\locations','to_id');
    }
}
