<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Purchase extends Model
{
    //
    public function supplier()
    {
        return $this->belongsTo('App\Suppliers','supplier_id');
    }
    public function warehouse() 
    {
        return $this->belongsTo('App\Warehouse','warehouse_id');
    }
    public function purchase_items()
    {
        return $this->hasMany('App\PurchaseItem');
    }
}
