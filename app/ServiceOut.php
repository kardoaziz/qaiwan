<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceOut extends Model
{
    //
    //  public function drive()
    // {
    //     return $this->belongsTo('App\Drive','drive_id');
    // }
    public function project()
    {
        return $this->belongsTo('App\Projects','id_project');
    }
}
