<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Items extends Model
{
    public function itemModel()
    {
        return $this->belongsTo('App\ItemModel','model_id');
    }
    public function itemSize()
    {
        return $this->belongsTo('App\ItemSize','size_id');
    }
     public function services()
    {
        return $this->hasMany('App\ServiceItems','item_id');
    }
    public function serviceOuts()
    {
        return $this->hasMany('App\ServiceOutItems','item_id');
    }
    public function checks()
    {
        return $this->hasMany('App\CheckItems','item_id');
    }
}
