<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Driver extends Model
{
    //
     public function drive()
    {
        return $this->hasMany('App\Drive');
    }
}
