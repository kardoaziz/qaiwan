<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Material extends Model
{
    //
    public function trips()
    {
        return $this->hasMany('App\TripMaterial','material_id');
    }
}
