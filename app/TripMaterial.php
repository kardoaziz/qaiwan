<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TripMaterial extends Model
{
    public function material()
    {
        return $this->belongsTo('App\Material','material_id');
    }
}
