<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Services extends Model
{
    //
    public function drive()
    {
        return $this->belongsTo('App\Drive','drive_id');
    }
    public function items()
    {
        return $this->hasMany('App\ServiceItems');
    }
}
