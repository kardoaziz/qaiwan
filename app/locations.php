<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class locations extends Model
{
    public function locationCost()
    {
        return $this->hasMany('App\LocationCost');
    }
}
