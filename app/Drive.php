<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Drive extends Model
{
    //
    public function driver()
    {
        return $this->belongsTo('App\Driver','driver_id');
    }
    public function truck()
    {
        return $this->belongsTo('App\Trucks','truck_id');
    }
    public function gass(){
        return $this->hasMany('App\gassQtyToTrucks');
    }
     public function trips(){
        return $this->hasMany('App\Trip','drive_id');
    }
     public function payments(){
        return $this->hasMany('App\DrivePayment','drive_id');
    }
    public function services(){
        return $this->hasMany('App\Services','drive_id');
    }
    public function checks(){
        return $this->hasMany('App\Checks','drive_id');
    }
    // public function gass(){
    //     return $this->hasMany('App\Checks','drive_id');
    // }
}
