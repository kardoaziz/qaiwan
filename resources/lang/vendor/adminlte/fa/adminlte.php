<?php

return [

    'full_name'                   => 'ناو',
    'email'                       => 'ئیمەیڵ',
    'password'                    => 'ووشەی نهێنی',
    'retype_password'             => 'دووپات کردنەوەی ووشەی نهێنی',
    'remember_me'                 => 'بیرهێنانەوە',
    'register'                    => 'خۆتۆمارکردن',
    'register_a_new_membership'   => 'تۆمارکردنی بەکارهێنەری نوێ',
    'i_forgot_my_password'        => 'ووشەی نهێنیت لەبیر کردووە',
    'i_already_have_a_membership' => 'پێشتر تۆمار کراوە',
    'sign_in'                     => 'چوونەژوورەوە',
    'log_out'                     => 'چوونەدەرەوە',
    'toggle_navigation'           => 'ناڤیگەشن',
    'login_message'               => 'نامەی چوونەژوورەوە',
    'register_message'            => 'نامەی خۆتۆمارکردن',
    'password_reset_message'      => 'نامەی ڕێکخستنەوەی ووشەی نهێنی',
    'reset_password'              => 'رێکخستنەوەی ووشەی نهێنی',
    'send_password_reset_link'    => 'ناردنی بەستەری ڕێکخستنەوەی ووشەی نهێنی',
];
