@extends('adminlte::page')
@section('head_scripts')
    <!-- DataTables CSS -->
    {{-- {{ HTML::style('css/plugins/dataTables.bootstrap.css') }} --}}
@stop
@section('content')
 <div class="modal-content">
      <div class="modal-header">
        {{-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button> --}}
        <h4 class="modal-title" align="center"><b> {{ __("main.update") }}  {{ __("main.service") }}</b></h4>
      </div>
      <div class="modal-body" style="overflow-y: scroll;">
        <form action="{{ route('serviceOut.update',$service->id) }}" method="POST" enctype="multipart/form-data">
                {{csrf_field()}}
                  <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
<input type="hidden" name="_method" value="PUT">
          <div class="form-group">
                <label>{{ __("main.project") }}</label>
                <input type="hidden" name="project_id" id="project_id" value="{{ $service->id_project }}">
                <input list="projectslist" name="projects" id="projects" class="form-control" onchange="selectproject(this.value)" required value="{{ $service->project->id }}">
                <datalist id="projectslist">
                  @foreach(App\Projects::all() as $c)
                  <option value="{{ $c->id }}-{{ $c->name }}">{{ $c->name }} </option>
                  @endforeach
                </datalist>

              </div>
 

          <div class="form-group">
            <input type="text" name="project" id="project" readonly value="{{ $service->project->name }}">
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1"> {{ __("main.plate") }}</label>
              <input type="text" id="plate" name="plate" class="form-control" placeholder="plate" required value="{{ $service->plate }}">
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1"> {{ __("main.date") }}</label>
              <input type="date" id="date" name="date" class="form-control" placeholder="Date" required value="{{ $service->date }}">
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1"> {{ __("main.invoice_no") }}</label>
              <input type="text" id="invoice_no" name="invoice_no" class="form-control" placeholder="{{ __('main.invoice_no') }}" value="{{ $service->invoice_no }}">
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1"> {{ __("main.fee") }}</label>
              <input type="text" id="fee" name="fee" class="form-control" placeholder="{{ __('main.fee') }}" value="{{ $service->fee }}">
          </div>
         {{--  <div class="form-group">
            <label for="exampleInputEmail1"> {{ __("main.current mileage") }}</label>
              <input type="text" id="mileage" name="mileage" class="form-control" placeholder="{{ __('main.mileage') }}" >
          </div> --}}
        
         {{--  <div class="form-group">
            <label for="exampleInputEmail1"> {{ __("main.discount") }}</label>
              <input type="number" id="discount" name="discount" class="form-control" placeholder="{{ __('main.discount') }}" required value='0'>
          </div> --}}
          <div class="form-group">
            <label for="exampleInputEmail1"> {{ __("main.note") }}</label>
              <input type="text" id="note" name="note" class="form-control" placeholder="{{ __('main.note') }}"  >
          </div>


          <button type="submit" class="btn btn-primary"> {{ __("main.add") }}</button>
        </form>
      </div>
        {{-- <div class="modal-footer">
            <button type="button" class="btn btn-default pull-left" data-dismiss="modal"> {{ __("main.cancel") }}</button>

          </div> --}}
    </div>
    @stop