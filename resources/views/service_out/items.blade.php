@extends('adminlte::page')
@section('head_scripts')
    <!-- DataTables CSS -->
    {{-- {{ HTML::style('css/plugins/dataTables.bootstrap.css') }} --}}
@stop
@section('content')
<style type="text/css">
  .dataTables_info{
    display: none;
  }
</style>
<h3>{{ __('main.items') }}</h3>
{{-- <a  href="{{ route('items.create') }}"><button type="button" class="btn btn-success"><i class="fa fa-fw fa-plus"></i>{{ __('main.new') }}</button></a> --}}
<div class="card card-success">
    <div class="card-body">
        <div class="row">
            <div class="col-md-2">{{ __('main.project') }}</div>
            <div class="col-md-4"><h3>{{ $service->project->name }}</h3></div>
            <div class="col-md-2">{{ __('main.plate') }}</div>
            <div class="col-md-4"><h3>{{ $service->plate }}</h3></div>
        </div>

        <hr>
        <div class="row">
            <div class="col-md-2">{{ __('main.invoice_no') }}</div>
            <div class="col-md-4">{{ $service->invoice_no }}</div>
            <div class="col-md-2">{{ __('main.date') }}</div>
            <div class="col-md-4">{{ $service->date }}</div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-2">{{ __('main.note') }}</div>
            <div class="col-md-4">{{ $service->note }}</div>
            <div class="col-md-2">{{ __('main.fee') }}</div>
            <div class="col-md-4">{{ number_format($service->fee,2) }}</div>
        </div>
        <div class="row">

        </div>
    </div>
</div>
<div class="card card-success responsive">
    <div class="card-body">
<table  class="display stripe table table-bordered table-hover" style="width:100%">
        <thead>
            <tr>
                <th>#</th>
                <th>{{ __('main.item') }}</th>
                <th>{{ __('main.code') }}</th>
                <th>{{ __('main.qty') }}</th>
                <th>{{ __('main.price') }}</th>
                <th>{{ __('main.note') }}</th>
                {{-- <th>{{ __('main.edit') }}</th> --}}
                <th>{{ __('main.delete') }}</th>
            </tr>
        </thead>
        <tbody>
        	@foreach(App\ServiceOutItems::where('id_service',$service->id)->get() as $d)
            <tr>
                <td>{{ $loop->iteration }}</td>
                <td>{{ $d->item->name }}</td>
                <td>{{ $d->item->code }}</td>
                <td><input type="number" step="0.01" value="{{ $d->qty }}" style="width:100%; margin:0px;text-align: center;" onblur="updateitem('qty',{{ $d->id }},this.value)"></td>
                <td><input type="number" step="0.01" value="{{ $d->price }}" style="width:100%; margin:0px;text-align: center;" onblur="updateitem('price',{{ $d->id }},this.value)"  ></td>
                <td><input type="text"  value="{{ $d->note }}" style="width:100%; margin:0px;text-align: center;"onblur="updateitem('note',{{ $d->id }},this.value)" ></td>

                <td>
                                <form action="{{ route('service_out_items.destroy',$d->id) }}" method="post">
                         {{ csrf_field() }}
                           <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                <input type="hidden" name="_method" value="DELETE"  >

                    <a style="padding: 0px;
                      font-size: 12px;
                      color: red;" role="menuitem" tabindex="-1"  onclick="return confirm('{{ __('main.are you sure') }}')">
                      <button type="submit" class="btn btn-sm btn-danger"><i class="fa fa-fw fa-trash"></i></button>
                    </a>
                  </form>
                  </td>
            </tr>
             @endforeach

        </tbody>
        <tfoot>
            <tr><td colspan="3">{{ __('main.total_item') }}</td>
                <td><input style="width:100%;text-align: center;" type="number"   id="total_qty" value="{{ App\ServiceOutItems::where('id_service',$service->id)->sum('qty') }}"   disabled="true" ></td>

                <td colspan="2"></td>
                </tr>
        </tfoot>
    </table>
    {{-- <hr>

    <div class="row text-center">
        <div class="col-md-2"><h4>{{ __('main.new') }}</h4></div>
        <div class="col-md-2"> <input list="itemslist" name="item_id" id="item_id" class="form-control" onchange="hideList(this)" placeholder="{{ __('main.item') }}">
                <datalist id="itemslist">
                  @foreach(App\Items::all() as $c)
                  <option value="{{ $c->id }}">{{ $c->name }}-{{ $c->code }} </option>
                  @endforeach
                </datalist></div>
                <div class="col-md-2"><input type="number" step="0.01" name="qty" placeholder="{{ __('main.qty') }}" id="qty"></div>
                <div class="col-md-2"><input type="number" step="0.01" name="price" placeholder="{{ __('main.price') }}" id="price"></div>
                <div class="col-md-2"><input type="text"  name="invoice_no" placeholder="{{ __('main.invoice_no') }}" id="invoice_no"></div>
                <div class="col-md-2"><input type="text"  name="note" placeholder="{{ __('main.note') }}" id="note"></div>
                <div class="col-md-2"><button onclick="additem({{ $service->id }})" type="submit" class="btn btn-success"><i class="fa fa-fw fa-save" ></i>{{ __('main.add') }}</button></div>
    </div> --}}
{{-- </form> --}}
     </div>
 </div>


<div class="card card-light">
    <div class="card-body">
        <div class="table-responsive">
        <table id="example1" class="table table-hover" >
                <thead class="thead-dark">
                    <tr>
                        <th>#</th>
                        <th>{{ __('main.add') }}</th>
                        <th>{{ __('main.name') }}</th>
                        <th>{{ __('main.code') }}</th>
                        <th>{{ __('main.location') }}</th>
                        <th>{{ __('main.description') }}</th>
                        <th>{{ __('main.note') }}</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach(App\Items::where('model_id',2)->get() as $d)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td><button type="button" class="btn btn-block btn-success" onclick="addItem({{ $service->id }},{{ $d->id }})"> <i class="fa fa-plus"></i> {{ __("main.add") }}</button></td>
                        <td>{{ $d->name }}</td>
                        <td>{{ $d->code }}</td>
                        <td>{{ $d->location }}</td>
                        <td>{{ $d->description }}</td>
                        <td>{{ $d->note }}</td>
                    </tr>
                     @endforeach
                </tbody>
            </table>
        </div>
        </div>
</div>



<script type="text/javascript">
    function additem(service_id)
{
  //
  console.log(service_id);
  var item_id = document.getElementById('item_id').value;
  // var qty = document.getElementById('qty').value;
  // var invoice_no = document.getElementById('invoice_no').value;
  // var price = document.getElementById('price').value;
  // var note = document.getElementById('note').value;
  // console.log(item_id);
  // console.log(qty);
  // console.log(customer_id);
  $.ajax({
          type:'POST',
          headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
          url:'{{ route("service_out_items.store") }}',
          data:{ service_id:service_id, item_id:item_id},
           success:function(data){
            console.log(data);
            location.reload();
           }
        });
}
function addItem(service_id,item_id)
{
  console.log(service_id);
  // var item_id = document.getElementById('item_id').value;
  var qty = 0;//document.getElementById('qty').value;
  // var invoice_no = document.getElementById('invoice_no').value;
  var price =0;// document.getElementById('price').value;
  var note = '';//document.getElementById('note').value;
  // console.log(item_id);
  // console.log(qty);
  // console.log(customer_id);
  $.ajax({
          type:'POST',
          headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
          url:'{{ route("service_out_items.store") }}',
          data:{ service_id:service_id, item_id:item_id, qty:qty,price:price, note:note},
           success:function(data){
            console.log(data);
            location.reload();
           }
        });
}
function updateitem(field,id,val)
{
  console.log(field);
  console.log(id);
  console.log(val);

  $.ajax({
          type:'POST',
          headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
          url:'{{ route("service_out_items.updatee") }}',
          data:{ id:id, field:field,val:val},
           success:function(data){
            console.log(data);
            // console.log(data['total_qty']);
            // console.log(data['total']);
            // document.getElementById("total_qty").value =data['total_qty'];
            // document.getElementById("total").value =data['total'];
           }
        });
}
</script>

@stop
