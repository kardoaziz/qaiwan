@extends('adminlte::page')
@section('head_scripts')
    <!-- DataTables CSS -->
    {{-- {{ HTML::style('css/plugins/dataTables.bootstrap.css') }} --}}
@stop 
@section('content')
<h3>{{ __('main.services out') }}</h3>
    <div class="card card-light">
        <div class="card-header bg-info">
          @can('add_project_service')  <a class="btn btn-light noprint" data-toggle="modal" data-target="#new-service-modal" style="font-size: 18px;float:right; color:black !important;">
                <i class="fa fa-plus"></i> {{ __("main.new") }}
              </a>@endcan
        </div>
        <div class="card-body">
            <div class="table-responsive">
    <table id="example" class="table table-hover ">
        <thead class="">
            <tr>
                <th>#</th>
                <th>{{ __('main.project') }}</th>
                <th>{{ __('main.plate') }}</th>
                <th>{{ __('main.date') }}</th>
                <th>{{ __('main.invoice_no') }}</th>
                {{-- <th>{{ __('main.discount') }}</th> --}}
                <th>{{ __('main.total') }}</th>
                <th>{{ __('main.total cost') }}</th>
                {{-- <th>{{ __('main.total cost $') }}</th> --}}
                {{-- <th>{{ __('main.invoice_no') }}</th> --}}
                <th>{{ __('main.note') }}</th>
                <th>{{ __('main.items') }}</th>
                @can('edit_project_service') <th>{{ __('main.edit') }}</th>@endcan
                 @can('delete_project_service')<th>{{ __('main.delete') }}</th>@endcan
            </tr>
        </thead>
        <tbody>
          @php
          // echo $id;
          if(isset($id)) $services = App\ServiceOut::where('id_project',$id)->orderby('id','desc')->get(); 
          else $services = App\ServiceOut::orderby('id','desc')->get(); 
          @endphp
        	@foreach($services as $d)
            <tr>
                <td>{{ $loop->iteration }}</td>
                <td>{{ $d->project->name }}</td>
                <td>{{ $d->plate }}</td>
                <td>{{ $d->date }}</td>
                <td>{{ $d->invoice_no }}</td>
                <td>{{ number_format( App\ServiceOutItems::where('id_service',$d->id)->select(DB::raw('sum(qty) as total'))->first()->total,2) }} </td>
                <td>{{ number_format( App\ServiceOutItems::where('id_service',$d->id)->select(DB::raw('sum(qty*price) as total'))->first()->total+$d->fee,2) }} </td>
                {{-- <td>{{ number_format( App\ServiceOutItems::where('id_service',$d->id)->select(DB::raw('sum(qty*dollar) as total'))->first()->total,2) }} </td> --}}
                <td>{{ $d->note }}</td>
                 @can('view_project_service')<td>
                   <form method="GET" action="{{ route('serviceOut.items',$d->id) }}">
                                    {{ csrf_field() }}
  <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                    <div class="form-group">
                                        <input type="submit" class="btn btn-warning btn-sm " value="{{ __('main.items') }}" >

                                    </div>
                                </form></td>@endcan
                 @can('edit_project_service')<td>
                   <form method="GET" action="{{ route('serviceOut.edit',$d->id) }}">
                                    {{ csrf_field() }}
  <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

                                    <div class="form-group">
                                        <input type="submit" class="btn btn-info btn-sm " value="{{ __('main.edit') }}" >

                                    </div>
                                </form></td>@endcan
                 @can('delete_project_service')<td>
                                <form action="{{ route('serviceOut.destroy',$d->id) }}" method="post">
                         {{ csrf_field() }}
                           <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                <input type="hidden" name="_method" value="DELETE"  >

                    <a style="padding: 0px;
                      font-size: 12px;
                      color: red;" role="menuitem" tabindex="-1"  onclick="return confirm('{{ __('main.are you sure') }}')">
                      <button type="submit" class="btn btn-sm btn-danger"><i class="fa fa-fw fa-trash"></i></button>
                    </a>
                  </form>
                  </td>@endcan
            </tr>
             @endforeach
        </tbody>

    </table>
</div>
<div class="modal fade" id="new-service-modal" >
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" align="center"><b> {{ __("main.new") }}  {{ __("main.service") }}</b></h4>
      </div>
      <div class="modal-body" style="overflow-y: scroll;">
        <form action="{{ route('serviceOut.store') }}" method="POST" enctype="multipart/form-data">
                {{csrf_field()}}
          <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
          <div class="form-group">
                <label>{{ __("main.project") }}</label>
                <input type="hidden" name="project_id" id="project_id">
                <input list="projectslist" name="projects" id="projects" class="form-control" onchange="selectproject(this.value)" required>
                <datalist id="projectslist">
                  @foreach(App\Projects::all() as $c)
                  <option value="{{ $c->id }}-{{ $c->name }}">{{ $c->name }} </option>
                  @endforeach
                </datalist>

              </div>
 

          <div class="form-group">
            <input type="text" name="project" id="project" readonly>
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1"> {{ __("main.plate") }}</label>
              <input type="text" id="plate" name="plate" class="form-control" placeholder="plate" required >
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1"> {{ __("main.date") }}</label>
              <input type="date" id="date" name="date" class="form-control" placeholder="Date" required value="{{date('Y-m-d', strtotime('+0 day'))}}">
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1"> {{ __("main.invoice_no") }}</label>
              <input type="text" id="invoice_no" name="invoice_no" class="form-control" placeholder="{{ __('main.invoice_no') }}" >
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1"> {{ __("main.fee") }}</label>
              <input type="text" id="fee" name="fee" class="form-control" placeholder="{{ __('main.fee') }}" value="0">
          </div>
         {{--  <div class="form-group">
            <label for="exampleInputEmail1"> {{ __("main.current mileage") }}</label>
              <input type="text" id="mileage" name="mileage" class="form-control" placeholder="{{ __('main.mileage') }}" >
          </div> --}}
        
         {{--  <div class="form-group">
            <label for="exampleInputEmail1"> {{ __("main.discount") }}</label>
              <input type="number" id="discount" name="discount" class="form-control" placeholder="{{ __('main.discount') }}" required value='0'>
          </div> --}}
          <div class="form-group">
            <label for="exampleInputEmail1"> {{ __("main.note") }}</label>
              <input type="text" id="note" name="note" class="form-control" placeholder="{{ __('main.note') }}"  >
          </div>


          <button type="submit" class="btn btn-primary"> {{ __("main.add") }}</button>
        </form>
      </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default pull-left" data-dismiss="modal"> {{ __("main.cancel") }}</button>

          </div>
    </div>
  </div>
</div>
        </div>
    </div>
    <script type="text/javascript">
      function selectproject(id) {
        // body...
        $('#project_id').val(id.split("-")[0]);
    $('#project').val(id.split("-")[1]);
      }
    </script>
@stop
