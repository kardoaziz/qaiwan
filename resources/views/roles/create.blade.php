@extends('layouts.app')

@section('title', '| Add Role')

@section('content')

<div class='col-lg-12 col-lg-offset-12'>

    <h1><i class='fa fa-key'></i> Add Role</h1>
    <hr>

    {{ Form::open(array('url' => 'roles')) }}

    <div class="form-group">
        {{ Form::label('name', 'Name') }}
        {{ Form::text('name', null, array('class' => 'form-control')) }}
    </div>

    <h5><b>Assign Permissions</b></h5>
<div class="row">
    
        @foreach ($permissions as $permission)
        <div class="col-md-3">
            <div class='form-group'>
                <input type="checkbox" name="permissions[]" value="{{ $permission->id }}">
                <label>{{ trans('main.'.$permission->name) }}</label>
              {{--   {{ Form::checkbox('permissions[]',  $permission->id ) }}
                {{ Form::label(trans('main.'.$permission->name), ucfirst(trans('main.'.$permission->name))) }}<br> --}}
            </div>
        </div>
        @endforeach
    </div>
</div>

    {{ Form::submit('Add', array('class' => 'btn btn-primary')) }}

    {{ Form::close() }}

</div>

@endsection
