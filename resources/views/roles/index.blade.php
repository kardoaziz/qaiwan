{{-- \resources\views\roles\index.blade.php --}}
@extends('layouts.app')

@section('title', '| Roles')

@section('content')

<div class="col-lg-10 col-lg-offset-1">
    <h1><i class="fa fa-key"></i> Roles

   @can('view_users') <a href="{{ route('users.index') }}" class="btn btn-default pull-right">Users</a>@endcan
   {{--  <a href="{{ route('permissions.index') }}" class="btn btn-default pull-right">Permissions</a> --}}</h1>
    <hr>
    <div class="table-responsive">
        <table class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>Role</th>
                    <th style="width:500px;">Permissions</th>
                    <th>Operation</th>
                </tr>
            </thead>

            <tbody>
                @foreach ($roles as $role)
                <tr>
@php
$perms = $role->permissions()->pluck('name');
$perm = [];

for ($i=0; $i <sizeof($perms) ; $i++) { 
    # code...
    array_push($perm, trans('main.'.$perms[$i]));
}

@endphp
                    <td>{{ $role->name }}</td>

                    <td style="width:500px;word-wrap: break-word;
    word-break: break-all;
    white-space: normal;">@php echo str_replace(array('[',']','"'),',',implode(" , ",$perm)); @endphp</td>
    {{-- Retrieve array of permissions associated to a role and convert to string --}}
                    <td>
                   @can('edit_role') <a href="{{ URL::to('roles/'.$role->id.'/edit') }}" class="btn btn-info pull-left" style="margin-right: 3px;">Edit</a>@endcan

                    @can('delete_role'){!! Form::open(['method' => 'DELETE', 'route' => ['roles.destroy', $role->id] ]) !!}
                    {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                    {!! Form::close() !!}
                    @endcan
                    </td>
                </tr>
                @endforeach
            </tbody>

        </table>
    </div>

    <a href="{{ URL::to('roles/create') }}" class="btn btn-success">Add Role</a>

</div>

@endsection
