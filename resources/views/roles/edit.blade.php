@extends('layouts.app')

@section('title', '| Edit Role')

@section('content')

<div class='col-lg-12 col-lg-offset-12'>
    <h1><i class='fa fa-key'></i> Edit Role: {{$role->name}}</h1>
    <hr>

    {{ Form::model($role, array('route' => array('roles.update', $role->id), 'method' => 'PUT')) }}

    <div class="form-group">
        {{ Form::label('name', 'Role Name') }}
        {{ Form::text('name', null, array('class' => 'form-control')) }}
    </div>

    <h5><b>Assign Permissions</b></h5>
    <div class="row">
    
        @foreach ($permissions as $permission)
        <div class="col-md-3">
            <div class='form-group'>
                <input type="checkbox" name="permissions[]" value="{{ $permission->id }}" @if($role->hasPermissionTo($permission->name)) checked @endif>
                <label>{{ trans('main.'.$permission->name) }}</label>
              {{--   {{ Form::checkbox('permissions[]',  $permission->id ) }}
                {{ Form::label(trans('main.'.$permission->name), ucfirst(trans('main.'.$permission->name))) }}<br> --}}
            </div>
        </div>
        @endforeach
    </div>
    <br>
    {{ Form::submit('Edit', array('class' => 'btn btn-primary')) }}

    {{ Form::close() }}
</div>

@endsection
