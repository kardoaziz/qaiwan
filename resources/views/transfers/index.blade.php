@extends('adminlte::page')
@section('head_scripts')
    <!-- DataTables CSS -->
    {{-- {{ HTML::style('css/plugins/dataTables.bootstrap.css') }} --}}
@stop
@section('content_header')
<meta name="csrf-token" content="{{ Session::token() }}">
@stop
@section('content')
<style>
  .badge{
    font-size:20px;
  }
</style>
<h3>{{ __('main.transfers') }}</h3>
<div class="card card-light">
    <div class="card-header bg-info">
         @can('add_transfer')<a class="btn btn-light noprint" data-toggle="modal" data-target="#new-transfer-modal" style="font-size: 18px;float:right;color:grey !important;">
                <i class="fa fa-plus"></i> {{ __("main.new") }}
              </a>@endcan
    </div>
    <div class="card-body">
        <div class="table-responsive">
        <table id="example" class="table table-hover" >
                <thead class="table-primary">
                    <tr>
                        <th>#</th>
                        {{-- <th>{{ __('main.date') }}</th> --}}
                        <th>{{ __('main.from bank') }}</th>
                        <th>{{ __('main.to bank') }}</th>
                        <th>{{ __('main.date') }}</th>
                        <th>{{ __('main.amount') }}</th>
                        <th>{{ __('main.dollar') }}</th>
                        <th>{{ __('main.status') }}</th>
                        <th>{{ __('main.note') }}</th>
                        
                         @can('edit_transfer')<th>{{ __('main.edit') }}</th>@endcan
                        @can('delete_transfer') <th>{{ __('main.delete') }}</th>@endcan
                    </tr>
                </thead>
                <tbody>
                    @php
                    $items = App\BankTransfer::orderby('created_at','desc')->get();
                    @endphp
                    @foreach($items as $d)
                    <tr>
                       
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $d->from_bank->name }}</td>
                        <td>{{ $d->to_bank->name }}</td>
                        <td>{{ $d->date }}</td>
                        <td>{{ number_format($d->amount,2) }}</td>
                        <td>{{ number_format($d->dollar,2) }}</td>
                        <td>@if($d->status=="recieved") 
                              <span class="badge badge-pill badge-success">{{ trans('main.'.$d->status) }}</span>
                                
                            @elseif($d->status=="rejected")
                            <span class="badge badge-pill badge-warning">{{ trans('main.'.$d->status) }}</span>
                            @else
                            
                            <div class="dropdown">
                              <button class="btn btn-danger dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                {{ $d->status }}
                              </button>
                              <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" onclick="changestatus({{ $d->id }},'rejected')">{{ __('main.rejected') }}</a>
                                <a class="dropdown-item" onclick="changestatus({{ $d->id }},'recieved')">{{ __('main.recieved') }}</a>
                               {{--  <a class="dropdown-item" onclick="changestatus('rejected')">Something else here</a> --}}
                              </div>
                            </div>

                            @endif
                              </td>
                        <td>{{ $d->note }}</td>
                        
                         @can('edit_transfer')<td>
                           @if($d->status=="recieved" || $d->status=="rejected")
                           <button disabled=""><i class="fa fa-fw fa-pen"></i></button>
                           @else<form method="GET" action="{{ route('transfers.edit',$d->id) }}">
                                            {{ csrf_field() }}
  <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                            <div class="form-group">
                                                <input type="submit" class="btn btn-info btn-sm " value="{{ __('main.edit') }}" >

                                            </div>
                                        </form>@endif </td>@endcan
                       @can('delete_transfer')  <td>
                                       @if($d->status=="recieved" || $d->status=="rejected")
                           <button disabled=""><i class="fa fa-fw fa-trash"></i></button>
                           @else <form action="{{ route('transfers.destroy',$d->id) }}" method="post">
                                 {{ csrf_field() }}
                                   <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                        <input type="hidden" name="_method" value="DELETE"  >

                            <a style="padding: 0px;
                              font-size: 12px;
                              color: red;" role="menuitem" tabindex="-1"  onclick="return confirm('{{ __('main.are you sure') }}')">
                              <button type="submit" class="btn btn-sm btn-danger"><i class="fa fa-fw fa-trash"></i></button>
                            </a>
                          </form>@endif
                          </td>@endcan
                    </tr>
                     @endforeach
                     
                </tbody>
                <tfoot>
                    <tr>
                       
                        <td></td>
                        <td colspan="2">{{ __('main.total') }}</td>
                        <td></td>
                        <td>{{ App\BankTransfer::sum('amount') }}</td>
                        <td>{{ App\BankTransfer::sum('dollar') }}</td>
                        <td></td>
                        <td></td>
                      </tr>
                </tfoot>
            </table>
        </div>
        </div>
</div>
<div class="modal fade" id="new-transfer-modal" >
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" align="center"><b> {{ __("main.new") }}  {{ __("main.transfer") }}</b></h4>
      </div>
      <div class="modal-body" style="overflow-y: scroll;">
        <form action="{{ route('transfers.store') }}" method="POST" enctype="multipart/form-data">
                {{csrf_field()}}
          <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
          

          <div class="form-group">
            <label for="exampleInputEmail1"> {{ __("main.date") }}</label>
              <input type="date" id="date" name="date" class="form-control" placeholder="date" required >
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1"> {{ __("main.from snduq") }}</label>
              <select class="form-control" name="snduq_id_from">
                  @foreach(App\Snduq::all() as $s)
                  <option value="{{ $s->id }}">{{ $s->name }}</option>
                  @endforeach
              </select>
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1"> {{ __("main.to snduq") }}</label>
              <select class="form-control" name="snduq_id_to">
                  @foreach(App\Snduq::all() as $s)
                  <option value="{{ $s->id }}">{{ $s->name }}</option>
                  @endforeach
              </select>
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1"> {{ __("main.amount") }}</label>
              <input type="number" step="any" id="amount" name="amount" class="form-control" placeholder="{{ __('main.amount') }}"  onblur="update('amount',this.value,{{ App\DollarPrice::orderby('created_at','desc')->first()->price }})">
          </div>
           <div class="form-group">
            <label for="exampleInputEmail1"> {{ __("main.dollar") }}</label>
              <input type="number" step="any" id="dollar" name="dollar" class="form-control" placeholder="{{ __('main.dollar') }}"  onblur="update('dollar',this.value,{{ App\DollarPrice::orderby('created_at','desc')->first()->price }})">
          </div>
          
          <div class="form-group">
            <label for="exampleInputEmail1"> {{ __("main.note") }}</label>
              <input type="text" id="note" name="note" class="form-control" placeholder="{{ __('main.note') }}"  >
          </div>


          <button type="submit" class="btn btn-primary"> {{ __("main.add") }}</button>
        </form>
      </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default pull-left" data-dismiss="modal"> {{ __("main.cancel") }}</button>

          </div>
    </div>
  </div>
</div>
        </div>
    </div>
    <script type="text/javascript">
        function changestatus(id,status) {
          $.ajax({
          type:'POST',
          headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
          url:'{{ route("transfers.updatee") }}',
          data:{ id:id, status:status},
           success:function(data){
            console.log(data);
            location.reload();
            // document.getElementById("totalall").value =total-discount_a-(discount_p/100)*total;
           }
        });
        }
        function update(field,val,dollar) {
        // body...
        if(field=='amount'){
            document.getElementById('dollar').value = (val/dollar);
        }
        else if(field=='dollar')
        {
           document.getElementById('amount').value =(val*dollar);
        }
      }

    </script>
@stop
