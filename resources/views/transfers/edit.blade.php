@extends('adminlte::page')
@section('head_scripts')
    <!-- DataTables CSS -->
    {{-- {{ HTML::style('css/plugins/dataTables.bootstrap.css') }} --}}
@stop
@section('content')
<div class="modal-content">
      <div class="modal-header">
        
        <h4 class="modal-title" align="center"><b> {{ __("main.new") }}  {{ __("main.transfer") }}</b></h4>
      </div>
      <div class="modal-body" style="overflow-y: scroll;">
        <form action="{{ route('transfers.update', $transfer->id) }}" method="POST" enctype="multipart/form-data">
                {{csrf_field()}}
<input type="hidden" name="_method" value="PUT">          
  <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
          <div class="form-group">
            <label for="exampleInputEmail1"> {{ __("main.date") }}</label>
              <input type="date" id="date" name="date" class="form-control" placeholder="date" required value="{{ $transfer->date }}" >
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1"> {{ __("main.from snduq") }}</label>
              <select class="form-control" name="snduq_id_from" value="{{ $transfer->bank_id_from }}">
                  @foreach(App\Snduq::all() as $s)
                  <option value="{{ $s->id }}">{{ $s->name }}</option>
                  @endforeach
              </select>
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1"> {{ __("main.to snduq") }}</label>
              <select class="form-control" name="snduq_id_to" value="{{ $transfer->bank_id_to }}">
                  @foreach(App\Snduq::all() as $s)
                  <option value="{{ $s->id }}">{{ $s->name }}</option>
                  @endforeach
              </select>
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1"> {{ __("main.amount") }}</label>
              <input type="number" step="any" id="amount" name="amount" class="form-control" placeholder="{{ __('main.amount') }}"  onblur="update('amount',this.value,{{ App\DollarPrice::orderby('created_at','desc')->first()->price }})" value="{{ $transfer->amount }}">
          </div>
           <div class="form-group">
            <label for="exampleInputEmail1"> {{ __("main.dollar") }}</label>
              <input type="number" step="any" id="dollar" name="dollar" class="form-control" placeholder="{{ __('main.dollar') }}"  onblur="update('dollar',this.value,{{ App\DollarPrice::orderby('created_at','desc')->first()->price }})" value="{{ $transfer->dollar }}">
          </div>
          
          <div class="form-group">
            <label for="exampleInputEmail1"> {{ __("main.note") }}</label>
              <input type="text" id="note" name="note" class="form-control" placeholder="{{ __('main.note') }}"  value="{{ $transfer->note }}">
          </div>


          <button type="submit" class="btn btn-primary"> {{ __("main.update") }}</button>
        </form>
      </div>
       
    </div>
    <script type="text/javascript">
        function update(field,val,dollar) {
        // body...
        if(field=='amount'){
            document.getElementById('dollar').value = (val/dollar);
        }
        else if(field=='dollar')
        {
           document.getElementById('amount').value =(val*dollar);
        }
      }
    </script>
    @stop