@extends('adminlte::page')
@section('head_scripts')
    <!-- DataTables CSS -->
    {{-- {{ HTML::style('css/plugins/dataTables.bootstrap.css') }} --}}
@stop
@section('content')
<h3>{{ __('main.checks') }}</h3>
    <div class="card card-light">
        <div class="card-header bg-info">
             @can('add_check')<a class="btn btn-light noprint" data-toggle="modal" data-target="#new-service-modal" style="font-size: 18px;float:right; color:black !important;">
                <i class="fa fa-plus"></i> {{ __("main.new") }}
              </a>@endcan
        </div>
        <div class="card-body">
            <div class="table-responsive">
    <table id="example" class="table table-hover ">
        <thead class="table-danger">
            <tr>
                <th>#</th>
                <th>{{ __('main.code') }}</th>
                <th>{{ __('main.plate') }}</th>
                <th>{{ __('main.date') }}</th>
                {{-- <th>{{ __('main.invoice_no') }}</th> --}}
                {{-- <th>{{ __('main.discount') }}</th> --}}
                {{-- <th>{{ __('main.total') }}</th> --}}
                {{-- <th>{{ __('main.total cost') }}</th> --}}
                {{-- <th>{{ __('main.total cost $') }}</th> --}}
                {{-- <th>{{ __('main.invoice_no') }}</th> --}}
                <th>{{ __('main.note') }}</th>
                <th>{{ __('main.items') }}</th>
               @can('edit_check')<th>{{ __('main.edit') }}</th>@endcan
               @can('delete_check') <th>{{ __('main.delete') }}</th>@endcan
            </tr>
        </thead>
        <tbody>
        	@foreach(App\Checks::all() as $d)
            <tr>
                <td>{{ $loop->iteration }}</td>
                <td>{{ $d->drive->truck->code }}</td>
                <td>{{ $d->drive->truck->plate }}</td>
                <td>{{ $d->date }}</td>
               {{--  <td>{{ $d->invoice_no }}</td>
                <td>{{ number_format( App\ServiceItems::where('service_id',$d->id)->select(DB::raw('sum(qty) as total'))->first()->total,2) }} </td>
                <td>{{ number_format( App\ServiceItems::where('service_id',$d->id)->select(DB::raw('sum(qty*price) as total'))->first()->total,2) }} </td>
                <td>{{ number_format( App\ServiceItems::where('service_id',$d->id)->select(DB::raw('sum(qty*dollar) as total'))->first()->total,2) }} </td> --}}
                <td>{{ $d->note }}</td>
                @can('edit_check')<td>
                   <form method="GET" action="{{ route('checks.items',$d->id) }}">
                                    {{ csrf_field() }}
  <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                    <div class="form-group">
                                        <input type="submit" class="btn btn-warning btn-sm " value="{{ __('main.items') }}" >

                                    </div>
                                </form></td>@endcan
               @can('edit_check') <td>
                   <form method="GET" action="{{ route('checks.edit',$d->id) }}">
                                    {{ csrf_field() }}
  <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                    <div class="form-group">
                                        <input type="submit" class="btn btn-info btn-sm " value="{{ __('main.edit') }}" >

                                    </div>
                                </form></td>@endcan
                @can('delete_check')<td>
                                <form action="{{ route('checks.destroy',$d->id) }}" method="post">
                         {{ csrf_field() }}
                           <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                <input type="hidden" name="_method" value="DELETE"  >

                    <a style="padding: 0px;
                      font-size: 12px;
                      color: red;" role="menuitem" tabindex="-1"  onclick="return confirm('{{ __('main.are you sure') }}')">
                      <button type="submit" class="btn btn-sm btn-danger"><i class="fa fa-fw fa-trash"></i></button>
                    </a>
                  </form>
                  </td>@endcan
            </tr>
             @endforeach
        </tbody>
    </table>
</div>
<div class="modal fade" id="new-service-modal" >
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" align="center"><b> {{ __("main.new") }}  {{ __("main.service") }}</b></h4>
      </div>
      <div class="modal-body" style="overflow-y: scroll;">
        <form action="{{ route('checks.store') }}" method="POST" enctype="multipart/form-data">
                {{csrf_field()}}
          <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
          <div class="form-group">
                <label>{{ __("main.truck") }}</label>

                <input list="drivelist" name="drive_id" id="drives" class="form-control" onchange="hideList(this)" required>
                <datalist id="drivelist">
                  @foreach(App\Drive::where('status','active')->get() as $c)
                  <option value="{{ $c->id }}">{{ $c->truck->code }}- {{ $c->truck->plate }}- {{ $c->driver->name }}  </option>
                  @endforeach
                </datalist>

              </div>
              {{-- <div class="form-group">
                <label>{{ __("main.supplier") }}</label>

                <input list="supplierlist" name="supplier_id" id="suppliers" class="form-control" onchange="hideList(this)">
                <datalist id="supplierlist">
                  @foreach(App\Suppliers::all() as $c)
                  <option value="{{ $c->id }}">{{ $c->name }} </option>
                  @endforeach
                </datalist>

              </div> --}}

          <div class="form-group">
            <label for="exampleInputEmail1"> {{ __("main.date") }}</label>
              <input type="date" id="date" name="date" class="form-control" placeholder="Date" required value="{{date('Y-m-d', strtotime('+0 day'))}}">
          </div>
         {{--  <div class="form-group">
            <label for="exampleInputEmail1"> {{ __("main.invoice_no") }}</label>
              <input type="text" id="invoice_no" name="invoice_no" class="form-control" placeholder="{{ __('main.invoice_no') }}" >
          </div> --}}
          <div class="form-group">
            <label for="exampleInputEmail1"> {{ __("main.current mileage") }}</label>
              <input type="text" id="mileage" name="mileage" class="form-control" placeholder="{{ __('main.mileage') }}" >
          </div>
        
         {{--  <div class="form-group">
            <label for="exampleInputEmail1"> {{ __("main.discount") }}</label>
              <input type="number" id="discount" name="discount" class="form-control" placeholder="{{ __('main.discount') }}" required value='0'>
          </div> --}}
          <div class="form-group">
            <label for="exampleInputEmail1"> {{ __("main.note") }}</label>
              <input type="text" id="note" name="note" class="form-control" placeholder="{{ __('main.note') }}"  >
          </div>


          <button type="submit" class="btn btn-primary"> {{ __("main.add") }}</button>
        </form>
      </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default pull-left" data-dismiss="modal"> {{ __("main.cancel") }}</button>

          </div>
    </div>
  </div>
</div>
        </div>
    </div>
@stop
