{{-- resources/views/admin/dashboard.blade.php --}}

@extends('layouts.app')

@section('title', 'Dashboard')

@if(Session::has('flash_message'))
<div class="container">
    <div class="alert alert-success"><em> {!! session('flash_message') !!}</em>
    </div>
</div>
@endif

<div class="row">
<div class="col-md-8 col-md-offset-2">
    {{-- @include ('errors.list') Including error file --}}
</div>
</div>

@section('content_header')
    <h1>پەڕەی سەرەکی</h1>
@stop

@section('content')
<style type="text/css">
.small-box{
  opacity: 0.9;
}
</style>
@php
use Carbon\Carbon;
$today_date = Carbon::today();
$today_date45 = Carbon::now()->addDays(45);
@endphp
<img src="{{ asset('img/logo.png') }}" style="width:600px;position: fixed;top:90px;opacity: 0.5;margin-left: auto; margin-right: auto;left:15%;">
<div class="row" style="margin: 20px;">

  @can('view_dollar')
	<div class="col-md-4 col-sm-12 col-xs-12 text-center responsive">
		<div class="small-box text-center bg-black color-palette "     >
            <div class="inner">
              <h3>{{ App\DollarPrice::orderby('id','desc')->first()->price}}</h3>

              <p style="font-weight: bold;color:white;">{{ __('main.dollar price') }}</p>
            </div>
            <div class="icon">
              <i class="fa fa-user-secret"></i>
            </div>
            <a href="{{ route('dollars.index') }}" class="small-box-footer">
              {{ __('main.visit') }} <i class="fa fa-arrow-circle-left"></i>
            </a>
          </div>
    </div>
    @endcan
    {{-- @can('view_driver')
  <div class="col-md-4 col-sm-12 col-xs-12 text-center">
    <div class="small-box text-center " style="    background-color: #f39c12  !important;">
            <div class="inner">
              <h3>{{ App\Driver::where('license_expire','<=',$today_date)->count() }}</h3>

              <p style="font-weight: bold;color:white;">{{ __('main.expired_license') }}</p>
            </div>
            <div class="icon">
              <i class="fa fa-user-secret"></i>
            </div>
            <a href="{{ route('drivers.expired') }}" class="small-box-footer">
              {{ __('main.visit') }} <i class="fa fa-arrow-circle-left"></i>
            </a>
          </div>
    </div>
    @endcan --}}
    @can('view_trip_cost')
    <div class="col-md-4 col-sm-12 col-xs-12 text-center">
		<div class="small-box text-center bg-danger">
            <div class="inner">
              <h3 style="color: transparent">.</h3>

              <p style="font-weight: bold;color:white;">{{ __('main.today transfer price') }}</p>
            </div>
            <div class="icon">
              <i class="fas fa-fw fa-money-bill-alt"></i>
            </div>
            @php
            $CostDate=App\CostDate::orderby('id','desc')->first();
            @endphp
            <a href="{{ route('locationCost.show',$CostDate->id) }}" class="small-box-footer">
              {{ __('main.visit') }} <i class="fa fa-arrow-circle-left"></i>
            </a>

          </div>
	</div>
  @endcan
  @can('view_service')
  <div class="col-md-4 col-sm-12 col-xs-12 text-center">
    <div class="small-box text-center bg-success">
       @php
            $services=App\Services::where('date',$today_date)->count();
            $ss =  App\Services::join('service_items','service_items.service_id','services.id')->where('services.date',$today_date)->select(DB::raw('(service_items.qty*service_items.price) as total'))->first();
            @endphp
            <div class="inner">
              <h4>{{ $services }} {{ __('main.dana') }} {{ __('main.with cost') }} @if($ss){{ $ss->total}}@else 0 @endif</h4>

              <p style="font-weight: bold;color:white;">{{ __('main.today services') }}</p>
            </div>
            <div class="icon">
              <i class="fas fa-fw fa-cog"></i>
            </div>

            <a href="{{ route('services.index') }}" class="small-box-footer">
              {{ __('main.visit') }} <i class="fa fa-arrow-circle-left"></i>
            </a>

          </div>
  </div>
  @endcan
  @can('view_buy_item')
  <div class="col-md-4 col-sm-12 col-xs-12 text-center">
    <div class="small-box text-center bg-info">
       @php
            $services=App\Items::select('alert_qty',DB::raw('(select sum(qty) from purchase_items where item_id=items.id) as purchase'),DB::raw('(select sum(qty) from service_items where item_id=items.id) as service'))->get();
            $items = 0;
            foreach($services as $s) {

              if(($s->purchase-$s->service)<$s->alert_qty) {
                $items+=1;
                // echo $s->purchase;
              }
            }
            @endphp

            <div class="inner">
              <h3 >{{ $items }}</h3>

              <p style="font-weight: bold;color:white;">{{ __('main.item alert qty') }}</p>
            </div>
            <div class="icon">
              <i class="fas fa-fw fa-cog"></i>
            </div>

            <a href="{{ route('items.index') }}" class="small-box-footer">
              {{ __('main.visit') }} <i class="fa fa-arrow-circle-left"></i>
            </a>

          </div>
  </div>
  @endcan
  
  <div class="col-md-4 col-sm-12 col-xs-12 text-center">
    <div class="small-box text-center bg-light" style="">
        <div class="inner">
            <h3 style="color: transparent">.</h3>

          <p style="font-weight: bold;">{{ __('main.monthly report') }}</p>
        </div>
        <div class="icon">
          <i class="fas fa-fw fa-file"></i>
        </div>
        <a href="{{ route('reports.monthly') }}" class="small-box-footer">
          {{ __('main.visit') }} <i class="fa fa-arrow-circle-left"></i>
        </a>
      </div>
</div>

  @can('view_driver')
<div class="col-md-4 col-sm-12 col-xs-12 text-center">
    <div class="small-box text-center " style="background-color: #3b76b3;">
        <div class="inner">
            <h3 >{{ App\Trucks::where('registeration_expire','<=',$today_date45)->count() }}</h3>

          <p style="font-weight: bold;">{{ __('main.expiring registeration') }}</p>
        </div>
        <div class="icon">
          <i class="fas fa-fw fa-calendar"></i>
        </div>
        <a href="{{ route('trucks.expired_registeration',1) }}" class="small-box-footer">
          {{ __('main.visit') }} <i class="fa fa-arrow-circle-left"></i>
        </a>
      </div>
</div>
@endcan
  @can('view_truck')
<div class="col-md-4 col-sm-12 col-xs-12 text-center">
    <div class="small-box text-center " style="background-color: #39CCCC;">
        <div class="inner">
            <h3 >{{ App\Trucks::where('permit_expire','<=',$today_date45)->count() }}</h3>

          <p style="font-weight: bold;">{{ __('main.expiring permit') }}</p>
        </div>
        <div class="icon">
          <i class="fas fa-fw fa-calendar"></i>
        </div>
        <a href="{{ route('trucks.expired_permit',1) }}" class="small-box-footer">
          {{ __('main.visit') }} <i class="fa fa-arrow-circle-left"></i>
        </a>
      </div>
</div>
@endcan
  @can('view_gas_driver')
<div class="col-md-4 col-sm-12 col-xs-12 text-center">
    <div class="small-box text-center bg-warning  " style="">
        @php
    $purchaseAmount=App\gassQtyPurchase::get()->sum("qty");
    $sellAmount=App\gassQtyToTrucks::get()->sum("qty");
    $totalAmount=$purchaseAmount - $sellAmount;
@endphp

        <div class="inner">
            <h3 >{{ $totalAmount }}</h3>

          <p style="font-weight: bold;">{{ __('main.gass available') }}</p>
        </div>
        <div class="icon">
          <i class="fas fa-fw fa-fire"></i>
        </div>
        <a href="{{ route('gass_purchase.index') }}" class="small-box-footer">
          {{ __('main.visit') }} <i class="fa fa-arrow-circle-left"></i>
        </a>
      </div>
</div>
@endcan
@can('view_banzin_driver')
<div class="col-md-4 col-sm-12 col-xs-12 text-center">
    <div class="small-box text-center bg-warning  " style="">
        @php
    $purchaseAmount=App\banzinQtyPurchase::get()->sum("qty");
    $sellAmount=App\banzinQtyToTrucks::get()->sum("qty");
    $totalAmount=$purchaseAmount - $sellAmount;
@endphp

        <div class="inner">
            <h3 >{{ $totalAmount }}</h3>

          <p style="font-weight: bold;">{{ __('main.banzin available') }}</p>
        </div>
        <div class="icon">
          <i class="fas fa-fw fa-fire"></i>
        </div>
        <a href="{{ route('banzin_purchase.index') }}" class="small-box-footer">
          {{ __('main.visit') }} <i class="fa fa-arrow-circle-left"></i>
        </a>
      </div>
</div>
@endcan
  @can('view_check')
<div class="col-md-4 col-sm-12 col-xs-12 text-center">
    <div class="small-box text-center  bg-gray-active color-palette" >
        <div class="inner">
            <h4 >{{ App\Checks::where('date','=',$today_date)->count() }} {{ __('main.truck') }} {{ App\CheckItems::join('checks','checks.id','check_items.check_id')->where('checks.date','=',$today_date)->count() }} {{ __('main.check') }} </h4>

          <p style="font-weight: bold;">{{ __('main.today checks') }}</p>
        </div>
        <div class="icon">
          <i class="fas fa-fw fa-heartbeat"></i>
        </div>
        <a href="{{ route('checks.index') }}" class="small-box-footer">
          {{ __('main.visit') }} <i class="fa fa-arrow-circle-left"></i>
        </a>
      </div>
</div>
@endcan
  @can('view_snduq')
<div class="col-md-4 col-sm-12 col-xs-12 text-center">
    <div class="small-box text-center  bg-purple color-palette" >
        <div class="inner">
            <h4 >{{ number_format( App\Slfa::sum('amount')-(App\DrivePayment::sum('amount')+App\Expenses::sum('cost'))) }}<br>
            ${{ number_format( App\Slfa::sum('dollar')-(App\DrivePayment::sum('dollar')+App\Expenses::sum('dollar'))) }}</h4>

          <p style="font-weight: bold;">{{ __('main.snduqs') }}</p>
        </div>
        <div class="icon">
          <i class="fas fa-university"></i>
        </div>
        <a href="{{ route('snduq.index') }}" class="small-box-footer">
          {{ __('main.visit') }} <i class="fa fa-arrow-circle-left"></i>
        </a>
      </div>
</div>
@endcan

</div>
@can('view_charts')
<div class="row">
  <div class="col col-md-6">{!! $chart2->container() !!}</div>

  <div class="col col-md-6" style="width:100%; height:12cm;"> {!! $chart->container() !!}</div>
</div>
@endcan
{{-- </div> --}}
   {!! $chart->script() !!}
   {!! $chart2->script() !!}
@stop


