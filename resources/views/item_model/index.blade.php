@extends('adminlte::page')
@section('head_scripts')
    <!-- DataTables CSS -->
    {{-- {{ HTML::style('css/plugins/dataTables.bootstrap.css') }} --}}
@stop
@section('content')
<h3>{{ __('main.item model') }}</h3>
<div class="card card-light">
    <div class="card-header bg-light">
        <a  href="{{ route('itemModel.create') }}"><button type="button" class="btn btn-dark"><i class="fa fa-fw fa-plus"></i>{{ __('main.new') }}</button></a>
    </div>
    <div class="card-body">
        <div class="table-responsive">
        <table id="example" class="table table-hover" >
                <thead class="table-light">
                    <tr>
                        <th>#</th>
                        <th>{{ __('main.name') }}</th>

                        <th>{{ __('main.edit') }}</th>
                        <th>{{ __('main.delete') }}</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach(App\ItemModel::all() as $d)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $d->name }}</td>

                        <td>
                           <form method="GET" action="{{ route('itemModel.edit',$d->id) }}">
                                            {{ csrf_field() }}
  <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                            <div class="form-group">
                                                <input type="submit" class="btn btn-info btn-sm " value="{{ __('main.edit') }}" >

                                            </div>
                                        </form></td>
                        <td>
                                        @if($d->id!=2)
                                        <form action="{{ route('itemModel.destroy',$d->id) }}" method="post">
                                            {{ csrf_field() }}
                                              <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                                   <input type="hidden" name="_method" value="DELETE"  >

                                       <a style="padding: 0px;
                                         font-size: 12px;
                                         color: red;" role="menuitem" tabindex="-1"  onclick="return confirm('{{ __('main.are you sure') }}')">
                                         <button type="submit" class="btn btn-sm btn-danger"><i class="fa fa-fw fa-trash"></i></button>
                                       </a>
                                     </form>
                                     @else
                                     <button  class="btn btn-sm btn-danger disabled"><i class="fa fa-fw fa-trash"></i></button>
                                        @endif
                          </td>
                    </tr>
                     @endforeach
                </tbody>
            </table>
        </div>
        </div>
</div>
@stop
