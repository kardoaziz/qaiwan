@extends('adminlte::page')
@section('head_scripts')
    <!-- DataTables CSS -->
    {{-- {{ HTML::style('css/plugins/dataTables.bootstrap.css') }} --}}
@stop
@section('content')
<h3>{{ __('main.dollar price') }}</h3>
<div class="card card-light">
	<div class="card-header">
		 <a class="btn btn-light noprint" data-toggle="modal" data-target="#new-price-modal" style="font-size: 18px;float:right;color:grey !important;">
                <i class="fa fa-plus"></i> {{ __("main.new") }}
              </a>
	</div>
	<div class="card-body">
		<table id="example" class="table table-bordered table-hover">
			<thead>
				<th>#</th>
				<th>{{ __('main.date') }}</th>
				<th>{{ __('main.price') }}</th>
				<th>{{ __('main.note') }}</th>
			</thead>
			<tbody>
				@foreach(App\DollarPrice::orderby('id','desc')->get() as $d)
				<tr>
					<td>{{ $loop->iteration }}</td>
					<td>{{ $d->date }}</td>
					<td>{{ number_format( $d->price,2) }}</td>
					<td>{{ $d->note }}</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
</div>
<div class="modal fade" id="new-price-modal" >
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" align="center"><b> {{ __("main.new") }}  {{ __("main.dollar price") }}</b></h4>
      </div>
      <div class="modal-body" style="overflow-y: scroll;">
        <form action="{{ route('dollars.store') }}" method="POST" >
                {{csrf_field()}}
          <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
   
             

          <div class="form-group">
            <label for="exampleInputEmail1"> {{ __("main.date") }}</label>
              <input type="date" id="date" name="date" class="form-control" placeholder="Date" required value="{{date('Y-m-d', strtotime('+0 day'))}}">
          </div>
           <div class="form-group">
            <label for="exampleInputEmail1"> {{ __("main.price") }}</label>
              <input type="number" step="0.01"  name="price" class="form-control" placeholder="price" required >
          </div>
          
          <div class="form-group">
            <label for="exampleInputEmail1"> {{ __("main.note") }}</label>
              <input type="text" id="note" name="note" class="form-control" placeholder="{{ __('main.note') }}"  >
          </div>


          <button type="submit" class="btn btn-primary"> {{ __("main.add") }}</button>
        </form>
      </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default pull-left" data-dismiss="modal"> {{ __("main.cancel") }}</button>

          </div>
    </div>
  </div>
</div>
        </div>
    </div>
@stop