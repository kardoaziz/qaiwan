@extends('adminlte::page')
@section('head_scripts')
    <!-- DataTables CSS -->
    {{-- {{ HTML::style('css/plugins/dataTables.bootstrap.css') }} --}}
@stop
@section('content')
<div class="modal-content">
      <div class="modal-header">
       
        <h4 class="modal-title" align="center"><b> {{ __("main.update") }}  {{ __("main.slfa") }}</b></h4>
      </div>
      <div class="modal-body" style="overflow-y: scroll;">
        <form action="{{ route('slfa.update',$slfa->id) }}" method="POST" >
                {{csrf_field()}}
                  <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
<input type="hidden" name="_method" value="PUT">          

          <div class="form-group">
            <label for="exampleInputEmail1"> {{ __("main.date") }}</label>
              <input type="date" id="date" name="date" class="form-control" placeholder="date" required value="{{ $slfa->date }}">
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1"> {{ __("main.snduq") }}</label>
              <select class="form-control" name="snduq_id" value="{{ $slfa->snduq_id }}">
                  @foreach(App\Snduq::all() as $s)
                  <option value="{{ $s->id }}">{{ $s->name }}</option>
                  @endforeach
              </select>
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1"> {{ __("main.amount") }}</label>
              <input type="number" step="any" id="amount" name="amount" class="form-control" placeholder="{{ __('main.amount') }}" value="{{ $slfa->amount }}" onblur="update('amount',this.value,{{ App\DollarPrice::orderby('created_at','desc')->first()->price }})">
          </div>
           <div class="form-group">
            <label for="exampleInputEmail1"> {{ __("main.dollar") }}</label>
              <input type="number" step="any" id="dollar" name="dollar" class="form-control" placeholder="{{ __('main.dollar') }}" value="{{ $slfa->dollar }}" onblur="update('dollar',this.value,{{ App\DollarPrice::orderby('created_at','desc')->first()->price }})">
          </div>
          
          <div class="form-group">
            <label for="exampleInputEmail1"> {{ __("main.note") }}</label>
              <input type="text" id="note" name="note" class="form-control" placeholder="{{ __('main.note') }}"  value="{{ $slfa->note }}">
          </div>


          <button type="submit" class="btn btn-primary"> {{ __("main.add") }}</button>
        </form>
      </div>
        
    </div>
    <script type="text/javascript">
        function update(field,val,dollar) {
        // body...
        if(field=='amount'){
            document.getElementById('dollar').value = (val/dollar);
        }
        else if(field=='dollar')
        {
           document.getElementById('amount').value =(val*dollar);
        }
      }
    </script>
    @stop