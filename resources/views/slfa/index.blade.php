@extends('adminlte::page')
@section('head_scripts')
    <!-- DataTables CSS -->
    {{-- {{ HTML::style('css/plugins/dataTables.bootstrap.css') }} --}}
@stop
@section('content')
<h3>{{ __('main.slfa') }}</h3>
<div class="card card-light">
    <div class="card-header bg-info">
         @can('add_slfa')<a class="btn btn-light noprint" data-toggle="modal" data-target="#new-slfa-modal" style="font-size: 18px;float:right;color:grey !important;">
                <i class="fa fa-plus"></i> {{ __("main.new") }}
              </a>@endcan
    </div>
    <div class="card-body">
        <div class="table-responsive">
        <table id="example" class="table table-hover" >
                <thead class="table-primary">
                    <tr>
                        <th>#</th>
                        <th>{{ __('main.date') }}</th>
                        <th>{{ __('main.amount') }}</th>
                        <th>{{ __('main.dollar') }}</th>
                        <th>{{ __('main.note') }}</th>
                        
                         @can('edit_slfa')<th>{{ __('main.edit') }}</th>@endcan
                        @can('delete_slfa') <th>{{ __('main.delete') }}</th>@endcan
                    </tr>
                </thead>
                <tbody>
                    @php
                    $items = App\Slfa::all();
                    @endphp
                    @foreach($items as $d)
                    <tr>
                       
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $d->date }}</td>
                        <td>{{ number_format($d->amount,2) }}</td>
                        <td>{{ number_format($d->dollar,2) }}</td>
                        <td>{{ $d->note }}</td>
                        
                         @can('edit_slfa')<td>
                           <form method="GET" action="{{ route('slfa.edit',$d->id) }}">
                                            {{ csrf_field() }}
  <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                            <div class="form-group">
                                                <input type="submit" class="btn btn-info btn-sm " value="{{ __('main.edit') }}" >

                                            </div>
                                        </form></td>@endcan
                       @can('delete_slfa')  <td>
                                        <form action="{{ route('slfa.destroy',$d->id) }}" method="post">
                                 {{ csrf_field() }}
                                   <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                        <input type="hidden" name="_method" value="DELETE"  >

                            <a style="padding: 0px;
                              font-size: 12px;
                              color: red;" role="menuitem" tabindex="-1"  onclick="return confirm('{{ __('main.are you sure') }}')">
                              <button type="submit" class="btn btn-sm btn-danger"><i class="fa fa-fw fa-trash"></i></button>
                            </a>
                          </form>
                          </td>@endcan
                    </tr>
                     @endforeach
                     
                </tbody>
                {{-- <tfoot>
                    <tr>
                         <td colspan="7">
                             {{ __('main.total') }}
                         </td>
                         <td  class="text-center">{{ $items->sum('stock') }}</td>
                         <td  class="text-center"></td>
                         <td  class="text-center"></td>
                         <td  class="text-center">{{ number_format($items->sum('total_cost'),2) }}</td>
                         <td  class="text-center">{{ number_format($items->sum('total_costd'),2) }}</td>
                         <td  class="text-center"></td>
                         <td colspan="2"></td>
                     </tr>
                </tfoot> --}}
            </table>
        </div>
        </div>
</div>
<div class="modal fade" id="new-slfa-modal" >
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" align="center"><b> {{ __("main.new") }}  {{ __("main.slfa") }}</b></h4>
      </div>
      <div class="modal-body" style="overflow-y: scroll;">
        <form action="{{ route('slfa.store') }}" method="POST" enctype="multipart/form-data">
                {{csrf_field()}}
          <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
          

          <div class="form-group">
            <label for="exampleInputEmail1"> {{ __("main.date") }}</label>
              <input type="date" id="date" name="date" class="form-control" placeholder="date" required >
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1"> {{ __("main.snduq") }}</label>
              <select class="form-control" name="snduq_id">
                  @foreach(App\Snduq::all() as $s)
                  <option value="{{ $s->id }}">{{ $s->name }}</option>
                  @endforeach
              </select>
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1"> {{ __("main.amount") }}</label>
              <input type="number" step="any" id="amount" name="amount" class="form-control" placeholder="{{ __('main.amount') }}"  onblur="update('amount',this.value,{{ App\DollarPrice::orderby('created_at','desc')->first()->price }})">
          </div>
           <div class="form-group">
            <label for="exampleInputEmail1"> {{ __("main.dollar") }}</label>
              <input type="number" step="any" id="dollar" name="dollar" class="form-control" placeholder="{{ __('main.dollar') }}"  onblur="update('dollar',this.value,{{ App\DollarPrice::orderby('created_at','desc')->first()->price }})">
          </div>
          
          <div class="form-group">
            <label for="exampleInputEmail1"> {{ __("main.note") }}</label>
              <input type="text" id="note" name="note" class="form-control" placeholder="{{ __('main.note') }}"  >
          </div>


          <button type="submit" class="btn btn-primary"> {{ __("main.add") }}</button>
        </form>
      </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default pull-left" data-dismiss="modal"> {{ __("main.cancel") }}</button>

          </div>
    </div>
  </div>
</div>
        </div>
    </div>
    <script type="text/javascript">
        function update(field,val,dollar) {
        // body...
        if(field=='amount'){
            document.getElementById('dollar').value = (val/dollar);
        }
        else if(field=='dollar')
        {
           document.getElementById('amount').value =(val*dollar);
        }
      }
    </script>
@stop
