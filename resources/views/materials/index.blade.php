@extends('adminlte::page')
@section('head_scripts')
    <!-- DataTables CSS -->
    {{-- {{ HTML::style('css/plugins/dataTables.bootstrap.css') }} --}}
@stop
@section('content')
<h3>{{ __('main.materials') }}</h3>
<div class="card card-light">
    <div class="card-header">
        @can('add_material')<a  href="{{ route('materials.create') }}"><button type="button" class="btn btn-success"><i class="fa fa-fw fa-plus"></i>{{ __('main.new') }}</button></a>@endcan
    </div>
    <div class="card-body">
        <div class="table-responsive">
        <table id="example" class="table table-hover" >
                <thead class="thead-dark">
                    <tr>
                        <th>#</th>
                        <th>{{ __('main.name') }}</th>

                        <th>{{ __('main.edit') }}</th>
                        <th>{{ __('main.delete') }}</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach(App\Material::all() as $d)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $d->name }}</td>

                        @can('edit_material')<td>
                           <form method="GET" action="{{ route('materials.edit',$d->id) }}">
                                            {{ csrf_field() }}
  <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                            <div class="form-group">
                                                <input type="submit" class="btn btn-info btn-sm " value="{{ __('main.edit') }}" >

                                            </div>
                                        </form></td>@endcan
                        @can('delete_material')<td>
                                        <form action="{{ route('materials.destroy',$d->id) }}" method="post">
                                 {{ csrf_field() }}
                                   <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                        <input type="hidden" name="_method" value="DELETE"  >

                            <a style="padding: 0px;
                              font-size: 12px;
                              color: red;" role="menuitem" tabindex="-1"  onclick="return confirm('{{ __('main.are you sure') }}')">
                              <button type="submit" class="btn btn-sm btn-danger"><i class="fa fa-fw fa-trash"></i></button>
                            </a>
                          </form>
                          </td>@endcan
                    </tr>
                     @endforeach
                </tbody>
            </table>
        </div>
        </div>
</div>
@stop
