@extends('adminlte::page')
@section('head_scripts')
    <!-- DataTables CSS -->
    {{-- {{ HTML::style('css/plugins/dataTables.bootstrap.css') }} --}}
@stop
@section('content')
<h3>{{ __('main.projects') }}</h3>
    <div class="card card-light">
        <div class="card-header bg-info">
          @can('add_project')  <a class="btn btn-light noprint" data-toggle="modal" data-target="#new-service-modal" style="font-size: 18px;float:right; color:black !important;">
                <i class="fa fa-plus"></i> {{ __("main.new") }}
              </a>@endcan
        </div>
        <div class="card-body">
            <div class="table-responsive">
    <table id="example" class="table table-hover ">
        <thead class="table-danger">
            <tr>
                <th>#</th>
                <th>{{ __('main.project') }}</th>
                {{-- <th>{{ __('main.manager') }}</th> --}}
                <th>{{ __('main.phone') }}</th>
                <th>{{ __('main.address') }}</th>
                <th>{{ __('main.total cost') }}</th>
                {{-- <th>{{ __asdgkll('main.total cost $') }}</th> --}}
                <th>{{ __('main.total payment') }}</th>
                <th>{{ __('main.due') }}</th>
                {{-- <th>{{ __('main.total payment $') }}</th> --}}
                {{-- <th>{{ __('main.invoice_no') }}</th> --}}
                <th>{{ __('main.note') }}</th>
                <th>{{ __('main.service') }}</th>
                <th>{{ __('main.payments') }}</th>
                @can('edit_project_service') <th>{{ __('main.edit') }}</th>@endcan
                 @can('delete_project_service')<th>{{ __('main.delete') }}</th>@endcan
            </tr>
        </thead>
        @php 
        $totalt = 0;
        $totalp = 0;
        @endphp
        <tbody>
        	@foreach(App\Projects::all() as $d)
          @php
          $t = App\ServiceOut::where('id_project',$d->id)->sum('fee')+  App\ServiceOut::join('service_out_items','service_out_items.id_service','service_outs.id')->where('service_outs.id_project',$d->id)->select(DB::raw('sum(qty*price) as total'))->first()->total;
          $p = App\projectPayment::where('id_project',$d->id)->select(DB::raw('sum(amount) as total'))->first()->total;

          $totalt+=$t;
          $totalp+=$p;
          @endphp
            <tr>
                <td>{{ $loop->iteration }}</td>
                <td>{{ $d->name }}</td>
                {{-- <td>{{ $d->manager }}</td> --}}
                <td>{{ $d->phone }}</td>
                <td>{{ $d->location }}</td>
                <td>{{ number_format($t,2) }} </td>
                {{-- <td>{{ number_format( App\ServiceOut::where('id_project',$d->id)->select(DB::raw('sum(dolar) as total'))->first()->total,2) }} </td> --}}
                <td>{{ number_format( $p,2) }} </td>
                <td>{{ number_format( $t-$p,2) }} </td>
                {{-- <td>{{ number_format( App\projectPayment::where('id_project',$d->id)->select(DB::raw('sum(dolar) as total'))->first()->total,2) }} </td> --}}
                <td>{{ $d->note }}</td>
                @can('edit_project_service')<td>
                    <form method="GET" action="{{ route('serviceOut.services',$d->id) }}">
                                     {{ csrf_field() }}
  <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                     <div class="form-group">
                                         <input type="submit" class="btn btn-warning btn-sm " value="{{ __('main.service') }}" >

                                     </div>
                                 </form></td>@endcan
                                 @can('edit_service')<td>
                                    <form method="GET" action="{{ route('projectpayments.payments',$d->id) }}">
                                                     {{ csrf_field() }}
  <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                                     <div class="form-group">
                                                         <input type="submit" class="btn btn-warning btn-sm " value="{{ __('main.payments') }}" >

                                                     </div>
                                                 </form></td>@endcan
                 @can('add_project_payment')<td>
                   <form method="GET" action="{{ route('projects.edit',$d->id) }}">
                                    {{ csrf_field() }}
  <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                    <div class="form-group">
                                        <input type="submit" class="btn btn-info btn-sm " value="{{ __('main.edit') }}" >

                                    </div>
                                </form></td>@endcan
                 @can('delete_project_service')<td>
                                <form action="{{ route('projects.destroy',$d->id) }}" method="post">
                         {{ csrf_field() }}
                           <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                <input type="hidden" name="_method" value="DELETE"  >

                    <a style="padding: 0px;
                      font-size: 12px;
                      color: red;" role="menuitem" tabindex="-1"  onclick="return confirm('{{ __('main.are you sure') }}')">
                      <button type="submit" class="btn btn-sm btn-danger"><i class="fa fa-fw fa-trash"></i></button>
                    </a>
                  </form>
                  </td>@endcan
            </tr>
             @endforeach
        </tbody>
        <tfoot>
          <tr>
            <td colspan="4">{{ __('main.total') }}</td>
            <td >{{ number_format($totalt) }}</td>
            <td >{{ number_format($totalp) }}</td>
            <td >{{ number_format($totalt-$totalp) }}</td>
            <td colspan="5"></td>
          </tr>
        </tfoot>
    </table>
</div>
<div class="modal fade" id="new-service-modal" >
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" align="center"><b> {{ __("main.new") }}  {{ __("main.service") }}</b></h4>
      </div>
      <div class="modal-body" style="overflow-y: scroll;">
        <form action="{{ route('projects.store') }}" method="POST" enctype="multipart/form-data">
                {{csrf_field()}}
          <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
          <div class="form-group">
                <label>{{ __("main.name") }}</label>
                <input class="form-control" type="text" name="name">

              </div>

          <div class="form-group">
            <label for="exampleInputEmail1"> {{ __("main.manager") }}</label>
              <input type="text" name="manager" class="form-control" >
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1"> {{ __("main.phone") }}</label>
              <input type="text" name="phone" class="form-control" >
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1"> {{ __("main.address") }}</label>
              <input type="text" name="location" class="form-control" >
          </div>

          <div class="form-group">
            <label for="exampleInputEmail1"> {{ __("main.note") }}</label>
              <input type="text" id="note" name="note" class="form-control" placeholder="{{ __('main.note') }}"  >
          </div>


          <button type="submit" class="btn btn-primary"> {{ __("main.add") }}</button>
        </form>
      </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default pull-left" data-dismiss="modal"> {{ __("main.cancel") }}</button>

          </div>
    </div>
  </div>
</div>
        </div>
    </div>
@stop
