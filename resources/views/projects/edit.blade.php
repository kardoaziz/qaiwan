@extends('adminlte::page')
@section('head_scripts')
    <!-- DataTables CSS -->
    {{-- {{ HTML::style('css/plugins/dataTables.bootstrap.css') }} --}}
@stop
@section('content')
<div class="modal-content">
      <div class=" text-center" style="margin:5px;">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          {{-- <span aria-hidden="true">&times;</span> --}}
        </button>
        <span style="margin:5px;" class=" text-center" align="center"><b> {{ __("main.update") }}  {{ __("main.projects") }}</b></span>
      </div>
      <div class="modal-body" style="overflow-y: scroll;">
        <form action="{{ route('projects.update',$projects->id) }}" method="POST" enctype="multipart/form-data">
                {{csrf_field()}}
                  <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
<input type="hidden" name="_method" value="PUT">

              <div class="form-group">
            <label for="exampleInputEmail1"> {{ __("main.name") }}</label>
              <input type="text"  name="name" class="form-control"  value="{{ $projects->name }}">
          </div>

          <div class="form-group">
            <label for="exampleInputEmail1"> {{ __("main.manager") }}</label>
              <input type="text" name="text" class="form-control" value="{{ $projects->manager }}">
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1"> {{ __("main.phone") }}</label>
              <input type="text" name="phone" class="form-control" value="{{ $projects->phone }}" >
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1"> {{ __("main.address") }}</label>
              <input type="text" name="location" class="form-control" value="{{ $projects->location }}" >
          </div>

          <div class="form-group">
            <label for="exampleInputEmail1"> {{ __("main.note") }}</label>
              <input type="text" id="note" name="note" class="form-control"  value="{{ $projects->note }}">
          </div>


          <button type="submit" class="btn btn-primary"> {{ __("main.add") }}</button>
        </form>
      </div>

    </div>
    @stop
