@extends('adminlte::page')
@section('head_scripts')
    <!-- DataTables CSS -->
    {{-- {{ HTML::style('css/plugins/dataTables.bootstrap.css') }} --}}
@stop
@section('content')
<style type="text/css">
	td{
		text-align: center;
	}
	th {
		text-align: center !important;
	}
</style>

<div class="card card-primary">
            <div class="card-header with-border text-center">
              {{ __('main.new price') }}
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form role="form" action="{{ route('locationCost.store') }}" method="POST">
            	    {{csrf_field()}}
          <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
              <div class="card-body">
                @php
                $lastDate=App\CostDate::orderBy('id', 'DESC')->first();
                @endphp
                <div class="table-responsive">
                <table class="table table-hover">
                    <thead class="thead-dark">
                        <tr>
                            <th>{{ __('main.from') }}</th>
                            <th>{{ __('main.to') }}</th>
                            <th>{{ __('main.price') }}</th>
                            <th><a href="#" class="addtrip"><i class="fa fa-fw fa-plus"></i></a></th>


                        </tr>
                    </thead>
                    @if ( $lastDate!=null)
                    @php
                    $lastPrice=App\LocationCost::where('id_date',$lastDate->id)->get();
                    @endphp
                   <tbody class="bMaterial">
                     @foreach($lastPrice as $lp)
                     <tr>
                       <td><select name="from[]" class="form-control">
                         @foreach(App\Locations::all() as $f)
                         <option @if($lp->from_id==$f->id) selected @endif value="{{ $f->id }}">{{ $f->name }}</option>
                         @endforeach
                       </select></td>
                       <td><select name="to[]" class="form-control">
                         @foreach(App\Locations::all() as $f)
                         <option @if($lp->to_id==$f->id) selected @endif value="{{ $f->id }}">{{ $f->name }}</option>
                         @endforeach
                       </select></td>
                       <td><input class="form-control" type="number"step="0.01" name="price[]" value="{{ $lp->price }}"></td>
                       <td><a href="#" class="btn btn-danger removeTrip">X</td>
                     </tr>
                     @endforeach
                     <tr>
                       <td><select name="from[]" class="form-control">
                         @foreach(App\Locations::all() as $f)
                         <option value="{{ $f->id }}">{{ $f->name }}</option>
                         @endforeach
                       </select></td>
                       <td><select name="to[]" class="form-control">
                         @foreach(App\Locations::all() as $f)
                         <option value="{{ $f->id }}">{{ $f->name }}</option>
                         @endforeach
                       </select></td>
                       <td><input class="form-control" type="number"step="0.01" name="price[]"></td>
                       <td><a href="#" class="btn btn-danger removeTrip">X</td>
                     </tr>
                   </tbody>

                  
                    @endif
                  

              

                  </table>
                </div>
              </div>
              <!-- /.box-body -->

              <div class="card-footer">
                <button type="submit" class="btn btn-success"><i class="fa fa-fw fa-save"></i>{{ __('main.save') }}</button>
              </div>
            </form>
          </div>
          <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js"></script>
<script type="text/javascript">

    $('.addtrip').on('click',function(){
       addTrip();
    });

    function addTrip(){
        var tr='<tr>\
                       <td><select name="from[]" class="form-control">'+
                         @foreach(App\Locations::all() as $f)
                         '<option value="{{ $f->id }}">{{ $f->name }}</option>'+
                         @endforeach
                       '</select></td>\
                       <td><select name="to[]" class="form-control">'+
                         @foreach(App\Locations::all() as $f)
                         '<option value="{{ $f->id }}">{{ $f->name }}</option>'+
                         @endforeach
                       '</select></td>\
                       <td><input class="form-control" type="number"step="0.01" name="price[]"></td>\
                       <td><a href="#" class="btn btn-danger removeTrip">X</td>\
                     </tr>';
        $('.bMaterial').append(tr);
    };
    $('.removeTrip').live('click',function(){
        var last=$('.bMaterial tr').length;
        if(last==1){
            alert('at lease one column required');
        }else{
            $(this).parent().parent().remove();
        }
    });
    
  </script>
@stop
