
@extends('adminlte::page')
@section('head_scripts')
    <!-- DataTables CSS -->
    {{-- {{ HTML::style('css/plugins/dataTables.bootstrap.css') }} --}}
@stop
@section('content')

<div class="card card-light">
    <div class="card-header text-center">
        <h3>{{ __('main.prices') }}{{$data->created_at->format('بەرواری : Y/m/d | کات : h:i A')}}</h3>
    </div>
    <div class="card-body">
        <div class="table-responsive">
        <table id="example" class="table table-hover" >
                <thead class="thead-dark">
                    <tr>
                        <th>#</th>
                        <th>{{ __('main.from') }}</th>
                        <th>{{ __('main.to') }}</th>
                        <th>{{ __('main.price') }}</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($prices as $d)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{$d->from_location->name}}</td>
                        <td>{{$d->to_location->name}}</td>
                        <td>{{$d->price}}</td>
                    </tr>
                     @endforeach
                </tbody>
            </table>
        </div>
        </div>
</div>
@stop
