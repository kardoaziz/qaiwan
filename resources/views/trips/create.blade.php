@extends('adminlte::page')
@section('head_scripts')
    <!-- DataTables CSS -->
    {{-- {{ HTML::style('css/plugins/dataTables.bootstrap.css') }} --}}
@stop
@section('content')
<h3>{{ __('main.trips create') }}</h3>
<form role="form" action="{{ route('trips.store') }}" method="POST">
    {{csrf_field()}}
      <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
<div class="card card-info">
    <div class="card-header">
        <div class="row">
        <div class="col-md-4">
            <table class="table table-borderless">
                <tr>
                    <td><label >{{ __('main.invoice_no') }}</label></td>
                    <td><input type="text" class="form-control"  id="invoice_no" name="invoice_no" placeholder="{{ __('main.invoice no') }}" required=""></td>
                </tr>
                <tr>
                    <td> <label >{{ __('main.date') }}</label></td>
                    <td><input type="date" class="form-control"  id="date" name="t_date" placeholder="{{ __('main.date') }}" required=""></td>
                </tr>

            </table>

        </div>
        <div class="col-md-8">

            <table class="table table-borderless">
                <tr>
                    <input type="hidden" name="driver_id" id="driver_id" value="">
                    <td> <label >{{ __('main.driver') }}</label></td>
                    <td colspan="2">
                        <input list="driverslist" name="drivers" id="drivers" class="form-control" onchange="selectdriver(this.value)" placeholder="{{ __('main.driver') }}" required="">
                        <datalist id="driverslist">
                          @foreach (App\Drive::where('status','active')->get() as $d)
                        <option value="{{$d->id}}-{{$d->driver->name}}">{{$d->driver->name}} - {{$d->truck->code}}</option>
                                    @endforeach
                        </datalist>

                    </td>
                </tr>
                <tr>
                    <td><label>{{ __('main.locations') }}</label></td>
                    <td>
                        <input type="hidden" name="from_to" id="from_to" value="">
                        {{ __('main.from') }}
                        <select id="from_location" name="from" class="form-control" onchange="selecttrip()">
                            <option value=-1>{{ __('main.select source') }}</option>
                            @foreach(App\locations::all() as $f)
                            <option value="{{ $f->id }}">{{ $f->name }}</option>
                            @endforeach
                        </select>
                    </td>
                    <td>
                        {{ __('main.to') }}
                        {{-- <input type="hidden" name="from_to" id="from_to" value=""> --}}
                        <select id="to_location" name="to" class="form-control" onchange="selecttrip()">
                            <option value=-1>{{ __('main.select destination') }}</option>
                            @foreach(App\locations::all() as $f)
                            <option value="{{ $f->id }}">{{ $f->name }}</option>
                            @endforeach
                        </select>
                    </td>
                       {{--  <input list="tripslist" name="trips" id="trips" class="form-control" onchange="selecttrip(this.value)" placeholder="{{ __('main.trip') }}" required="">
                    <datalist id="tripslist">
                        @if ($cd= App\CostDate::orderBy('id', 'DESC')->first() )
                                @foreach (App\LocationCost::where('id_date',$cd->id)->get() as $lc)
                        <option value="{{$lc->id}}-{{ $lc->from_location->name }}-{{ $lc->to_location->name }}-{{ $lc->price }}">{{$lc->from_location->name}} بۆ {{$lc->to_location->name}} IQD {{ $lc->price }}</option>
                                @endforeach
                            @endif
                    </datalist> --}}
                    </td>
                </tr>

            </table>

        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-md-1">{{  __('main.price') }}</div>
        <div class="col-md-2"><input class="form-control" type="text" name="price" id="price" disabled placeholder="{{ __('main.price') }}"></div>
        <div class="col-md-1">{{ __('main.driver') }}</div>
        <div class="col-md-2"><input class="form-control" type="text" name="driver" id="driver" disabled placeholder="{{ __('main.driver') }}"></div>
        {{-- <div class="col-md-1">{{ __('main.from') }}</div>
        <div class="col-md-2"><input class="form-control" type="text" name="from" id="from" disabled placeholder="{{ __('main.from') }}"></div>
        <div class="col-md-1">{{ __('main.to') }}</div>
        <div class="col-md-2"><input class="form-control" type="text" name="to" id="to" disabled placeholder="{{ __('main.to') }}"></div> --}}
    </div>
    <br>
    <div class="container">
        <label for="">{{ __('main.note') }}</label>
        <textarea class="form-control" name="note" id="" cols="30" rows="3" placeholder="{{ __('main.note') }}"></textarea>
    </div>
    </div>
    <div class="card-body">
        <div class="card-header bg-success text-white"><h3>{{ __('main.materials') }}</h3></div>
        <table class="table table-bordered table-responsive">
            <thead>
                <th>{{ __('main.material') }}</th>
                <th>{{ __('main.unit') }}</th>
                <th>{{ __('main.invoice_no_from') }}</th>
                <th>{{ __('main.qty barkrdn') }}</th>
                <th>{{ __('main.date barkrdn') }}</th>
                <th>{{ __('main.invoice_no_to') }}</th>
                <th>{{ __('main.qty dagrtn') }}</th>
                <th>{{ __('main.date dagrtn') }}</th>
                <th>{{ __('main.note') }}</th>
                <th><a href="#" class="addMaterial"><i class="fa fa-fw fa-plus"></i></a></th>
            </thead>
            <tbody class="bMaterial">
                <tr>
                    <td><select class="form-control" name="material_id[]" style="width: 150px;" required="">
                            @foreach (App\Material::all() as $m)
                                <option value="{{$m->id}}">{{$m->name}}</option>
                            @endforeach
                        </select>
                    </td>
                    <td><select class="form-control" name="unit[]" style="width: 80px;">
                                <option value="ton">{{__('main.ton')}}</option>
                                <option value="kilo">{{__('main.kilo')}}</option>
                                <option value="litr">{{__('main.litr')}}</option>
                        </select>
                    </td>
                    <td><input class="form-control" type="text" name="m_invoice_no[]" required=""></td>
                    <td><input class="form-control" type="number" name="m_qty_barkrdn[]" style="width: 150px;" required=""></td>
                    <td><input class="form-control" type="date" name="m_date_barkrdn[]" style="width: 150px;" required=""></td>
                    <td><input class="form-control" type="number" name="m_invoice_no_to[]" style="width: 150px;" required=""></td>
                    <td><input class="form-control" type="number" name="m_qty_dagrtn[]" style="width: 150px;"  required=""></td>
                    <td><input class="form-control" type="date" name="m_date_dagrtn[]" style="width: 150px;" onblur="getPrice(this.value)" required=""></td>
                    <td><textarea class="form-control" name="m_note[]" style="width: 150px;"></textarea></td>
                    <td><a href="#" class="btn btn-danger removeMaterial">X</td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="card-body">
       <div class="card-header bg-danger text-white"> <h3>{{ __('main.expenses') }}</h3></div>
        <table class="table table-bordered">
            <thead>
                <th>{{ __('main.expense types') }}</th>
                <th>{{ __('main.invoice_no') }}</th>
                <th>{{ __('main.cost') }}</th>
                <th>{{ __('main.dollar') }}</th>
                <th>{{ __('main.note') }}</th>
                <th><a href="#" class="addExpense"><i class="fa fa-fw fa-plus"></i></a></th>
            </thead>
            <tbody class="bExpense">
                <!-- <tr>
                    <td><select class="form-control" name="expense_type_id[]" style="width: 150px;">
                        @foreach (App\ExpenseType::all() as $et)
                            <option value="{{$et->id}}">{{$et->name}}</option>
                        @endforeach
                    </select></td>
                    <td><input class="form-control" type="text" name="ex_invoice_no[]"></td>
                    <td><input class="form-control" type="number" name="ex_cost[]"></td>
                    <td><textarea class="form-control" name="ex_note[]" style="width: 150px;"></textarea></td>
                    <td><a href="#" class="btn btn-danger removeExpense">X</td>
                </tr> -->
            </tbody>
        </table>
    </div>
    <div class="card-body">
        <div class="card-header bg-info text-white"><h3>{{ __('main.gass') }}</h3></div>
        <table class="table table-bordered">
            <thead>
                <th>{{ __('main.invoice_no') }}</th>
                <th>{{ __('main.qty') }}</th>
                <th>{{ __('main.price') }}</th>
                <th>{{ __('main.date') }}</th>
                <th>{{ __('main.gass location') }}</th>
                <th>{{ __('main.note') }}</th>
                <th><a href="#" class="addGass"><i class="fa fa-fw fa-plus"></i></a></th>
            </thead>
            <tbody class="bGass">
                <!-- <tr>
                    <td><input class="form-control" type="text" name="g_invoice_no[]"></td>
                    <td><input class="form-control" type="number" name="g_qty[]"></td>
                    <td><input class="form-control" type="number" name="g_price[]"></td>
                    <td><input class="form-control" type="date" name="g_date[]"></td>
                    <td><input class="form-control" type="text" name="g_location[]"></td>
                    <td><textarea class="form-control" name="g_note[]" style="width: 150px;"></textarea></td>
                    <td><a href="#" class="btn btn-danger removeGass">X</td>
                </tr> -->
            </tbody>
        </table>

    </div>
    <div class="card-body">
        <div class="card-header bg-warning text-white"><h3>{{ __('main.fines') }}</h3></div>
        <table class="table table-bordered">
            <thead>
                <th>{{ __('main.invoice_no') }}</th>
                <th>{{ __('main.amount') }}</th>
                <th>{{ __('main.dollar') }}</th>
                <th>{{ __('main.note') }}</th>
                <th><a href="#" class="addFine"><i class="fa fa-fw fa-plus"></i></a></th>
            </thead>
            <tbody class="bFine">
               <!--  <tr>
                    <td><input class="form-control" type="text" name="f_invoice_no[]"></td>
                    <td><input class="form-control" type="number" name="f_amount[]"></td>
                    <td><textarea class="form-control" name="f_note[]" style="width: 150px;"></textarea></td>
                    <td><a href="#" class="btn btn-danger removeGass">X</td>
                </tr> -->
            </tbody>
        </table>

        <div class="card-footer">
                <button type="submit" class="btn btn-success"><i class="fa fa-fw fa-save"></i>{{ __('main.save') }}</button>
              </div>
    </div>
</div>

</form>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js"></script>
<script>
function selectdriver(id) {

    $('#driver_id').val(id.split("-")[0]);
    $('#driver').val(id.split("-")[1]);
}
function selecttrip() {

    var from_id = $('#from_location').val();
    var to_id = $('#to_location').val();
    console.log(from_id);
    console.log(to_id);
    // $('#from').val($('#from_location').val());
    // $('#to').val($('#to_location').val());
    if(from_id>0 && to_id>0)
    {
        console.log("get Price");
        $.ajax({
          type:'POST',
          headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
          url:'{{ route("trip.getprice") }}',
          data:{ id:'-1',date:'',from_id:from_id ,to_id:to_id},
           success:function(data){
            // console.log(data);
            if(data=="error")
            {
                alert('هیچ نرخێک دیاری ناکراوا بۆ ئەو شوێنە');
            }
            else{
                console.log(data);
               $('#price').val(data['price']);
               $('#from_to').val(data['id']);
           }
           }
        });
    }else{
        console.log("select the other");
    }
    // $('#price').val(id.split("-")[3]);

}
function getPrice(date) {
    console.log(date);
    id = $('#from_to').val();
    console.log($('#from_to').val());
    $.ajax({
          type:'POST',
          headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
          url:'{{ route("trip.getprice") }}',
          data:{ id:id,date:date},
           success:function(data){
            console.log(data);
           $('#price').val(data['price']);
           $('#from_to').val(data['id']);
           }
        });
    // body...
}
        </script>
<script type="text/javascript">

    $('.addMaterial').on('click',function(){
       addMaterial();
    });
    function addMaterial(){
        var tr='<tr>'+
        '<td><select class="form-control" name="material_id[]" style="width: 150px;" required>'+
            @foreach (App\Material::all() as $m)
            '<option value="{{$m->id}}">{{$m->name}}</option>'+
            @endforeach
            '</select></td>'+
            '<td><select class="form-control" name="unit[]" style="width: 80px;" required>\
                                <option value="ton">{{__("main.ton")}}</option>\
                                <option value="kilo">{{__("main.kilo")}}</option>\
                                <option value="litr">{{__("main.litr")}}</option>\
                        </select></td>'+
        '<td><input class="form-control" type="text" name="m_invoice_no[]" required></td>'+
        '<td><input class="form-control" type="number" name="m_qty_barkrdn[]" required></td>'+
        '<td><input class="form-control" type="date" name="m_date_barkrdn[]" style="width: 150px;" required></td>'+
        '<td><input class="form-control" type="number" name="m_invoice_no_to[]" style="width: 150px;" required></td>'+
        '<td><input class="form-control" type="number" name="m_qty_dagrtn[]" required></td>'+
        '<td><input class="form-control" type="date" name="m_date_dagrtn[]" style="width: 150px;" onblur="getPrice(this.value)" required></td>'+
        '<td><textarea class="form-control" name="m_note[]" style="width: 150px;"></textarea></td>'+
        '<td><a href="#" class="btn btn-danger removeMaterial">X</td>'
        +'</tr>';
        $('.bMaterial').append(tr);
    };
    $('.removeMaterial').live('click',function(){
        var last=$('.bMaterial tr').length;
        if(last==1){
            alert('at lease one column required');
        }else{
            $(this).parent().parent().remove();
        }
    });


    $('.addExpense').on('click',function(){
        addExpense();
    });
    var ei=0;
    function addExpense(){
        var tr='<tr>'+
            '<td><select class="form-control" name="expense_type_id[]" style="width: 150px;">'+
                @foreach (App\ExpenseType::all() as $et)
                '<option value="{{$et->id}}">{{$et->name}}</option>'+
                @endforeach
            '</select></td>'+
                    '<td><input class="form-control" type="text" name="ex_invoice_no[]"></td>'+
                '<td><input class="form-control" type="number" id="cost'+ei+'" name="ex_cost[]" onblur="updateexpense(\'cost\','+ei+',this.value,'+{{ App\DollarPrice::orderby('created_at','desc')->first()->price }}+')"></td>'+
                '<td><input class="form-control" id="dollar'+ei+'"  type="number" name="ex_dollar[]" onblur="updateexpense(\'dollar\','+ei+',this.value,'+{{ App\DollarPrice::orderby('created_at','desc')->first()->price }}+')"></td>'+
                    '<td><textarea class="form-control" name="ex_note[]" style="width: 150px;"></textarea></td>'+
                    '<td><a href="#" class="btn btn-danger removeExpense">X</td>'
        +'</tr>';
        $('.bExpense').append(tr);
        ei+=1;
    };
    $('.removeExpense').live('click',function(){

            $(this).parent().parent().remove();

    });

    $('.addGass').on('click',function(){
        addGass();
    });
    $('.addFine').on('click',function(){
        addFine();
    });
    function addGass(){
        var tr='<tr>'+
            '<td><input class="form-control" type="text" name="g_invoice_no[]"></td>'+
                    '<td><input class="form-control" type="number" name="g_qty[]"></td>'+
                    '<td><input class="form-control" type="number" name="g_price[]"></td>'+
                    '<td><input class="form-control" type="date" name="g_date[]"></td>'+
                    '<td><input class="form-control" type="text" name="g_location[]"></td>'+
                    '<td><textarea class="form-control" name="g_note[]" style="width: 150px;"></textarea></td>'+
                    '<td><a href="#" class="btn btn-danger removeGass">X</td>'
        +'</tr>';
        $('.bGass').append(tr);
    };
    var gi=0;
    function addFine(){
        var tr='<tr>'+
            '<td><input class="form-control" type="text" name="f_invoice_no[]"></td>'+
                    '<td><input class="form-control" id="amountf'+gi+'"  type="number" name="f_amount[]" onblur="updatefine(\'amount\','+gi+',this.value,'+{{ App\DollarPrice::orderby('created_at','desc')->first()->price }}+')"></td>'+
                    '<td><input class="form-control" id="dollarf'+gi+'"  type="number" name="f_dollar[]" onblur="updatefine(\'dollar\','+gi+',this.value,'+{{ App\DollarPrice::orderby('created_at','desc')->first()->price }}+')"></td>'+

                    '<td><textarea class="form-control" name="f_note[]" style="width: 150px;"></textarea></td>'+
                    '<td><a href="#" class="btn btn-danger removeGass">X</td>'
        +'</tr>';
        $('.bFine').append(tr);
        gi+=1;
    };
    $('.removeGass').live('click',function(){

            $(this).parent().parent().remove();

    });
        function updateexpense(field,key,val,dollar) {
            // body...
            console.log("expense");
            if(field=='cost'){
                  document.getElementById('dollar'+key).value = (val/dollar);
              }
              else if(field=='dollar')
              {
                 document.getElementById('cost'+key).value =(val*dollar);
              }
        }
        function updatefine(field,key,val,dollar) {
            // body...
            console.log("fine");
            if(field=='amount'){
                  document.getElementById('dollarf'+key).value = (val/dollar);
              }
              else if(field=='dollar')
              {
                 document.getElementById('amountf'+key).value =(val*dollar);
              }
        }
</script>

@stop
