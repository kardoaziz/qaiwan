@extends('adminlte::page')
@section('head_scripts')
    <!-- DataTables CSS -->
    {{-- {{ HTML::style('css/plugins/dataTables.bootstrap.css') }} --}}
@stop
@section('content')
<h3>{{ __('main.trips create') }}</h3>
<form role="form" action="{{ route('trips.update',$trip->id) }}" method="POST">
    {{csrf_field()}}
      <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
    <input type="hidden" name="_method" value="PUT">
<div class="card card-info">
    <div class="card-header">
        <div class="row">
        <div class="col-md-4">
            <table class="table table-borderless">
                <tr>
                    <td><label >{{ __('main.invoice_no') }}</label></td>
                <td><input type="text" value="{{$trip->invoice_no}}" class="form-control"  id="invoice_no" name="invoice_no" placeholder="{{ __('main.invoice no') }}"></td>
                </tr>
                <tr>
                    <td> <label >{{ __('main.date') }}</label></td>
                <td><input type="date" id="date" name="t_date" class="form-control" placeholder="Date" required value="{{ $trip->date }}">
                </tr>

            </table>

        </div>
        <div class="col-md-8">

            <table class="table table-borderless">
                <tr>
                    <input type="hidden" name="driver_id" id="driver_id" value="{{$trip->drive_id}}">
                    <td> <label >{{ __('main.driver') }}</label></td>
                    <td colspan="2">
                        <input list="driverslist" name="drivers" value="{{$trip->drive_id}}-{{$trip->drive->driver->name}}"  id="drivers" class="form-control" onchange="selectdriver(this.value)" placeholder="{{ __('main.driver') }}">
                        <datalist id="driverslist">
                          @foreach (App\Drive::where('status','active')->get() as $d)
                            @if ($d->id == $trip->drive_id)
                            <option  value="{{$d->id}}-{{$d->driver->name}}">{{$d->driver->name}} - {{$d->truck->code}}</option>
                            @endif
                                    @endforeach
                        </datalist>

                    </td>
                </tr>
                <tr>
                    <td><label>{{ __('main.locations') }}</label></td>
                     <td>
                        <input type="hidden" name="from_to" id="from_to" value="">
                        {{ __('main.from') }}
                        <select id="from_location" name="from" class="form-control" onchange="selecttrip()">
                            <option value=-1>{{ __('main.select source') }}</option>
                            @foreach(App\locations::all() as $f)
                            <option @if($trip->from_id==$f->id) selected @endif value="{{ $f->id }}">{{ $f->name }}</option>
                            @endforeach
                        </select>
                    </td>
                    <td>
                        {{ __('main.to') }}
                        {{-- <input type="hidden" name="from_to" id="from_to" value=""> --}}
                        <select id="to_location" name="to" class="form-control" onchange="selecttrip()">
                            <option value=-1>{{ __('main.select destination') }}</option>
                            @foreach(App\locations::all() as $f)
                            <option @if($trip->to_id==$f->id) selected @endif value="{{ $f->id }}">{{ $f->name }}</option>
                            @endforeach
                        </select>
                    </td>
                </tr>

            </table>

        </div>
    </div>
    <div class="row">
        <div class="col-md-1">{{ __('main.price') }}</div>
    <div class="col-md-2"><input class="form-control" type="text" value="{{$trip->price}}" name="price" id="price" disabled placeholder="{{ __('main.price') }}"></div>
        <div class="col-md-1">{{ __('main.driver') }}</div>
    <div class="col-md-2"><input class="form-control" type="text" value="{{$trip->drive->driver->name}}" name="driver" id="driver" disabled placeholder="{{ __('main.driver') }}"></div>
       {{--  <div class="col-md-1">{{ __('main.from') }}</div>
        <div class="col-md-2"><input class="form-control" type="text" value="{{$trip->from_location->name}}" name="from" id="from" disabled placeholder="{{ __('main.from') }}"></div>
        <div class="col-md-1">{{ __('main.to') }}</div>
        <div class="col-md-2"><input class="form-control" type="text" value="{{$trip->to_location->name}}" name="to" id="to" disabled placeholder="{{ __('main.to') }}"></div> --}}
    </div>
    <br>
    <div class="container">
        <label for="">{{ __('main.note') }}</label>
        <textarea class="form-control" name="note" id="" cols="30" rows="3" placeholder="{{ __('main.note') }}"></textarea>
    </div>
    </div>
    <div class="card-body">
        <div class="card-header bg-success text-white"><h3>{{ __('main.materials') }}</h3></div>
        <table class="table table-bordered table-responsive">
            <thead>
                <th>{{ __('main.material') }}</th>
                <th>{{ __('main.unit') }}</th>
                <th>{{ __('main.invoice_no_from') }}</th>
                <th>{{ __('main.qty barkrdn') }}</th>
                <th>{{ __('main.date barkrdn') }}</th>
                <th>{{ __('main.invoice_no_to') }}</th>
                <th>{{ __('main.qty dagrtn') }}</th>
                <th>{{ __('main.date dagrtn') }}</th>
                <th>{{ __('main.note') }}</th>
                <th><a href="#" class="addMaterial"><i class="fa fa-fw fa-plus"></i></a></th>
            </thead>
            <tbody class="bMaterial">
                @foreach ($tripMaterials as $m2)
                <tr>
                    <td><select class="form-control" name="material_id[]" style="width: 150px;">
                            @foreach (App\Material::all() as $m)
                                <option  value="{{$m->id}}" @if ($m2->material_id==$m->id) selected @endif>{{$m->name}}</option>
                            @endforeach
                        </select>
                    </td>
                     <td><select class="form-control" name="unit[]" style="width: 80px;" value="{{ $m->unit }}">
                                <option value="ton">{{__('main.ton')}}</option>
                                <option value="kilo">{{__('main.kilo')}}</option>
                                <option value="litr">{{__('main.litr')}}</option>
                        </select>
                    </td>
                <td><input class="form-control" value="{{$m2->invoice_no}}" type="text" name="m_invoice_no[]"></td>
                    <td><input class="form-control" value="{{$m2->qty_barkrdn}}" type="number" name="m_qty_barkrdn[]" style="width: 150px;"></td>
                    <td><input class="form-control" type="date" value="{{$m2->date_barkrdn}}" name="m_date_barkrdn[]" style="width: 170px;"></td>
                    <td><input class="form-control" type="number" value="{{$m2->invoice_no_to}}" name="m_invoice_no_to[]" style="width: 150px;"></td>
                    <td><input class="form-control" type="number" value="{{$m2->qty_dagrtn}}" name="m_qty_dagrtn[]" style="width: 150px;"></td>
                    <td><input class="form-control" type="date" value="{{$m2->date_dagrtn}}" name="m_date_dagrtn[]" style="width: 170px;" onblur="getPrice(this.value)"></td>
                    <td><textarea class="form-control" name="m_note[]" style="width: 150px;">{{$m2->note}}</textarea></td>
                    <td><a href="#" class="btn btn-danger removeMaterial">X</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <div class="card-body">
       <div class="card-header bg-danger text-white"> <h3>{{ __('main.expenses') }}</h3></div>
        <table class="table table-bordered">
            <thead>
                <th>{{ __('main.expense types') }}</th>
                <th>{{ __('main.invoice_no') }}</th>
                <th>{{ __('main.cost') }}</th>
                <th>{{ __('main.dollar') }}</th>
                <th>{{ __('main.note') }}</th>
                <th><a href="#" class="addExpense"><i class="fa fa-fw fa-plus"></i></a></th>
            </thead>
            <tbody class="bExpense">
                @foreach ($tripExpense as $ex)
                <tr>
                    <td><select class="form-control" name="expense_type_id[]" style="width: 150px;">
                        @foreach (App\ExpenseType::all() as $et)
                            @if ($et->id == $ex->expense_type_id)
                            <option value="{{$et->id}}" selected>{{$et->name}}</option>
                            @endif
                        @endforeach
                    </select></td>
                    <td><input class="form-control" value="{{$ex->invoice_no}}" type="text" name="ex_invoice_no[]"></td>
                    <td><input class="form-control" id="cost{{ $loop->iteration }}" value="{{$ex->cost}}" type="number" name="ex_cost[]" onblur="updateexpense('cost',{{ $loop->iteration }},this.value,{{ $ex->dollar_price }})"></td>
                    <td><input class="form-control" id="dollar{{ $loop->iteration }}" value="{{$ex->dollar}}" type="number" name="ex_dollar[]" onblur="updateexpense('dollar',{{ $loop->iteration }},this.value,{{ $ex->dollar_price }})"></td>
                    <td><textarea class="form-control" name="ex_note[]" style="width: 150px;">{{$ex->note}}</textarea></td>
                    <td><a href="#" class="btn btn-danger removeExpense">X</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <div class="card-body">
        <div class="card-header bg-info text-white"><h3>{{ __('main.gass') }}</h3></div>
        <table class="table table-bordered">
            <thead>
                <th>{{ __('main.invoice_no') }}</th>
                <th>{{ __('main.qty') }}</th>
                <th>{{ __('main.price') }}</th>
                <th>{{ __('main.date') }}</th>
                <th>{{ __('main.gass location') }}</th>
                <th>{{ __('main.note') }}</th>
                <th><a href="#" class="addGass"><i class="fa fa-fw fa-plus"></i></a></th>
            </thead>
            <tbody class="bGass">
                @foreach ($tripGass as $tg)
                <tr>
                <td><input class="form-control" value="{{$tg->invoice_no}}" type="text" name="g_invoice_no[]"></td>
                    <td><input class="form-control" value="{{$tg->qty}}" type="number" name="g_qty[]"></td>
                    <td><input class="form-control" value="{{$tg->price}}" type="number" name="g_price[]"></td>
                    <td><input class="form-control" value="{{Carbon\Carbon::parse($tg->date)->format('d/m/Y')}}" type="date" name="g_date[]"></td>
                    <td><input class="form-control" value="{{$tg->location}}" type="text" name="g_location[]"></td>
                    <td><textarea class="form-control" name="g_note[]" style="width: 150px;">{{$tg->note}}</textarea></td>
                    <td><a href="#" class="btn btn-danger removeGass">X</td>
                </tr>
                @endforeach
            </tbody>
        </table>

    </div>
    <div class="card-body">
        <div class="card-header bg-warning text-white"><h3>{{ __('main.fines') }}</h3></div>
        <table class="table table-bordered">
            <thead>
                <th>{{ __('main.invoice_no') }}</th>
                <th>{{ __('main.amount') }}</th>
                <th>{{ __('main.dollar') }}</th>
                <th>{{ __('main.note') }}</th>
                <th><a href="#" class="addFine"><i class="fa fa-fw fa-plus"></i></a></th>
            </thead>
            <tbody class="bFine">
                @foreach ($tripFine as $tf)
                <tr>
                <td><input class="form-control" value="{{$tf->invoice_no}}" type="text" name="f_invoice_no[]"></td>
                    <td><input class="form-control" value="{{$tf->amount}}" type="number" name="f_amount[]" id="amountf{{ $loop->iteration }}" onblur="updatefine('amount',{{ $loop->iteration }},this.value,{{ $tf->dollar_price }})"></td>
                    <td><input class="form-control" value="{{$tf->dollar}}" type="number" name="f_dollar[]" id="dollarf{{ $loop->iteration }}" onblur="updatefine('dollar',{{ $loop->iteration }},this.value,{{ $tf->dollar_price }})"></td>
                    <td><textarea class="form-control" name="f_note[]" style="width: 150px;">{{$tf->note}}</textarea></td>
                    <td><a href="#" class="btn btn-danger removeGass">X</td>
                </tr>
                @endforeach
            </tbody>
        </table>

        <div class="card-footer">
                <button type="submit" class="btn btn-success"><i class="fa fa-fw fa-save"></i>{{ __('main.save') }}</button>
              </div>
    </div>
</div>

</form>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js"></script>
<script>
                function selectdriver(id) {

$('#driver_id').val(id.split("-")[0]);
$('#driver').val(id.split("-")[1]);
}
function selecttrip(id) {

var from_id = $('#from_location').val();
    var to_id = $('#to_location').val();
    console.log(from_id);
    console.log(to_id);
    // $('#from').val($('#from_location').val());
    // $('#to').val($('#to_location').val());
    if(from_id>0 && to_id>0)
    {
        console.log("get Price");
        $.ajax({
          type:'POST',
          headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
          url:'{{ route("trip.getprice") }}',
          data:{ id:'-1',date:'',from_id:from_id ,to_id:to_id},
           success:function(data){
            // console.log(data);
            if(data=="error")
            {
                alert('هیچ نرخێک دیاری ناکراوا بۆ ئەو شوێنە');
            }
            else{
                console.log(data);
               $('#price').val(data['price']);
               $('#from_to').val(data['id']);
           }
           }
        });
    }else{
        console.log("select the other");
    }
}
function getPrice(date) {
    console.log(date);
    id = $('#from_to').val();
    console.log($('#from_to').val());
    $.ajax({
          type:'POST',
          headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
          url:'{{ route("trip.getprice") }}',
          data:{ id:id,date:date},
           success:function(data){
            console.log(data);
           $('#price').val(data['price']);
           $('#from_to').val(data['id']);
           }
        });
    // body...
}
        </script>
<script type="text/javascript">

    $('.addMaterial').on('click',function(){
       addMaterial();
    });
    function addMaterial(){
        var tr='<tr>'+
        '<td><select class="form-control" name="material_id[]" style="width: 150px;">'+
            @foreach (App\Material::all() as $m)
            '<option value="{{$m->id}}">{{$m->name}}</option>'+
            @endforeach
            '</select></td>'+
        '<td><input class="form-control" type="text" name="m_invoice_no[]"></td>'+
        '<td><input class="form-control" type="number" name="m_qty_barkrdn[]"></td>'+
        '<td><input class="form-control" type="date" name="m_date_barkrdn[]" style="width: 150px;"></td>'+
        '<td><input class="form-control" type="number" name="m_invoice_no_to[]" style="width: 150px;"></td>'+
        '<td><input class="form-control" type="number" name="m_qty_dagrtn[]"></td>'+
        '<td><input class="form-control" type="date" name="m_date_dagrtn[]" style="width: 150px;" onblur="getPrice(this.value)"></td>'+
        '<td><textarea class="form-control" name="m_note[]" style="width: 150px;"></textarea></td>'+
        '<td><a href="#" class="btn btn-danger removeMaterial">X</td>'
        +'</tr>';
        $('.bMaterial').append(tr);
    };
    $('.removeMaterial').live('click',function(){
        var last=$('.bMaterial tr').length;
        if(last==1){
            alert('at lease one column required');
        }else{
            $(this).parent().parent().remove();
        }
    });


    $('.addExpense').on('click',function(){
        addExpense();
    });
    var ei={{ $tripExpense->count()+1 }};
    function addExpense(){
        var tr='<tr>'+
            '<td><select class="form-control" name="expense_type_id[]" style="width: 150px;">'+
                @foreach (App\ExpenseType::all() as $et)
                '<option value="{{$et->id}}">{{$et->name}}</option>'+
                @endforeach
            '</select></td>'+
                    '<td><input class="form-control" type="text" name="ex_invoice_no[]"></td>'+
                    '<td><input class="form-control" type="number" name="ex_cost[]" id="cost'+ei+'" onblur="updateexpense(\'cost\','+ei+',this.value,'+{{ App\DollarPrice::orderby('created_at','desc')->first()->price }}+')"></td>'+
                    '<td><input class="form-control" type="number" name="ex_dollar[]"  id="dollar'+ei+'" onblur="updateexpense(\'dollar\','+ei+',this.value,'+{{ App\DollarPrice::orderby('created_at','desc')->first()->price }}+')"></td>'+
                    '<td><textarea class="form-control" name="ex_note[]" style="width: 150px;"></textarea></td>'+
                    '<td><a href="#" class="btn btn-danger removeExpense">X</td>'
        +'</tr>';
        $('.bExpense').append(tr);
        ei+=1;
    };
    $('.removeExpense').live('click',function(){

            $(this).parent().parent().remove();

    });

    $('.addGass').on('click',function(){
        addGass();
    });
    $('.addFine').on('click',function(){
        addFine();
    });
    function addGass(){
        var tr='<tr>'+
            '<td><input class="form-control" type="text" name="g_invoice_no[]"></td>'+
                    '<td><input class="form-control" type="number" name="g_qty[]"></td>'+
                    '<td><input class="form-control" type="number" name="g_price[]"></td>'+
                    '<td><input class="form-control" type="date" name="g_date[]"></td>'+
                    '<td><input class="form-control" type="text" name="g_location[]"></td>'+
                    '<td><textarea class="form-control" name="g_note[]" style="width: 150px;"></textarea></td>'+
                    '<td><a href="#" class="btn btn-danger removeGass">X</td>'
        +'</tr>';
        $('.bGass').append(tr);
    };
    var gi={{ $tripFine->count()+1 }};
    function addFine(){
        var tr='<tr>'+
            '<td><input class="form-control" type="text" name="f_invoice_no[]"></td>'+
                    '<td><input class="form-control" id="amountf'+gi+'"  type="number" name="f_amount[]" onblur="updatefine(\'amount\','+gi+',this.value,'+{{ App\DollarPrice::orderby('created_at','desc')->first()->price }}+')"></td>'+
                    '<td><input class="form-control" id="dollarf'+gi+'" type="number" name="f_dollar[]" onblur="updatefine(\'dollar\','+gi+',this.value,'+{{ App\DollarPrice::orderby('created_at','desc')->first()->price }}+')"></td>'+

                    '<td><textarea class="form-control" name="f_note[]" style="width: 150px;"></textarea></td>'+
                    '<td><a href="#" class="btn btn-danger removeGass">X</td>'
        +'</tr>';
        $('.bFine').append(tr);
        gi+=1;
    };
    $('.removeGass').live('click',function(){

            $(this).parent().parent().remove();

    });
    function updateexpense(field,key,val,dollar) {
            // body...
            console.log("expense");
            if(field=='cost'){
                  document.getElementById('dollar'+key).value = (val/dollar);
              }
              else if(field=='dollar')
              {
                 document.getElementById('cost'+key).value =(val*dollar);
              }
        }
        function updatefine(field,key,val,dollar) {
            // body...
            console.log("fine");
            if(field=='amount'){
                  document.getElementById('dollarf'+key).value = (val/dollar);
              }
              else if(field=='dollar')
              {
                 document.getElementById('amountf'+key).value =(val*dollar);
              }
        }
</script>

@stop
