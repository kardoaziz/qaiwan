@extends('adminlte::page')
@section('head_scripts')
    <!-- DataTables CSS -->
    {{-- {{ HTML::style('css/plugins/dataTables.bootstrap.css') }} --}}
@stop
@section('content')
<h4>{{ __('main.trips view') }}</h4>

<section class="invoice">
    @php
    $gastotal = App\TripGass::where('trip_id',$trip->id)->select(DB::raw('IFNULL(sum(qty*price),0) as total'))->first()->total;
    @endphp
        <div class="page-header">
        <center><img src="{{asset('img/logo_small.png')}}" style="width: 100px; height:100px">
            <h5>کۆمپانیای قەیوان</h5>
            <br>
        </center>
            <table class="table table-bordered">
                <tr>
                    <td>{{ __('main.invoice_no') }} : {{$trip->invoice_no}}</td>
                    <td>{{ __('main.date') }} : {{ Carbon\Carbon::parse($trip->date)->format('d/m/Y') }}</td>
                    <td>{{ __('main.driver') }} : {{$trip->drive->driver->name}}</td>
                    <td>{{ __('main.from') }} : {{$trip->from_location->name}}</td>
                    <td>{{ __('main.to') }} : {{$trip->to_location->name}}</td>
                    <td>{{ __('main.price') }} : {{ number_format($trip->price)}}</td>
                </tr>
                <tr>
                    <td colspan="4" rowspan="4"></td>
                    <td >{{ __('main.gass') }}</td>
                    <td >{{ number_format($gastotal) }}</td>
                </tr>
                <tr>
                    {{-- <td colspan="4"></td> --}}
                    <td >{{ __('main.fine') }}</td>
                    <td >{{ number_format($tripFine->sum('cost')) }}</td>
                </tr>
                 <tr>
                    {{-- <td colspan="4"></td> --}}
                    <td >{{ __('main.expense') }}</td>
                    <td >{{ number_format($tripExpense->sum('cost')) }}</td>
                </tr>
                <tr>
                    {{-- <td colspan="4"></td> --}}
                    <td >{{ __('main.total') }}</td>
                    <td >{{ number_format(($trip->price+$tripExpense->sum('cost'))-($gastotal+$tripFine->sum('cost'))) }}</td>
                </tr>
            </table>
        </div>

        <div class="card-body">
            <table class="table table-bordered">
                <thead class="thead-dark">
                    <tr>
                        <th>{{ __('main.material') }}</th>
                        <th>{{ __('main.invoice_no_from') }}</th>
                        <th>{{ __('main.qty barkrdn') }}</th>
                        <th>{{ __('main.date barkrdn') }}</th>
                        <th>{{ __('main.invoice_no_to') }}</th>
                        <th>{{ __('main.qty dagrtn') }}</th>
                        <th>{{ __('main.date dagrtn') }}</th>
                        <th>{{ __('main.note') }}</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($tripMaterials as $m)
                        <tr>
                            <td>{{$m->material->name}}</td>
                            <td>{{$m->invoice_no}}</td>
                            <td>{{$m->qty_barkrdn}}</td>
                            <td>{{Carbon\Carbon::parse($m->date_barkrdn)->format('d/m/Y')}}</td>
                            <td>{{$m->invoice_no_to}}</td>
                            <td>{{$m->qty_dagrtn}}</td>
                            <td>{{Carbon\Carbon::parse($m->date_dagrtn)->format('d/m/Y')}}</td>
                            <td>{{$m->note}}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

        <div class="card-body">
            <div class=" p-0">{{ __('main.expenses') }}</div>
            <table class="table table-bordered">
                <thead class="thead-dark">
                    <tr>
                        <th>{{ __('main.expense types') }}</th>
                        <th>{{ __('main.invoice_no') }}</th>
                        <th>{{ __('main.cost') }}</th>
                        <th>{{ __('main.dollar') }}</th>
                        <th>{{ __('main.note') }}</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($tripExpense as $ex)
                        <tr>
                            <td>{{$ex->type->name}}</td>
                            <td>{{$ex->invoice_no}}</td>
                            <td>{{$ex->cost}}</td>
                            <td>{{$ex->dollar}}</td>
                            <td>{{$ex->note}}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

        <div class="card-body">
            <div class=" p-0">{{ __('main.gass') }}</div>
            <table class="table table-bordered">
                <thead class="thead-dark">
                    <tr>
                        <th>{{ __('main.invoice_no') }}</th>
                        <th>{{ __('main.qty') }}</th>
                        <th>{{ __('main.price') }}</th>
                        <th>{{ __('main.date') }}</th>
                        <th>{{ __('main.gass location') }}</th>
                        <th>{{ __('main.note') }}</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($tripGass as $g)
                        <tr>
                            <td>{{$g->invoice_no}}</td>
                            <td>{{$g->qty}}</td>
                            <td>{{$g->price}}</td>
                            <td>{{Carbon\Carbon::parse($g->date)->format('d/m/Y')}}</td>
                            <td>{{$g->location}}</td>
                            <td>{{$g->note}}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

        <div class="card-body">
            <div class=" p-0">{{ __('main.fines') }}</div>
            <table class="table table-bordered">
                <thead class="thead-dark">
                    <tr>
                        <th>{{ __('main.invoice_no') }}</th>
                        <th>{{ __('main.amount') }}</th>
                        <th>{{ __('main.dollar') }}</th>
                        <th>{{ __('main.note') }}</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($tripFine as $f)
                        <tr>
                            <td>{{$f->invoice_no}}</td>
                            <td>{{$f->amount}}</td>
                            <td>{{$f->dollar}}</td>
                            <td>{{$f->note}}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>

    </div>
</section>

@stop
