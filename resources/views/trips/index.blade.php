@extends('adminlte::page')
@section('head_scripts')
    <!-- DataTables CSS -->
    {{-- {{ HTML::style('css/plugins/dataTables.bootstrap.css') }} --}}
@stop
@section('content')
<h3>{{ __('main.trips') }}</h3>
<div class="card card-light">
    <div class="card-header">
        @can('add_trip')<a  href="{{ route('trips.create') }}"><button type="button" class="btn btn-success"><i class="fa fa-fw fa-plus"></i>{{ __('main.new') }}</button></a>@endcan
    </div>
    <div class="card-body">
        <div class="row bg-info">
            <div class="col-md-2"></div>
            <div class="col-md-8 ">
                <table class="table table-borderless justify-content-center" >
                <tbody>
                     <tr>
                    <td>{{ __('main.from date') }}:</td>
                    <td><input class="form-control" type="text" id="from_date" name="from_date"></td>
                    <td>{{ __('main.to date') }}:</td>
                    <td><input class="form-control" type="text" id="to_date" name="to_date"></td>
                </tr>

                </tbody>
            </table>
        </div>
        </div>
        <br>
    <div class="table-responsive">

<table id="example" class="table table-hover">
        <thead class="table-active">

            <tr>
                <th>#</th>
                <th>{{ __('main.invoice_no') }}</th>
                <th>{{ __('main.from') }}</th>
                <th>{{ __('main.to') }}</th>
                <th>{{ __('main.price') }}</th>
                <th>{{ __('main.driver') }}</th>
                <th>{{ __('main.date') }}</th>
                <th>{{ __('main.note') }}</th>
                @can('edit_trip')<th>{{ __('main.edit') }}</th>@endcan
                @can('delete_trip')<th>{{ __('main.delete') }}</th>@endcan
            </tr>

        </thead>
        <tbody>

        	@foreach(App\Trip::orderby('id','desc')->get() as $d)
            <tr>
                <td>{{ $loop->iteration }}</td>
                <td><a href="{{ route('trips.show',$d->id) }}">{{ $d->invoice_no }}</a></td>
                <td>{{ $d->from_location->name }}</td>
                <td>{{ $d->to_location->name }}</td>
                <td>{{ $d->price }}</td>
                <td>{{ $d->drive->driver->name }}</td>
                <td>{{Carbon\Carbon::parse($d->date)->format('d/m/Y')}}</td>
                <td>{{ $d->note }}</td>
                @can('edit_trip')<td>
                   <form method="GET" action="{{ route('trips.edit',$d->id) }}">
                                    {{ csrf_field() }}
  <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                    <div class="form-group">
                                        <input type="submit" class="btn btn-info btn-sm " value="{{ __('main.edit') }}" >

                                    </div>
                                </form></td>@endcan
               @can('delete_trip') <td>
                                <form action="{{ route('trips.destroy',$d->id) }}" method="post">
                         {{ csrf_field() }}
                           <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                <input type="hidden" name="_method" value="DELETE"  >

                    <a style="padding: 0px;
                      font-size: 12px;
                      color: red;" role="menuitem" tabindex="-1"  onclick="return confirm('{{ __('main.are you sure') }}')">
                      <button type="submit" class="btn btn-sm btn-danger"><i class="fa fa-fw fa-trash"></i></button>
                    </a>
                  </form>
                  </td>@endcan
            </tr>
             @endforeach
        </tbody>
        <tfoot>
            <tr>
                <th>#</th>
                <th>{{ __('main.invoice_no') }}</th>
                <th>{{ __('main.from') }}</th>
                <th>{{ __('main.to') }}</th>
                <th>{{ __('main.price') }}</th>
                <th>{{ __('main.driver') }}</th>
                <th>{{ __('main.date') }}</th>
                <th>{{ __('main.note') }}</th>
                @can('edit_trip')<th>{{ __('main.edit') }}</th>@endcan
                @can('delete_trip')<th>{{ __('main.delete') }}</th>@endcan
            </tr>
        </tfoot>
    </table>
        </div>

    </div>
</div>
   <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.6/jquery.min.js" type="text/javascript"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js" type="text/javascript"></script>
    <link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="Stylesheet" type="text/css" />
<script type="text/javascript">
// Date range filter
$(document).ready(function() {
minDateFilter = new Date();
minDateFilter.setDate(minDateFilter.getDate()-30);
maxDateFilter = new Date();

$.fn.dataTableExt.afnFiltering.push(
  function(oSettings, aData, iDataIndex) {
       //alert("inside table");
      console.log( "----");
      //console.log( table.index("date"));
      var parts = aData[6].split('/');
       aData._date = new Date( parts[2]+"-"+parts[1]+"-"+parts[0]);
      // console.log(aData._date);
      // console.log("d");
      // console.log(parts[0]);
      // console.log(parts[1]);
      // console.log(parts[2]);
    // if (typeof aData._date == 'undefined') {
      // aData._date = new Date(aData[6]);

    // }
    // console.log(minDateFilter);

     //  console.log(maxDateFilter);
      // console.log(aData._date);
    if (minDateFilter && !isNaN(minDateFilter)) {
      if (aData._date < minDateFilter) {
       // console.log("false min");
        return false;
      }
    }

    if (maxDateFilter && !isNaN(maxDateFilter)) {
      if (aData._date > maxDateFilter ) {
       // console.log("false max");
        return false;
      }
    }
  //  console.log("true");
    return true;
  }
);
   // $('#from_date, #to_date').keyup( function() {
   //  console.log("search by date");
   //      table.draw();
   //  } );
   $('.ui-datepicker-trigger').css("display", "none");
   });

 $("#from_date").datepicker({
     format: 'yyyy-mm-dd',
    showOn: "both",
    buttonImageOnly: false,
    "onSelect": function(date) {
      minDateFilter = new Date(date);
      table.draw();
    }
  }).keyup(function() {
    minDateFilter = new Date(this.value,'yyyy-mm-dd');
    table.draw();
  });

  $("#to_date").datepicker({
    format: 'yyyy-mm-dd',
    showOn: "both",
    buttonImage: "images/calendar.gif",
    buttonImageOnly: false,
    "onSelect": function(date) {
      maxDateFilter = new Date(date);
      maxDateFilter.setDate(maxDateFilter.getDate()+1);
      table.draw();
    }
  }).keyup(function() {
    maxDateFilter = new Date(this.value,'yyyy-mm-dd');
    table.draw();
  });

 // $('#example').DataTable();

</script>
@stop
