@extends('adminlte::page')
@section('head_scripts')
    <!-- DataTables CSS -->
    {{-- {{ HTML::style('css/plugins/dataTables.bootstrap.css') }} --}}
@stop
@section('content')
<h3>{{ __('main.gass to trucks') }}</h3>
<div class="card card-light">
    <div class="card-header bg-dark">
        <a  href="{{ route('gass_purchase.create') }}"><button type="button" class="btn btn-light"><i class="fa fa-fw fa-plus"></i>{{ __('main.new') }}</button></a>
    </div>
    <div class="card-body">
        <div class="table-responsive">
        <table id="example" class="table table-hover" >
                <thead class="table-light">
                    <tr>
                        <th>#</th>

                        <th>{{ __('main.amount') }}</th>
                        <th>{{ __('main.date') }}</th>
                        <th>{{ __('main.note') }}</th>
                        <th>{{ __('main.edit') }}</th>
                        <th>{{ __('main.print') }}</th>
                        <th>{{ __('main.delete') }}</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach(App\gassQtyPurchase::all() as $d)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $d->qty }}</td>
                        <td>{{ $d->date }}</td>
                        <td>{{ $d->note }}</td>
                        <td>
                            <form method="GET" action="{{ route('gass_purchase.edit',$d->id) }}">
                                             {{ csrf_field() }}
  <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                             <div class="form-group">
                                                 <input type="submit" class="btn btn-info btn-sm " value="{{ __('main.edit') }}" >

                                             </div>
                                         </form></td>
                           <td>
                                            <form method="GET" action="{{ route('gass_purchase.show',$d->id) }}">
                                                             {{ csrf_field() }}
  <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                                             <div class="form-group">
                                                                 <input type="submit" class="btn btn-info btn-sm " value="{{ __('main.print') }}" >

                                                             </div>
                                                         </form></td>
                        <td>
                                        <form action="{{ route('gass_purchase.destroy',$d->id) }}" method="post">
                                 {{ csrf_field() }}
                                   <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                        <input type="hidden" name="_method" value="DELETE"  >

                            <a style="padding: 0px;
                              font-size: 12px;
                              color: red;" role="menuitem" tabindex="-1"  onclick="return confirm('{{ __('main.are you sure') }}')">
                              <button type="submit" class="btn btn-sm btn-danger"><i class="fa fa-fw fa-trash"></i></button>
                            </a>
                          </form>
                          </td>
                    </tr>
                     @endforeach
                </tbody>
            </table>
        </div>
        </div>
</div>
@stop
