@extends('adminlte::page')
@section('head_scripts')
    <!-- DataTables CSS -->
    {{-- {{ HTML::style('css/plugins/dataTables.bootstrap.css') }} --}}
@stop
@section('content')
<h3>{{ __('main.snduq') }}</h3>
<div class="card card-light">
    <div class="card-header bg-info">
         @can('add_snduq')<a class="btn btn-light noprint" data-toggle="modal" data-target="#new-snduq-modal" style="font-size: 18px;float:right;color:grey !important;">
                <i class="fa fa-plus"></i> {{ __("main.new") }}
              </a>@endcan
    </div>
    <div class="card-body">
        <div class="table-responsive">
        <table id="example" class="table table-hover" >
                <thead class="table-primary">
                    <tr>
                        <th>#</th>
                        <th>{{ __('main.name') }}</th>
                        <th>{{ __('main.default') }}</th>
                        <th>{{ __('main.initial_balance') }}</th>
                        <th>{{ __('main.initial dollar') }}</th>
                        <th>{{ __('main.total slfa') }}</th>
                        <th>{{ __('main.total income') }}</th>
                        <th>{{ __('main.total spent') }}</th>
                        <th>{{ __('main.total incoming') }}</th>
                        <th>{{ __('main.current balance') }}</th>
                        <th>{{ __('main.description') }}</th>
                        
                         @can('edit_snduq')<th>{{ __('main.edit') }}</th>@endcan
                        {{-- @can('delete_snduq') <th>{{ __('main.delete') }}</th>@endcan --}}
                    </tr>
                </thead>
                <tbody>
                    @php
                    $items = App\Snduq::all();
                    @endphp
                    @foreach($items as $d)
                    <tr>
                       
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $d->name }}</td>
                        <td><input type="checkbox" disabled="" @if($d->default==1) checked @endif></td>
                        <td>{{ number_format($d->initial_balance,2) }}</td>
                        <td>{{ number_format($d->dollar,2) }}</td>
                        <td>{{ number_format(App\Slfa::where('snduq_id',$d->id)->sum('amount'),2) }}</td>
                        <td>{{ number_format(App\ProjectPayment::where('snduq_id',$d->id)->sum('amount'),2) }}</td>
                        <td>{{ 
                          number_format(
                            App\DrivePayment::where('snduq_id',$d->id)->sum('amount')+
                            App\Expenses::where('snduq_id',$d->id)->sum('cost')+
                            App\BankTransfer::where('bank_id_from',$d->id)->sum('amount')
                          ,2) }}
                        </td>
                        <td>{{ 
                          number_format(
                            App\BankTransfer::where('bank_id_to',$d->id)->sum('amount')
                          ,2) }}
                        </td>
                        <td class="bg-success">{{ 
                          number_format(
                            App\Slfa::where('snduq_id',$d->id)->sum('amount')+App\ProjectPayment::where('snduq_id',$d->id)->sum('amount')+
                            App\BankTransfer::where('bank_id_to',$d->id)->sum('amount')-
                            (App\DrivePayment::where('snduq_id',$d->id)->sum('amount')+
                            App\Expenses::where('snduq_id',$d->id)->sum('cost')+
                            App\BankTransfer::where('bank_id_from',$d->id)->sum('amount'))
                          ,2) }}
                        </td>
                        <td>{{ $d->description }}</td>
                        
                         @can('edit_snduq')<td>
                           <form method="GET" action="{{ route('snduq.edit',$d->id) }}">
                                            {{ csrf_field() }}
  <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                            <div class="form-group">
                                                <input type="submit" class="btn btn-info btn-sm " value="{{ __('main.edit') }}" >

                                            </div>
                                        </form></td>@endcan
                       {{-- @can('delete_snduq')  <td>
                                        <form action="{{ route('snduq.destroy',$d->id) }}" method="post">
                                 {{ csrf_field() }}
                                        <input type="hidden" name="_method" value="DELETE"  >

                            <a style="padding: 0px;
                              font-size: 12px;
                              color: red;" role="menuitem" tabindex="-1"  onclick="return confirm('{{ __('main.are you sure') }}')">
                              <button type="submit" class="btn btn-sm btn-danger"><i class="fa fa-fw fa-trash"></i></button>
                            </a>
                          </form>
                          </td>@endcan --}}
                    </tr>
                     @endforeach
                     
                </tbody>
                <tfoot>
                    <tr>
                         <td colspan="5">
                             {{ __('main.total') }}
                         </td>
                         <td  class="text-center">{{ number_format(App\Slfa::sum('amount'),2) }}</td>
                         <td  class="text-center">{{ number_format(App\ProjectPayment::sum('amount'),2) }}</td>
                         <td  class="text-center">{{ 
                          number_format(
                            App\DrivePayment::sum('amount')+
                            App\Expenses::sum('cost')+
                            App\BankTransfer::sum('amount')
                          ,2) }}</td>
                         <td  class="text-center">{{ 
                          number_format(
                            App\BankTransfer::sum('amount')
                          ,2) }}</td>
                         <td  class="text-center bg-success">
                           {{ 
                          number_format(
                            App\Slfa::sum('amount')+App\ProjectPayment::sum('amount')+
                            App\BankTransfer::sum('amount')-
                            (App\DrivePayment::sum('amount')+
                            App\Expenses::sum('cost')+
                            App\BankTransfer::sum('amount'))
                          ,2) }}
                         </td>
                       
                         <td colspan="3"></td>
                     </tr>
                </tfoot>
            </table>
        </div>
        </div>
</div>
<div class="modal fade" id="new-snduq-modal" >
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" align="center"><b> {{ __("main.new") }}  {{ __("main.snduq") }}</b></h4>
      </div>
      <div class="modal-body" style="overflow-y: scroll;">
        <form action="{{ route('snduq.store') }}" method="POST" enctype="multipart/form-data">
                {{csrf_field()}}
          <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
          

          <div class="form-group">
            <label for="exampleInputEmail1"> {{ __("main.name") }}</label>
              <input type="text" id="name" name="name" class="form-control" placeholder="name" required >
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1"> {{ __("main.initial_balance") }}</label>
              <input type="number" step="any" id="initial_balance" name="initial_balance" class="form-control" placeholder="{{ __('main.initial_balance') }}" >
          </div>
            
          <div class="form-group">
            <label for="exampleInputEmail1"> {{ __("main.initial dollar") }}</label>
              <input type="number" step="any" id="dollar" name="dollar" class="form-control" placeholder="{{ __('main.dollar') }}" >
          </div>
          
          <div class="form-group">
            <label for="exampleInputEmail1"> {{ __("main.description") }}</label>
              <input type="text" id="description" name="description" class="form-control" placeholder="{{ __('main.description') }}"  >
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1"> {{ __("main.default") }}</label>
              <select name="default">
                <option value="0">{{ __('main.no') }}</option>
                <option value="1">{{ __('main.yes') }}</option>
                  
              </select>
          </div>


          <button type="submit" class="btn btn-primary"> {{ __("main.add") }}</button>
        </form>
      </div>
      </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default pull-left" data-dismiss="modal"> {{ __("main.cancel") }}</button>

          </div>
    </div>
  </div>
</div>
        </div>
    </div>
@stop
