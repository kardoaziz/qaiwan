 @extends('adminlte::page')
@section('head_scripts')
    <!-- DataTables CSS -->
    {{-- {{ HTML::style('css/plugins/dataTables.bootstrap.css') }} --}}
@stop
@section('content')
 <div class="modal-content">
      <div class="modal-header">
       {{--  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button> --}}
        <h4 class="modal-title" align="center"><b> {{ __("main.update") }}  {{ __("main.snduq") }}</b></h4>
      </div>
      <div class="modal-body" style="overflow-y: scroll;">
        <form action="{{ route('snduq.update',$snduq->id) }}" method="POST" >
                {{csrf_field()}}
                  <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
<input type="hidden" name="_method" value="PUT">          

          <div class="form-group">
            <label for="exampleInputEmail1"> {{ __("main.name") }}</label>
              <input type="text" id="name" name="name" class="form-control" placeholder="name" required value="{{ $snduq->name }}">
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1"> {{ __("main.initial_balance") }}</label>
              <input type="number" step="any" id="initial_balance" name="initial_balance" class="form-control" placeholder="{{ __('main.initial_balance') }}" value="{{ $snduq->initial_balance }}">
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1"> {{ __("main.initial dollar") }}</label>
              <input type="number" step="any" id="dollar" name="dollar" class="form-control" placeholder="{{ __('main.dollar_price') }}" value="{{ $snduq->dollar }}">
          </div>
          
          <div class="form-group">
            <label for="exampleInputEmail1"> {{ __("main.description") }}</label>
              <input type="text" id="description" name="description" class="form-control" placeholder="{{ __('main.description') }}"  value="{{ $snduq->description }}">
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1"> {{ __("main.default") }}</label>
              <select name="default" class="form-control" value="{{ $snduq->default }}">
                <option value="0">{{ __('main.no') }}</option>
                <option value="1">{{ __('main.yes') }}</option>
                  
              </select>
          </div>

          <button type="submit" class="btn btn-primary"> {{ __("main.add") }}</button>
        </form>
      </div>
       {{--  <div class="modal-footer">
            <button type="button" class="btn btn-default pull-left" data-dismiss="modal"> {{ __("main.cancel") }}</button>

          </div> --}}
    </div>
    @stop