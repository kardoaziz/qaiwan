@extends('adminlte::page')
@section('head_scripts')
    <!-- DataTables CSS -->
    {{-- {{ HTML::style('css/plugins/dataTables.bootstrap.css') }} --}}
@stop
@section('content')
<style type="text/css">
	td{
		text-align: center;
	}
	th {
		text-align: center !important;
	}
</style>

<div class="card card-primary">
            <div class="card-header with-border text-center">
              {{ __('main.update driver') }}
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form role="form" action="{{ route('drivers.update',$driver->id) }}" method="POST">
            	     {{csrf_field()}}
          <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
          <input type="hidden" name="_method" value="PUT">
              <div class="card-body">
                <div class="form-group">
                  <label >{{ __('main.name') }}</label>
                  <input type="text" class="form-control"  placeholder="{{ __('main.name') }}" name="name" value="{{ $driver->name }}" required>
                </div>
                <div class="form-group">
                  <label >{{ __('main.phone') }}</label>
                  <input type="text" class="form-control"  placeholder="{{ __('main.phone') }}" name="phone" value="{{ $driver->phone }}" required>
                </div>
                <div class="form-group">
                  <label >{{ __('main.kafil name') }}</label>
                  <input type="text" class="form-control"  placeholder="{{ __('main.kafil name') }}" name="kafil_name" value="{{ $driver->kafil_name }}">

                </div> 
                <div class="form-group">
                  <label >{{ __('main.kafil phone') }}</label>
                  <input type="text" class="form-control"  placeholder="{{ __('main.kafil phone') }}" name="kafil_phone" value="{{ $driver->kafil_phone }}">

                </div>
                <div class="form-group">
                  <label >{{ __('main.email') }}</label>
                  <input type="text" class="form-control"  placeholder="{{ __('main.email') }}" name="email" value="{{ $driver->email }}">

                </div> 
                <div class="form-group">
                  <label >{{ __('main.license_no') }}</label>
                  <input type="text" class="form-control"  placeholder="{{ __('main.license_no') }}" name="license_no" value="{{ $driver->license_no }}">

                </div>
                <div class="form-group">
                  <label >{{ __('main.license_expire') }}</label>
                  <input type="date" class="form-control"  placeholder="{{ __('main.license_expire') }}" name="license_expire" value="{{ $driver->license_expire }}">

                </div>
                <div class="form-group">
                  <label >{{ __('main.address') }}</label>
                  <input type="text" class="form-control"  placeholder="{{ __('main.address') }}" name="address" value="{{ $driver->address }}">

                </div>
                <div class="form-group">
                  <label >{{ __('main.note') }}</label>
                  <input type="text" class="form-control"  placeholder="{{ __('main.note') }}" name="note" value="{{ $driver->note }}">

                </div>
               
              </div>
              <!-- /.box-body -->

              <div class="card-footer">
                <button type="submit" class="btn btn-success"><i class="fa fa-fw fa-save"></i>{{ __('main.save') }}</button>
              </div>
            </form>
          </div>
@stop