@extends('adminlte::page')
@section('head_scripts')
    <!-- DataTables CSS -->
    {{-- {{ HTML::style('css/plugins/dataTables.bootstrap.css') }} --}}
@stop
@section('content')
<h3>{{ __('main.drivers') }}</h3>
<div class="card card-light">
    <div class="card-header bg-info">
       @can('add_driver') <a  href="{{ route('drivers.create') }}"><button type="button" class="btn btn-light"><i class="fa fa-fw fa-plus"></i>{{ __('main.new') }}</button></a>@endcan
    </div>
    <div class="card-body">
        <div class="table-responsive">
<table id="example" class="table table-hover" >
        <thead class="table-secondary">
            <tr>
                <th>#</th>
                <th>{{ __('main.name') }}</th>
                <th>{{ __('main.phone') }}</th>
                {{-- <th>{{ __('main.email') }}</th> --}}
                <th>{{ __('main.address') }}</th>
                <th>{{ __('main.license_no') }}</th>
                <th>{{ __('main.license_expire') }}</th>
                <th>{{ __('main.note') }}</th>
               @can('edit_driver')<th>{{ __('main.edit') }}</th>@endcan
                @can('delete_driver')<th>{{ __('main.delete') }}</th>@endcan
            </tr>
        </thead>
        <tbody>
            @php
use Carbon\Carbon;
$today_date = Carbon::today();
@endphp
        	@foreach($drivers as $d)
            <tr>
                <td>{{ $loop->iteration }}</td>
                <td>{{ $d->name }}</td>
                <td>{{ $d->phone }}</td>
                {{-- <td>{{ $d->email }}</td> --}}
                <td>{{ $d->address }}</td>
                <td>{{ $d->license_no }}</td>
                <td>
                    @if( $d->license_expire && $today_date >= $d->license_expire)
                    <div style="width:100%; color:red; border:1px solid red; text-align: center;font-weight: bold;">
                        {{ $d->license_expire }}
                    </div>
                    @else
                    {{ $d->license_expire }}
                    @endif
                </td>
                <td>{{ $d->note }}</td>
               @can('edit_driver') <td>
                   <form method="GET" action="{{ route('drivers.edit',$d->id) }}">
                                    {{ csrf_field() }}
  <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                    <div class="form-group">
                                        <input type="submit" class="btn btn-info btn-sm " value="{{ __('main.edit') }}" >

                                    </div>
                                </form></td>@endcan
              @can('delete_driver')  <td>
                                <form action="{{ route('drivers.destroy',$d->id) }}" method="post">
                         {{ csrf_field() }}
                           <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                <input type="hidden" name="_method" value="DELETE"  >

                    <a style="padding: 0px;
                      font-size: 12px;
                      color: red;" role="menuitem" tabindex="-1"  onclick="return confirm('{{ __('main.are you sure') }}')">
                      <button type="submit" class="btn btn-sm btn-danger"><i class="fa fa-fw fa-trash"></i></button>
                    </a>
                  </form>
                  </td>@endcan
            </tr>
             @endforeach
        </tbody>
    </table>
        </div>
    </div>
</div>

@stop
