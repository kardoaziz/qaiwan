@extends('adminlte::page')
@section('head_scripts')
    <!-- DataTables CSS -->
    {{-- {{ HTML::style('css/plugins/dataTables.bootstrap.css') }} --}}
@stop
@section('content')
<h3>{{ __('main.trucks') }}</h3>
    <div class="card card-light">
        <div class="card-header bg-info">
           @can('add_truck') <a  href="{{ route('trucks.create') }}"><button type="button" class="btn btn-light"><i class="fa fa-fw fa-plus"></i>{{ __('main.new') }}</button></a>@endcan 
            @can('alert_registeration_permit') <a  href="{{ route('trucks.expired_registeration',1) }}"><button type="button" class="btn btn-light"><i class="fa fa-fw fa-calendar"></i>{{ __('main.expiring registeration') }}</button></a>@endcan
            @can('alert_registeration_permit') <a  href="{{ route('trucks.expired_permit',1) }}"><button type="button" class="btn btn-light"><i class="fa fa-fw fa-calendar"></i>{{ __('main.expiring permit') }}</button></a>@endcan
        </div>
        <div class="card-body">
            <div class="table-responsive">
    <table id="example" class="table table-hover" >
    <thead class="table-secondary">
        <tr>
            <th>#</th>
            <th>{{ __('main.code') }}</th>
            <th>{{ __('main.year')}}</th>
            <th>{{ __('main.color')}}</th>
            <!-- <th>{{ __('main.vin')}}</th> -->
            <th>{{ __('main.plate')}}</th>
            <th>{{ __('main.capacity')}}</th>
            <th>{{ __('main.registeration_expire')}}</th>
            <th>{{ __('main.permit_expire')}}</th>
            <th>{{ __('main.description')}}</th>
            @can('edit_truck')<th>{{ __('main.edit')}}</th>@endcan
            @can('delete_truck')<th>{{ __('main.delete')}}</th>@endcan
        </tr>
    </thead>
    <tbody>
           @php
use Carbon\Carbon;
$today_date = Carbon::now()->addDays(45);
$today = Carbon::now();
if(isset($expired_registeration))
{
  $services = App\Trucks::where('registeration_expire','<=',$today_date)->get();
}else if(isset($expired_permit))
{
  $services = App\Trucks::where('permit_expire','<=',$today_date)->get(); 
}
else{
  $services = App\Trucks::all();
}
@endphp
        @foreach($services as $d)
        <tr>
            <td>{{ $loop->iteration }}</td>
            <td>{{ $d->code }}</td>
            <td>{{ $d->year }}</td>
            <td><div style="background-color:{{ $d->color }}; border:1px solid grey; height:15px; "></div></td>
            <!-- <td>{{ $d->vin }}</td> -->
            <td>{{ $d->plate }}</td>
            <td>{{ $d->capacity }}</td>
            <td>
                @if( !$d->registeration_expire || $d->registeration_expire <=$today_date  )
                <div style="width:100%; color:red; border:1px solid red; text-align: center;font-weight: bold;">
                {{ $d->registeration_expire }}
                 </div>@else
                 {{ $d->registeration_expire }}
                 @endif
            </td>
            <td>
                @if($d->permit_expire && $d->permit_expire <= $today_date )<div style="width:100%; color:red; border:1px solid red; text-align: center;font-weight: bold;">
                    {{ $d->permit_expire }}
                </div>@else
                 {{ $d->permit_expire }}
                @endif
            </td>
            <td>{{ $d->description }}</td>
           @can('edit_truck') <td>
               <form method="GET" action="{{ route('trucks.edit',$d->id) }}">
                                {{ csrf_field() }}
  <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                <div class="form-group">
                                    <input type="submit" class="btn btn-info btn-sm " value="{{ __('main.edit') }}" >

                                </div>
                            </form></td>@endcan
           @can('delete_truck') <td>
                            <form action="{{ route('trucks.destroy',$d->id) }}" method="post">
                     {{ csrf_field() }}
                            <input type="hidden" name="_method" value="DELETE"  >
  <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                <a style="padding: 0px;
                  font-size: 12px;
                  color: red;" role="menuitem" tabindex="-1"  onclick="return confirm('{{ __('main.are you sure') }}')">
                  <button type="submit" class="btn btn-sm btn-danger"><i class="fa fa-fw fa-trash"></i></button>
                </a>
              </form>
              </td>@endcan
        </tr>
         @endforeach
    </tbody>
</table>
            </div>

        </div>
    </div>
    <script type="text/javascript">
      function successmg(text) {

         // $('.swalDefaultSuccess').click(function() {
      Toast.fire({
        type: 'success',
        title: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr.'
      })
    // });
      }
      
    </script>
@stop
