@extends('adminlte::page')
@section('head_scripts')
    <!-- DataTables CSS -->
    {{-- {{ HTML::style('css/plugins/dataTables.bootstrap.css') }} --}}
@stop
@section('content')
<style type="text/css">
	td{
		text-align: center;
	}
	th {
		text-align: center !important;
	}
</style>

<div class="card card-primary">
            <div class="card-header with-border text-center">
              {{ __('main.update truck') }}
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form role="form" action="{{ route('trucks.update',$truck->id) }}" method="POST">
            	     {{csrf_field()}}
                     <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
          <input type="hidden" name="_method" value="PUT">
              <div class="card-body">
                <div class="form-group">
                  <label >{{ __('main.code') }}</label>
                  <input type="text" class="form-control"  placeholder="{{ __('main.code') }}" name="code" value="{{ $truck->code }}" required>
                </div>
                <div class="form-group">
                  <label >{{ __('main.year') }}</label>
                  <input type="text" class="form-control"  placeholder="{{ __('main.year') }}" name="year" value="{{ $truck->year }}" required>
                </div>
                <div class="form-group">
                  <label >{{ __('main.color') }}</label>
                  <input type="color" class="form-control"  placeholder="{{ __('main.color') }}" name="color" value="{{ $truck->color }}">

                </div> 
                <div class="form-group">
                  <label >{{ __('main.capacity') }}</label>
                  <input type="number" class="form-control"  placeholder="{{ __('main.capacity') }}" name="capacity" value="{{ $truck->capacity }}">

                </div> 
                <div class="form-group">
                  <label >{{ __('main.vin') }}</label>
                  <input type="text" class="form-control"  placeholder="{{ __('main.vin') }}" name="vin" value="{{ $truck->vin }}">

                </div>
                <div class="form-group">
                  <label >{{ __('main.registeration_no') }}</label>
                  <input type="text" class="form-control"  placeholder="{{ __('main.registeration_no') }}" name="registeration_no" value="{{ $truck->registeration_no }}">

                </div>
                <div class="form-group">
                  <label >{{ __('main.registeration_expire') }}</label>
                  <input type="date" class="form-control"  placeholder="{{ __('main.registeration_expire') }}" name="registeration_expire" value="{{ $truck->registeration_expire }}">

                </div>
                <div class="form-group">
                  <label >{{ __('main.permit_no') }}</label>
                  <input type="text" class="form-control"  placeholder="{{ __('main.permit_no') }}" name="permit_no" value="{{ $truck->permit_no }}">

                </div>
                <div class="form-group">
                  <label >{{ __('main.permit_expire') }}</label>
                  <input type="date" class="form-control"  placeholder="{{ __('main.permit_expire') }}" name="permit_expire" value="{{ $truck->permit_expire }}">

                </div>
                <div class="form-group">
                  <label >{{ __('main.plate') }}</label>
                  <input type="text" class="form-control"  placeholder="{{ __('main.plate') }}" name="plate" value="{{ $truck->plate }}">

                </div>
                <div class="form-group">
                  <label >{{ __('main.description') }}</label>
                  <input type="text" class="form-control"  placeholder="{{ __('main.description') }}" name="description" value="{{ $truck->description }}">

                </div>
               
              </div>
              <!-- /.box-body -->

              <div class="card-footer">
                <button type="submit" class="btn btn-success"><i class="fa fa-fw fa-save"></i>{{ __('main.save') }}</button>
              </div>
            </form>
          </div>
@stop