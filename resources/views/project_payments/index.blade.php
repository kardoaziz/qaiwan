@extends('adminlte::page')
@section('head_scripts')
    <!-- DataTables CSS -->
    {{-- {{ HTML::style('css/plugins/dataTables.bootstrap.css') }} --}}
@stop
@section('content')
<h3>{{ __('main.projects payments') }}</h3>
<div class="card card-light">
    <div class="card-header bg-dark">
       @can('add_project_payment') <a  href="{{ route('projectPayment.create') }}"><button type="button" class="btn btn-light"><i class="fa fa-fw fa-plus"></i>{{ __('main.new') }}</button></a>@endcan
    </div>
    <div class="card-body">
        <div class="table-responsive">
        <table id="example" class="table table-hover" >
                <thead class="table-light">
                    <tr>
                        <th>#</th>
                        <th>{{ __('main.name') }}</th>
                        <th>{{ __('main.invoice') }}</th>
                        <th>{{ __('main.amount') }}</th>
                        <th>{{ __('main.date') }}</th>
                        <th>{{ __('main.note') }}</th>
                       @can('edit_project_payment') <th>{{ __('main.edit') }}</th>@endcan
                       @can('view_project_payment') <th>{{ __('main.print') }}</th>@endcan
                       @can('delete_project_payment') <th>{{ __('main.delete') }}</th>@endcan
                    </tr>
                </thead>
                <tbody>
                    @foreach($payments as $d)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $d->project->name }}</td>
                        <td>{{ $d->invoice_no }}</td>
                        <td>{{ number_format($d->amount,2) }}</td>
                        <td>{{ $d->date }}</td>
                        <td>{{ $d->note }}</td>
                       @can('edit_project_payment') <td>
                            <form method="GET" action="{{ route('projectPayment.edit',$d->id) }}">
                                             {{ csrf_field() }}
  <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                             <div class="form-group">
                                                 <input type="submit" class="btn btn-info btn-sm " value="{{ __('main.edit') }}" >

                                             </div>
                                         </form></td>@endcan
                         @can('view_project_payment')  <td>
                                            <form method="GET" action="{{ route('projectPayment.show',$d->id) }}">
                                                             {{ csrf_field() }}
  <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                                             <div class="form-group">
                                                                 <input type="submit" class="btn btn-info btn-sm " value="{{ __('main.print') }}" >

                                                             </div>
                                                         </form></td>@endcan
                    @can('delete_project_payment')    <td>
                                        <form action="{{ route('projectPayment.destroy',$d->id) }}" method="post">
                                 {{ csrf_field() }}
                                   <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                        <input type="hidden" name="_method" value="DELETE"  >

                            <a style="padding: 0px;
                              font-size: 12px;
                              color: red;" role="menuitem" tabindex="-1"  onclick="return confirm('{{ __('main.are you sure') }}')">
                              <button type="submit" class="btn btn-sm btn-danger"><i class="fa fa-fw fa-trash"></i></button>
                            </a>
                          </form>
                          </td>@endcan
                    </tr>
                     @endforeach
                </tbody>
            </table>
        </div>
        </div>
</div>
@stop
