@extends('adminlte::page')
@section('head_scripts')
    <!-- DataTables CSS -->
    {{-- {{ HTML::style('css/plugins/dataTables.bootstrap.css') }} --}}
@stop


@section('content')

    <div class="container bg-light">
        <div class="row">
            <div class="col-md-4 pull-right"><img src="{{asset('img/logo_small.png')}}" style="width: 100px; height:100px"></div>
            <div class="col-md-4"></div>
            <div class="col-md-4 pull-left">
                <tr>
                    <td><h5 class="text-left">{{ __('main.code')}} : {{$payment->id}}</h5></td>
                </tr>
                <tr>
                    <td><h5 class="text-left">{{ __('main.date')}} : {{$payment->date}}</h5></td>
                </tr>
            </div>
        </div>
        <hr>
        @php
        $user=Auth::user();
        @endphp

        <div class="container">
            <table class="table table-borderless" >
                <tr>
                    <td><h5 class="text-left">{{ __('main.project name')}} : {{$payment->project->name}}</h5></td>
                </tr>
                <tr>
                    <td><h5 class="text-left">{{ __('main.invoice')}} : {{$payment->invoice_no}}</h5></td>
                </tr>
                <tr>
                    <td><h5 class="text-left">{{ __('main.amount')}} : {{$payment->amount}}</h5></td>
                </tr>
                <tr>
                    <td><h5 class="text-left">{{ __('main.note')}} : {{$payment->note}}</h5></td>
                </tr>
            </table>
        </div>
        <br>
        <br>
        <hr>
        <br>
        <br>
        <div class="container">
            <table class="table table-borderless" >
                <tr>
                    <td><p class="text-left">{{ __('main.sign')}} <br> {{ __('main.name')}} : {{$payment->project->name}}
                        <br> {{ __('main.phone')}} : {{$payment->project->phone}}</p> </td>
                    <td><p class="text-right">{{ __('main.sign')}} <br>{{ __('main.user')}} : {{$user->name}}</p></td>
                </tr>

            </table>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-4 pull-right"><img src="{{asset('img/logo_small.png')}}" style="width: 100px; height:100px"></div>
            <div class="col-md-4"></div>
            <div class="col-md-4 pull-left">
                <tr>
                    <td><h5 class="text-left">{{ __('main.code')}} : {{$payment->id}}</h5></td>
                </tr>
                <tr>
                    <td><h5 class="text-left">{{ __('main.date')}} : {{$payment->date}}</h5></td>
                </tr>
            </div>
        </div>
        <hr>
        <div class="container">
            <table class="table table-borderless" >
                <tr>
                    <td><h5 class="text-left">{{ __('main.project name')}} : {{$payment->project->name}}</h5></td>
                </tr>
                <tr>
                    <td><h5 class="text-left">{{ __('main.invoice')}} : {{$payment->invoice_no}}</h5></td>
                </tr>
                <tr>
                    <td><h5 class="text-left">{{ __('main.amount')}} : {{$payment->amount}}</h5></td>
                </tr>
                <tr>
                    <td><h5 class="text-left">{{ __('main.note')}} : {{$payment->note}}</h5></td>
                </tr>
            </table>
        </div>
        <hr>
        <div class="container">
            <table class="table table-borderless" >
                <tr>
                    <td><p class="text-left">{{ __('main.sign')}} <br> {{ __('main.name')}} : {{$payment->project->name}}
                        <br> {{ __('main.phone')}} : {{$payment->project->phone}}</p> </td>
                        <td><p class="text-right">{{ __('main.sign')}} <br>{{ __('main.user')}} : {{$user->name}}</p></td>
                </tr>

            </table>
        </div>
        <hr>
    </div>

@stop
