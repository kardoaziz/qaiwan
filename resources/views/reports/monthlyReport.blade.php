
@extends('adminlte::page')
@section('head_scripts')
    <!-- DataTables CSS -->
    {{-- {{ HTML::style('css/plugins/dataTables.bootstrap.css') }} --}}
@stop
@section('content')
<h4>{{ __('main.monthly report') }}</h4>

<section class="invoice">
        <div class="page-header">
        <center><img src="{{asset('img/logo_small.png')}}" style="width: 100px; height:100px">
            <h5>کۆمپانیای قەیوان</h5>
            <br>
            <form class="form-inline justify-content-center bg-info" style="padding: 15px;" action="{{ route('reports.monthly') }}" method="POST">
                {{csrf_field()}}
             <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
               <div class="form-group mb-2">
                   <label for="staticEmail2" >{{ __('main.from date') }}&nbsp;&nbsp;</label>
                   <input class="form-control" type="date" id="from_date" name="from_date">
                 </div>
                 <div class="form-group mx-sm-3 mb-2">
                   <label for="inputPassword2" >{{ __('main.to date') }}&nbsp;&nbsp;</label>
                   <input class="form-control" type="date" id="to_date" name="to_date">
                 </div>
                 <button type="submit" class="btn btn-light form-control mb-2"><i class="fa fa-fw fa-save"></i>{{ __('main.filter') }}</button>
           </form>
           <br>
        </center>

        </div>

        <div class="container">
            <div class="row">
                <div  class="col-md-4">
                    <div class="info-box">
                        <span class="info-box-icon bg-info"><i class="far fa fa-road"></i></span>
                        <div class="info-box-content">
                        <span class="info-box-text">{{ __('main.total trips')}}</span>
                          <span class="info-box-number">{{$trips}}</span>
                        </div>
                      </div>
                </div>
                <div  class="col-md-4">
                    <div class="info-box">
                        <span class="info-box-icon bg-info"><i class="far fa fa-road"></i></span>
                        <div class="info-box-content">
                        <span class="info-box-text">{{ __('main.total trips cost')}}</span>
                          <span class="info-box-number">{{$trip_cost}}</span>
                        </div>
                      </div>
                </div>
                <div  class="col-md-4">
                    <div class="info-box">
                        <span class="info-box-icon bg-info"><i class="far fa fa-road"></i></span>
                        <div class="info-box-content">
                        <span class="info-box-text">{{ __('main.total trips gass')}}</span>
                          <span class="info-box-number">{{$tripGass}}</span>
                        </div>
                      </div>
                </div>
                <div  class="col-md-4">
                    <div class="info-box">
                        <span class="info-box-icon bg-info"><i class="far fa fa-road"></i></span>
                        <div class="info-box-content">
                        <span class="info-box-text">{{ __('main.total trips expenses')}}</span>
                          <span class="info-box-number">{{$tripExpense}}</span>
                        </div>
                      </div>
                </div>
                <div  class="col-md-4">
                    <div class="info-box">
                        <span class="info-box-icon bg-info"><i class="far fa fa-road"></i></span>
                        <div class="info-box-content">
                        <span class="info-box-text">{{ __('main.total trips fine')}}</span>
                          <span class="info-box-number">{{$tripFine}}</span>
                        </div>
                      </div>
                </div>
            </div>
        </div>

</section>

@stop
