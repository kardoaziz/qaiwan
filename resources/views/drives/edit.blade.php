@extends('adminlte::page')
@section('head_scripts')
    <!-- DataTables CSS -->
    {{-- {{ HTML::style('css/plugins/dataTables.bootstrap.css') }} --}}
@stop
@section('content')
<style type="text/css">
	td{
		text-align: center;
	}
	th {
		text-align: center !important;
	}
</style>

<div class="card card-primary">
            <div class="card-header with-border text-center">
              {{ __('main.update driver') }}
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form role="form" action="{{ route('drives.update',$drive->id) }}" method="POST">
            	     {{csrf_field()}}
                     <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
          <input type="hidden" name="_method" value="PUT">
              <div class="card-body">
                <div class="form-group">
                  <label >{{ __('main.driver') }}</label>
                  <input list="driverlist" name="driver_id" id="drivers" class="form-control" onchange="hideList(this)" value="{{ $drive->driver_id }}" required>
                <datalist id="driverlist">
                  @foreach(App\Driver::all() as $c)
                  <option value="{{ $c->id }}">{{ $c->name }} - {{ $c->phone }} </option>
                  @endforeach
                </datalist>
                </div>
                <div class="form-group">
                  <label >{{ __('main.truck') }}</label>
                 <input list="trucklist" name="truck_id" id="trucks" class="form-control" onchange="hideList(this)" value="{{ $drive->truck_id }}" required>
                <datalist id="trucklist">
                  @foreach(App\Trucks::all() as $c)
                  <option value="{{ $c->id }}">{{ $c->code }} - {{ $c->plate }} </option>
                  @endforeach
                </datalist>
                </div>
                <div class="form-group">
                  <label >{{ __('main.date') }}</label>
                  <input type="date" class="form-control"  placeholder="{{ __('main.date') }}" name="date" value="{{ $drive->date }}" required>

                </div> 
                <div class="form-group">
                  <label >{{ __('main.old balance') }}</label>
                  <input type="number" step="0.01" class="form-control"  placeholder="{{ __('main.old balance') }}" name="old_balance" value="{{ $drive->old_balance }}">

                </div> 
                <div class="form-group">
                  <label >{{ __('main.note') }}</label>
                  <input type="text" class="form-control"  placeholder="{{ __('main.note') }}" name="note" value="{{ $drive->note }}">

                </div>
                <div class="form-group">
                  <label >{{ __('main.status') }}</label>
                 <select name="status">
                    <option value="active" @if($drive->status=="active") selected @endif>{{ __('main.active') }}</option>
                    <option value="suspended" @if($drive->status=="suspended") selected @endif>{{ __('main.suspended') }}</option>
                    <option value="replaced" @if($drive->status=="replaced") selected @endif>{{ __('main.replaced') }}</option>
                 </select>

                </div>
               
              </div>
              <!-- /.box-body -->

              <div class="card-footer">
                <button type="submit" class="btn btn-success"><i class="fa fa-fw fa-save"></i>{{ __('main.save') }}</button>
              </div>
            </form>
          </div>
          <script type="text/javascript">
            function hideList(input) {
    console.log("hide");
  var datalist = document.querySelector("datalist");
  var x = document.getElementById("data");
  if (datalist.value!="") {
    x.style.display="none";   
  } else {
    x.style.display="block";
  }
}
          </script>
@stop