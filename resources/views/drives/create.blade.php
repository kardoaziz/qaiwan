@extends('adminlte::page')
@section('head_scripts')
    <!-- DataTables CSS -->
    {{-- {{ HTML::style('css/plugins/dataTables.bootstrap.css') }} --}}
@stop
@section('content')
<style type="text/css">
	td{
		text-align: center;
	}
	th {
		text-align: center !important;
	}
</style>

<div class="card card-primary">
            <div class="card-header with-border text-center">
              {{ __('main.new truck to driver') }}
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form role="form" action="{{ route('drives.store') }}" method="POST">
            	    {{csrf_field()}}
          <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
              <div class="card-body">
                <div class="form-group">
                  <label >{{ __('main.driver') }}</label>
                  <input type="hidden" name="driver_id" id="driver_id">
                  <input list="driverlist" name="drivers" id="drivers" class="form-control" onchange="getDriverName(this.value)" required>
                <datalist id="driverlist">
                  @foreach(App\Driver::all() as $c)
                  <option value="{{ $c->id }}-{{ $c->name }} - {{ $c->phone }}">{{ $c->name }} - {{ $c->phone }} </option>
                  @endforeach
                </datalist>
                </div>
                <div class="form-group">
                  <label >{{ __('main.truck') }}</label>
                  <input type="hidden" name="truck_id" id="truck_id">
                 <input list="trucklist" name="trucks" id="trucks" class="form-control" onchange="getTruckName(this.value)" required>
                <datalist id="trucklist">
                  @foreach(App\Trucks::all() as $c)
                  <option value="{{ $c->id }}-{{ $c->code }} - {{ $c->plate }}">{{ $c->code }} - {{ $c->plate }} </option>
                  @endforeach
                </datalist>
                </div>
                <div class="form-group">
                  <label >{{ __('main.date') }}</label>
                  <input type="date" class="form-control"  placeholder="{{ __('main.date') }}" name="date" required>

                </div>
                 <div class="form-group">
                  <label >{{ __('main.old balance') }}</label>
                  <input type="number" step="0.01" class="form-control"  placeholder="{{ __('main.old balance') }}" name="old_balance">

                </div> 
               
                <div class="form-group">
                  <label >{{ __('main.note') }}</label>
                  <input type="text" class="form-control"  placeholder="{{ __('main.note') }}" name="note">

                </div>
               
              </div>
              <!-- /.box-body -->

              <div class="card-footer">
                <button type="submit" class="btn btn-success"><i class="fa fa-fw fa-save"></i>{{ __('main.save') }}</button>
              </div>
            </form>
          </div>
          <script type="text/javascript">
             function getDriverName(id) {
              console.log(id);
              $('#driver_id').val(id.split("-")[0]);
              // $('#driver_name').val(id.split("-")[1]);
              // body...
            }
            function getTruckName(id) {
              console.log(id);
              $('#truck_id').val(id.split("-")[0]);
             // $('#truck_name').val(id.split("-")[1]);
              // body...
            }
            function hideList(input) {
    console.log("hide");
  var datalist = document.querySelector("datalist");
  var x = document.getElementById("data");
  if (datalist.value!="") {
    x.style.display="none";   
  } else {
    x.style.display="block";
  }
}
          </script>
@stop