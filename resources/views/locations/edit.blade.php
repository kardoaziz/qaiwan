@extends('adminlte::page')
@section('head_scripts')
    <!-- DataTables CSS -->
    {{-- {{ HTML::style('css/plugins/dataTables.bootstrap.css') }} --}}
@stop
@section('content')
<style type="text/css">
	td{
		text-align: center;
	}
	th {
		text-align: center !important;
	}
</style>

<div class="card card-primary">
            <div class="card-header with-border text-center">
              {{ __('main.new item') }}
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form role="form" action="{{ route('locations.update',$location->id) }}" method="POST">
                    {{csrf_field()}}
                    <input type="hidden" name="_method" value="PUT">
          <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
              <div class="card-body">
                <div class="form-group">
                  <label >{{ __('main.name') }}</label>
                <input type="text" class="form-control"  placeholder="{{ __('main.name') }}" name="name" value="{{$location->name}}" required>
                </div>


              </div>
              <!-- /.box-body -->

              <div class="card-footer">
                <button type="submit" class="btn btn-success"><i class="fa fa-fw fa-save"></i>{{ __('main.save') }}</button>
              </div>
            </form>
          </div>
@stop
