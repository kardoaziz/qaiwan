@extends('adminlte::page')
@section('head_scripts')
    <!-- DataTables CSS -->
    {{-- {{ HTML::style('css/plugins/dataTables.bootstrap.css') }} --}}
@stop
@section('content')
<style type="text/css">
  td{
    text-align: center;
  }
  th {
    text-align: center !important;
  }
</style>

<div class="card card-primary">
            <div class="card-header with-border text-center">
              {{ __('main.update payment') }}
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form role="form" action="{{ route('banzin_purchase.update',$payment->id) }}" method="POST">
            	    {{csrf_field()}}
                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
<input type="hidden" name="_method" value="PUT">
              <div class="card-body">

                 <div class="form-group">
                  <label >{{ __('main.date') }}</label>
                  <input type="date" class="form-control"  placeholder="{{ __('main.date') }}" name="date" value="{{ $payment->date }}">

                </div>
              {{-- </div> --}}
                 <div class="form-group">
                  <label >{{ __('main.amount') }}</label>
                  <input type="number" step="0.01" min="0" class="form-control"  placeholder="{{ __('main.qty') }}" name="amount" value="{{ $payment->qty }}">

                </div>
                 <div class="form-group">
                  <label >{{ __('main.price') }}</label>
                  <input type="number" step="0.01" min="0" class="form-control"  placeholder="{{ __('main.price') }}" name="price" value="{{ $payment->price }}">

                </div>
                 <div class="form-group">
                  <label >{{ __('main.note') }}</label>
                  <input type="text" class="form-control"  placeholder="{{ __('main.note') }}" name="note" value="{{ $payment->note }}">

                </div>

              </div>
              <!-- /.box-body -->

              <div class="card-footer">
                <button type="submit" class="btn btn-success"><i class="fa fa-fw fa-save"></i>{{ __('main.save') }}</button>
              </div>
            </form>
          </div>
          <script type="text/javascript">
             function selectdriver(id) {

$('#drive_id').val(id.split("-")[0]);
$('#driver').val(id.split("-")[1]);
}
          </script>
          @stop
