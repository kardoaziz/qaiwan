@extends('adminlte::page')
@section('head_scripts')
    <!-- DataTables CSS -->
    {{-- {{ HTML::style('css/plugins/dataTables.bootstrap.css') }} --}}
@stop
@section('content')
<h3>{{ __('main.warehouses') }}</h3>
    <div class="card card-light">
        <div class="card-header bg-light">
             @can('add_warehouse')<a  href="{{ route('warehouses.create') }}"><button type="button" class="btn btn-dark"><i class="fa fa-fw fa-plus"></i>{{ __('main.new') }}</button></a>@endcan
        </div>
        <div class="card-body">
            <div class="table-responsive">
    <table   table id="example" class="table table-hover" >
        <thead class="thead-dark">
            <tr>
                <th>#</th>
                <th>{{ __('main.name') }}</th>

                <th>{{ __('main.stock') }}</th>
                <th>{{ __('main.stock cost') }}</th>
                <th>{{ __('main.description') }}</th>

               @can('edit_warehouse')<th>{{ __('main.edit') }}</th>@endcan
                @can('delete_warehouse')<th>{{ __('main.delete') }}</th>@endcan
            </tr>
        </thead>
        <tbody>
        	@foreach(App\Warehouse::all() as $d)
            <tr>
                <td>{{ $loop->iteration }}</td>
                <td>{{ $d->name }}</td>
                <td>0</td>
                <td>0</td>
                {{-- <td>{{ $d->code }}</td> --}}
                {{-- <td>{{ $d->location }}</td> --}}
                <td>{{ $d->description }}</td>
                {{-- <td>{{ $d->note }}</td> --}}
                @can('edit_warehouse')<td>
                   <form method="GET" action="{{ route('warehouses.edit',$d->id) }}">
                                    {{ csrf_field() }}
  <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                    <div class="form-group">
                                        <input type="submit" class="btn btn-info btn-sm " value="{{ __('main.edit') }}" >

                                    </div>
                                </form></td>@endcan
                @can('delete_warehouse')<td>
                                <form action="{{ route('warehouses.destroy',$d->id) }}" method="post">
                         {{ csrf_field() }}
                           <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                <input type="hidden" name="_method" value="DELETE"  >

                    <a style="padding: 0px;
                      font-size: 12px;
                      color: red;" role="menuitem" tabindex="-1"  onclick="return confirm('{{ __('main.are you sure') }}')">
                      <button type="submit" class="btn btn-sm btn-danger"><i class="fa fa-fw fa-trash"></i></button>
                    </a>
                  </form>
                  </td>@endcan
            </tr>
             @endforeach
        </tbody>
    </table>
            </div>

        </div>
    </div>
@stop
