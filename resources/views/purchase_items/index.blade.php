@extends('adminlte::page')
@section('head_scripts')
    <!-- DataTables CSS -->
    {{-- {{ HTML::style('css/plugins/dataTables.bootstrap.css') }} --}}
@stop
@section('content')
<h3>{{ __('main.purchase items') }}</h3>
{{-- <a  href="{{ route('items.create') }}"><button type="button" class="btn btn-success"><i class="fa fa-fw fa-plus"></i>{{ __('main.new') }}</button></a> --}}
<div class="card card-success">
    <div class="card-body">
        <div class="row">
            <div class="col-md-2">{{ __('main.supplier') }}</div>
            <div class="col-md-4"><h3>{{ $purchase->supplier->name }}</h3></div>
          <div class="col-md-2">{{ __('main.warehouse') }}</div>
            <div class="col-md-4"><h3>{{ $purchase->warehouse->name }}</h3></div>
        </div>

        <div class="row">
          <div class="col-md-2">{{ __('main.date') }}</div>
            <div class="col-md-4">{{ $purchase->date }}</div>
            <div class="col-md-2">{{ __('main.discount') }}</div>
            <div class="col-md-4">{{ $purchase->discount }}</div>
        </div>
        <div class="row">
            <div class="col-md-2">{{ __('main.note') }}</div>
            <div class="col-md-4">{{ $purchase->note }}</div>
        </div>
        
    </div>
</div>
@php
$dollar_price = App\DollarPrice::orderby('id','desc')->first()->price;
echo $dollar_price;
@endphp
<div class="card card-success responsive">
    <div class="card-body" style="padding: 0px;">
<table  class="display stripe table table-bordered table-hover" style="width:100%">
        <thead>
            <tr>
                <th>#</th>
                <th>{{ __('main.item') }}</th>
                <th>{{ __('main.code') }}</th>
                <th>{{ __('main.qty') }}</th>
                <th>{{ __('main.price') }}</th>
                <th>{{ __('main.price $') }}</th>
                <th>{{ __('main.note') }}</th>
                {{-- <th>{{ __('main.edit') }}</th> --}}
                <th>{{ __('main.delete') }}</th>
            </tr>
        </thead>
        <tbody>
        	@foreach(App\PurchaseItem::where('purchase_id',$purchase->id)->get() as $key => $d)
            <tr>
                <td>{{ $loop->iteration }}</td>
                <td>{{ $d->item->name }}</td>
                <td>{{ $d->item->code }}</td>
                <td><input type="number" id="qty{{ $key }}" step="0.01" value="{{ $d->qty }}" style="width:100%; margin:0px;text-align: center;" onblur="updateitem('qty',{{ $d->id }},this.value,{{ $key }},{{ $dollar_price }})"></td>
                <td><input type="number" id="price{{ $key }}"  step="0.01" value="{{ $d->price }}" style="width:100%; margin:0px;text-align: center;" onblur="updateitem('price',{{ $d->id }},this.value,{{ $key }},{{ $dollar_price }})"></td>
                 <td><input type="number" id="dollar{{ $key }}"  step="0.01" value="{{ $d->dollar }}" style="width:100%; margin:0px;text-align: center;" onblur="updateitem('dollar',{{ $d->id }},this.value,{{ $key }},{{ $dollar_price }})"></td>
                <td><input type="text" step="0.01" id="note{{ $key }}"  value="{{ $d->note }}" style="width:100%; margin:0px;text-align: center;" onblur="updateitem('note',{{ $d->id }},this.value,{{ $key }},{{ $dollar_price }})"></td>
                
                <td>
                                <form action="{{ route('purchase_items.destroy',$d->id) }}" method="post">
                         {{ csrf_field() }}
                           <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                <input type="hidden" name="_method" value="DELETE"  >

                    <a style="padding: 0px;
                      font-size: 12px;
                      color: red;" role="menuitem" tabindex="-1"  onclick="return confirm('{{ __('main.are you sure') }}')">
                      <button type="submit" class="btn btn-sm btn-danger"><i class="fa fa-fw fa-trash"></i></button>
                    </a>
                  </form>
                  </td>
            </tr>
             @endforeach
             
        </tbody>
        <tfoot>
            <tr><td colspan="3">{{ __('main.total_item') }}</td>
                <td><input style="width:100%;text-align: center;" type="number"   id="total_qty" value="{{ App\PurchaseItem::where('purchase_id',$purchase->id)->sum('qty') }}"   disabled="true" ></td>
                <td >{{ __('main.invoice_total') }}</td>
                <td ><input style="width:100%;text-align: center;" type="number"   id="total" value="{{ number_format( App\PurchaseItem::where('purchase_id',$purchase->id)->select(DB::raw('sum(qty*price) as total'))->first()->total,2) }}"   disabled="true" ></td>
                <td colspan="2"></td>
                </tr>
        </tfoot>
    </table>
    {{-- <hr> --}}
    {{-- <form role="form" action="{{ route('purchase_items.store') }}" method="POST">
                    {{csrf_field()}}
          <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
          <input type="hidden" name="purchase_id" value="{{ $purchase->id }}"> --}}
    {{-- <div class="row text-center">
        <div class="col-md-2"><h4>{{ __('main.new') }}</h4></div>
        <div class="col-md-2"> <input list="itemslist" name="item_id" id="item_id" class="form-control" onchange="hideList(this)" placeholder="{{ __('main.item') }}">
                <datalist id="itemslist">
                  @foreach(App\Items::all() as $c)
                  <option value="{{ $c->id }}">{{ $c->name }}-{{ $c->code }} </option>
                  @endforeach
                </datalist></div>
                <div class="col-md-2"><input type="number" step="0.01" name="qty" placeholder="{{ __('main.qty') }}" id="qty"></div>
                <div class="col-md-2"><input type="number" step="0.01" name="price" placeholder="{{ __('main.price') }}" id="price"></div>
                <div class="col-md-2"><input type="text"  name="note" placeholder="{{ __('main.note') }}" id="note"></div>
                <div class="col-md-2"><button onclick="additem({{ $purchase->id }})" type="submit" class="btn btn-success"><i class="fa fa-fw fa-save" ></i>{{ __('main.add') }}</button></div>
    </div> --}}
{{-- </form> --}}
     </div>
 </div>
 <div class="card card-light">
    <div class="card-body">
        <div class="table-responsive">
        <table id="example1" class="table table-hover" >
                <thead class="thead-dark">
                    <tr>
                        <th>#</th>
                        <th>{{ __('main.add') }}</th>
                        <th>{{ __('main.name') }}</th>
                        <th>{{ __('main.code') }}</th>
                        <th>{{ __('main.location') }}</th>
                        <th>{{ __('main.description') }}</th>
                        <th>{{ __('main.note') }}</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach(App\Items::all() as $d)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td><button type="button" class="btn btn-block btn-success" onclick="addItem({{ $purchase->id }},{{ $d->id }})"> <i class="fa fa-plus"></i> {{ __("main.add") }}</button></td>
                        <td>{{ $d->name }}</td>
                        <td>{{ $d->code }}</td>
                        <td>{{ $d->location }}</td>
                        <td>{{ $d->description }}</td>
                        <td>{{ $d->note }}</td>
                    </tr>
                     @endforeach
                </tbody>
            </table>
        </div>
        </div>
</div>
<script type="text/javascript">
    function additem(purchase_id)
{ 
  console.log(purchase_id);
  var item_id = document.getElementById('item_id').value;
  var qty = document.getElementById('qty').value;
  var price = document.getElementById('price').value;
  var note = document.getElementById('note').value;
  // console.log(item_id);
  // console.log(customer_id);
  $.ajax({
          type:'POST',
          headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
          url:'{{ route("purchase_items.store") }}',
          data:{ purchase_id:purchase_id, item_id:item_id, qty:qty,price:price, note:note},
           success:function(data){
            console.log(data);
            console.log(data);
            location.reload();
           }
        });
}
function addItem(purchase_id,item_id)
{ 
  console.log(purchase_id);
  // var item_id = document.getElementById('item_id').value;
  var qty =0;// document.getElementById('qty').value;
  var price =0;// document.getElementById('price').value;
  var dollar =0;// document.getElementById('price').value;
  var note = '';//document.getElementById('note').value;
  // console.log(item_id);
  // console.log(customer_id);
  $.ajax({
          type:'POST',
          headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
          url:'{{ route("purchase_items.store") }}',
          data:{ purchase_id:purchase_id, item_id:item_id, qty:qty,price:price, note:note,dollar:dollar},
           success:function(data){
            console.log("data");
            console.log(data);
            location.reload();
           }
        });
}
function updateitem(field,id,val,key,dollar)
{ 
  console.log("key");
  console.log(key);
  console.log(dollar);
  if(field=='price'){
      document.getElementById('dollar'+key).value = (val/dollar);
  }
  else if(field=='dollar')
  {
     document.getElementById('price'+key).value =(val*dollar);
  }
  $.ajax({
          type:'POST',
          headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
          url:'{{ route("purchase_items.updatee") }}',
          data:{ id:id, field:field,val:val},
           success:function(data){
            console.log(data);
            // console.log(data['total_qty']);
            // console.log(data['total']);
            document.getElementById("total_qty").value =data['total_qty'];
            document.getElementById("total").value =data['total'];
           }
        });
}
</script>
@stop
