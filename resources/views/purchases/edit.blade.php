@extends('adminlte::page')
@section('head_scripts')
    <!-- DataTables CSS -->
    {{-- {{ HTML::style('css/plugins/dataTables.bootstrap.css') }} --}}
@stop
@section('content')
<div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          {{-- <span aria-hidden="true">&times;</span> --}}
        </button>
        <h4 class="modal-title" align="center"><b> {{ __("main.update") }}  {{ __("main.purchase") }}</b></h4>
      </div>
      <div class="modal-body" style="overflow-y: scroll;">
        <form action="{{ route('purchases.update',$purchase->id) }}" method="POST" enctype="multipart/form-data">
                {{csrf_field()}}
                  <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                  
<input type="hidden" name="_method" value="PUT">          <div class="form-group">
                <label>{{ __("main.warehouse") }}</label>

                <input list="warehouselist" name="warehouse_id" id="warehouses" class="form-control" onchange="hideList(this)" value="{{ $purchase->warehouse_id }}" required>
                <datalist id="warehouselist">
                  @foreach(App\Warehouse::all() as $c)
                  <option value="{{ $c->id }}">{{ $c->name }} </option>
                  @endforeach
                </datalist>
               
              </div>
              <div class="form-group">
                <label>{{ __("main.supplier") }}</label>

                <input list="supplierlist" name="supplier_id" id="suppliers" class="form-control" onchange="hideList(this)" value="{{ $purchase->supplier_id }}">
                <datalist id="supplierlist" required>
                  @foreach(App\Suppliers::all() as $c)
                  <option value="{{ $c->id }}">{{ $c->name }} </option>
                  @endforeach
                </datalist>
               
              </div>
          
          <div class="form-group">
            <label for="exampleInputEmail1"> {{ __("main.date") }}</label> 
              <input type="date" id="date" name="date" class="form-control" placeholder="Date" required value="{{ $purchase->date }}" required>
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1"> {{ __("main.invoice_no") }}</label> 
              <input type="text" id="invoice_no" name="invoice_no" class="form-control" placeholder="{{ __('main.invoice_no') }}" value="{{ $purchase->invoice_no }}" >
          </div>
          <div class="form-group">
                      <label > {{ __("main.invoice") }}</label>
                      <input type="file" name="file">
                    </div>
          <div class="form-group">
            <label for="exampleInputEmail1"> {{ __("main.discount") }}</label> 
              <input type="number" id="discount" name="discount" class="form-control" placeholder="{{ __('main.discount') }}" required value="{{ $purchase->discount }}">
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1"> {{ __("main.note") }}</label> 
              <input type="text" id="note" name="note" class="form-control" placeholder="{{ __('main.note') }}"  value="{{ $purchase->note }}">
          </div>
          
         
          <button type="submit" class="btn btn-primary"> {{ __("main.add") }}</button>
        </form>
      </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default pull-left" data-dismiss="modal"> {{ __("main.cancel") }}</button>
            
          </div>
    </div>
    @stop