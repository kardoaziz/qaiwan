@extends('adminlte::page')
@section('head_scripts')
    <!-- DataTables CSS -->
    {{-- {{ HTML::style('css/plugins/dataTables.bootstrap.css') }} --}}
@stop
@section('content')
<style type="text/css">
	td{
		text-align: center;
	}
	th {
		text-align: center !important;
	}
</style>

@php
    $purchaseAmount=App\gassQtyPurchase::get()->sum("qty");
    $sellAmount=App\gassQtyToTrucks::get()->sum("qty");
    $totalAmount=$purchaseAmount - $sellAmount;
@endphp

<div class="card card-primary">
            <div class="card-header with-border text-center">
              {{ __('main.new gass to trucks') }}
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form role="form" action="{{ route('gass_to_trucks.store') }}" method="POST">
            	    {{csrf_field()}}
          <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
              <div class="card-body">
                <div class="form-group">
                  <label >{{ __('main.name') }}</label>
                  <input type="hidden" name="drive_id" id="drive_id">
                  <input list="driverslist" name="drives" id="drives" class="form-control" onchange="selectdriver(this.value)">
                <datalist id="driverslist">
                  @foreach (App\Drive::all() as $d)
                <option value="{{$d->id}}-{{ $d->driver->name }} - {{$d->truck->code}}">{{$d->driver->name}} - {{$d->truck->code}}</option>
                            @endforeach
                </datalist>
                </div>


                <div class="form-group">
                  <input class="form-control" type="text" name="driver" id="driver" disabled>
                </div>
                {{-- <div class="form-group">
                  <label >{{ __('main.code') }}</label>
                  <input type="text" class="form-control"  placeholder="{{ __('main.code') }}" name="code">

                </div> --}}
                 <div class="form-group">
                  <label >{{ __('main.date') }}</label>
                  <input type="date" class="form-control"  placeholder="{{ __('main.date') }}" name="date">

                </div>
              {{-- </div> --}}
                 <div class="form-group">
                  <label >{{ __('main.amount') }}</label>
                  <input type="number" step="0.01" min="0" class="form-control" onchange="valChanged()"  placeholder="{{ __('main.amount') }}" name="amount" id="amount">
                 <input type="number" step="0.01" min="0" class="form-control"  value="{{$totalAmount}}" name="total" id="total" readonly>

                </div>
                 <div class="form-group">
                  <label >{{ __('main.note') }}</label>
                  <input type="text" class="form-control"  placeholder="{{ __('main.note') }}" name="note">

                </div>

              </div>
              <!-- /.box-body -->

              <div class="card-footer">
                @if ($totalAmount<=0)
                <div class="alert alert-danger">
                    <p>{{__('main.gass not available')}}</p>
                </div>
                @else
                <button type="submit" class="btn btn-success"><i class="fa fa-fw fa-save"></i>{{ __('main.save') }}</button>
                @endif
            </div>
            </form>
          </div>

          <script type="text/javascript">
             function selectdriver(id) {

                $('#drive_id').val(id.split("-")[0]);
                $('#driver').val(id.split("-")[1]);
                }
            function valChanged(){
              
                var amount=document.getElementById('amount').value;
                var total=document.getElementById('total').value;
                console.log(amount);
              console.log(total);
                if( amount > total ){
                    alert('ببورە ئەو بڕە بەردەست نییە ');
                    document.getElementById('amount').value='0';
                }
            }
          </script>
@stop
