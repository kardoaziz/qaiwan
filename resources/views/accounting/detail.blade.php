@extends('adminlte::page')
@section('head_scripts')
    <!-- DataTables CSS -->
    {{-- {{ HTML::style('css/plugins/dataTables.bootstrap.css') }} --}}
@stop
@section('content')
<style type="text/css">
	#info tr td{
		padding: 0px !important;
	}
	#trips tr td{
		padding: 0px !important;
	}
	.btn-circle {
    width: 20px;
    height: 20px;
    padding: 3px 0px;
    border-radius: 10px;
    text-align: center;
    font-size: 12px;
    line-height: 1.42857;
}

</style>
@php
	$trips = App\Trip::where('drive_id',$drive->id)->orderby('created_at','desc')->get();
	$payments = App\DrivePayment::where('drive_id',$drive->id)->get();
	$expenses = App\TripExpense::join('trips','trips.id','trip_expenses.trip_id')->where('trips.drive_id',$drive->id)->get();
	$fines = App\TripFine::join('trips','trips.id','trip_fines.trip_id')->where('trips.drive_id',$drive->id)->get();
    $gass = App\TripGass::join('trips','trips.id','trip_gasses.trip_id')->where('trips.drive_id',$drive->id)->get();
    $gasstotal = App\TripGass::join('trips','trips.id','trip_gasses.trip_id')->where('trips.drive_id',$drive->id)->select(DB::raw('(trip_gasses.qty*trip_gasses.price) as gass_total'))->first();
    if(empty($gasstotal))
    {
        $gasstotal = 0;
    }
    else{
        $gasstotal = $gasstotal->gass_total;
    }
@endphp

<div class="card card-light">
    <div class="card-header">
    	<h3>{{ __('main.driver detail') }}</h3>
    </div>
    <div class="card-body">
    	<div class="row">
    		<div class="col-md-6">
    			<table id="info" class="table table-bordered" style="padding:0px;">
                <tr>
                    <td><label style="font-weight: bold; font-size: 20px;" >{{ __('main.name') }}</label></td>
                    <td style="font-weight: bold; font-size: 20px;">{{ $drive->driver->name }}</td>
                </tr>
                <tr>
                    <td> <label >{{ __('main.code') }}</label></td>
                    <td>{{ $drive->truck->code }}</td>
                </tr>
                <tr>
                    <td> <label >{{ __('main.plate') }}</label></td>
                    <td>{{ $drive->truck->plate }}</td>
                </tr>
                <tr>
                    <td> <label >{{ __('main.old balance') }}</label></td>
                    <td>{{ $drive->old_balance}}</td>
                </tr>
                <tr>
                    <td> <label >{{ __('main.date') }}</label></td>
                    <td>{{ $drive->date }}</td>
                </tr>

            </table>
    		</div>
    		<div class="col-md-6">
    			<table id="trips" class="table table-bordered" style="padding:0px;">
                <tr>
                    <td><label >{{ __('main.old balance') }}</label></td>
                    <td>{{ number_format( $drive->old_balance) }}</td>
                </tr>
                <tr>
                    <td><label >{{ __('main.no trips') }}</label></td>
                    <td>{{ number_format( $trips->count()) }}</td>
                </tr>
                <tr>
                    <td><label >{{ __('main.trip costs') }}</label></td>
                    <td>{{ number_format($trips->sum('price')) }}</td>
                </tr>
                <tr>
                    <td>  <a  href="{{ route('drive_payments.create') }}"><button type="button" class="btn btn-success btn-circle"><i class="fa fa-plus"></i>
                            </button></a><label >{{ __('main.payments') }}</label></td>
                    <td>{{ number_format($payments->sum('amount')) }}</td>
                </tr>
                <tr>
                    <td> <label >{{ __('main.expenses') }}</label></td>
                    <td>{{ number_format($expenses->sum('cost')) }}</td>
                </tr>
                <tr>
                    <td> <label >{{ __('main.fines') }}</label></td>
                    <td>{{ number_format($fines->sum('amount'))}}</td>
                </tr>
                <tr>
                    <td> <label >{{ __('main.gass') }}</label></td>
                    <td>{{ number_format($gasstotal)}}</td>
                </tr>
                 <tr>
                    <td> <label style="font-weight: bold; font-size: 20px;">{{ __('main.balance') }}</label></td>
                    <td ><span style="font-weight: bold; font-size: 20px;" class = "badge badge-success">{{ number_format($drive->old_balance+$trips->sum('price')+$expenses->sum('cost')-$payments->sum('amount')-$fines->sum('amount')-$gasstotal)}}</span></td>
                </tr>

            </table>
    		</div>
    	</div>
    	
    </div>
</div>
<div class="card card-success">
    <div class="card-header">
    	<h3>{{ __('main.trips') }}</h3>
    </div>
    <div class="card-body">
         <form method="post" id="myForm" action="{{ route('accountant.showaccounting') }}">
                    {{csrf_field()}}
                      <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                    <input type="button" class="btn btn-success" value="{{ __('main.selected') }}" id="checkBtn" onclick="checkcheckbox()">
                    {{-- <button class="btn btn-success" onclick="checkcheckbox()"><i class="fa fa-fw fa-eye"></i>{{ __('main.show accounting') }}</button> --}}
        <table id="example" style="width:100%;" class="table table-bordered table-hover ">
            <thead>
                <th>
                #
                </th>
                <th>{{ __('main.date') }}</th>
                <th>{{ __('main.invoice_no') }}</th>
                <th>{{ __('main.price') }}</th>
                {{-- <th>{{ __('main.payments') }}</th> --}}
                <th>{{ __('main.expenses') }}</th>
                <th>{{ __('main.fines') }}</th>
                <th>{{ __('main.total') }}</th>
                <th>{{ __('main.finished') }}</th>
                <th>{{ __('main.note') }}</th>
                <th>{{ __('main.edit') }}</th>
                <th>{{ __('main.select all') }}  
                    <input type="checkbox" id="select-all">
</th>
            </thead>
            <tbody>
               
                
                @foreach($trips as $t)
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $t->date}}</td>
                    <td><a href="{{ route('trips.show',$t->id) }}">{{ $t->invoice_no}}</a></td>
                    <td>{{ $t->price}}</td>
                    <td><a href="{{ route('trips.show',$t->id) }}">{{ $t->expenses->sum('cost') }}</a></td>
                    <td><a href="{{ route('trips.show',$t->id) }}">{{ $t->fines->sum('amount') }}</a></td>
                    <td><a href="{{ route('trips.show',$t->id) }}">{{ $t->price+$t->expenses->sum('cost')-$t->fines->sum('amount') }}</a></td>
                    <td @if($t->finished==1) class="bg-success" @endif><input type="checkbox" @if($t->finished==1) checked  @endif disabled=""></td>
                    <td>{{ $t->note }}</td>
                    <td>
                   {{-- <form method="GET" action="{{ route('trips.edit',$t->id) }}">
                                    {{ csrf_field() }}
 --}}
                                    {{-- <div class="form-group"> --}}
                                        <button type="button"  class="bg-success"  onclick="edittrip({{ $t->id }})" >{{ __('main.edit') }}</button>

                                    {{-- </div> --}}
                               {{--  </form> --}}</td>
                    <td><input type="checkbox" name="selected[]" value="{{ $t->id }}"></td>
                </tr>
                @endforeach
               
            </tbody>
        </table> </form>
	</div>
</div>
<div class="card card-info">
    <div class="card-header">
    	<h3>{{ __('main.payments') }}</h3>
    	
    </div>
    <div class="card-body">
    	<table style="width:100%;" class="table table-bordered table-hover ">
    		<thead>
    			<th>
    			#
	    		</th>
	    		<th>{{ __('main.date') }}</th>
	    		<th>{{ __('main.amount') }}</th>
	    		<th>{{ __('main.note') }}</th>
                <th>{{ __('main.print') }}</th>
	    	</thead>
	    	<tbody>
	    		@foreach($payments as $p)
	    		<tr>
	    			<td>{{ $loop->iteration }}</td>
	    			<td>{{ $p->date }}</td>
	    			<td>{{ number_format($p->amount) }}</td>
	    			<td>{{ $p->note }}</td>
                     <td>
                                            <form method="GET" action="{{ route('drive_payments.show',$p->id) }}">
                                                             {{ csrf_field() }}
  <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                                             <div class="form-group">
                                                                 <input type="submit" class="btn btn-info btn-sm " value="{{ __('main.print') }}" >

                                                             </div>
                                                         </form></td>
	    		</tr>
	    		@endforeach
	    	</tbody>
    	</table>
	</div>
</div>
{{-- <div class="card card-warning">
    <div class="card-header">
    	<h3>{{ __('main.expenses') }}</h3>
    </div>
    <div class="card-body">
    	<table style="width:100%;" class="table table-bordered table-hover ">
    		<thead>
    			<th>
    			#
	    		</th>
	    		<th>{{ __('main.date') }}</th>
	    		<th>{{ __('main.type') }}</th>
	    		<th>{{ __('main.amount') }}</th>
	    		<th>{{ __('main.note') }}</th>
	    	</thead>
	    	<tbody>
	    		@foreach($expenses as $p)
	    		<tr>
	    			<td>{{ $loop->iteration }}</td>
	    			<td>{{ $p->date }}</td>
	    			<td>{{ $p->type->name }}</td>
	    			<td>{{ number_format($p->cost) }}</td>
	    			<td>{{ $p->note }}</td>
	    		</tr>
	    		@endforeach
	    	</tbody>
    	</table>
	</div>
</div> --}}
{{-- <div class="card card-danger">
    <div class="card-header">
    	<h3>{{ __('main.fines') }}</h3>
    </div>
    <div class="card-body">
    	<table style="width:100%;" class="table table-bordered table-hover ">
    		<thead>
    			<th>
    			#
	    		</th>
	    		<th>{{ __('main.date') }}</th>
	    		<th>{{ __('main.amount') }}</th>
	    		<th>{{ __('main.note') }}</th>
	    	</thead>
	    	<tbody>
	    		@foreach($fines as $p)
	    		<tr>
	    			<td>{{ $loop->iteration }}</td>
	    			<td>{{ $p->created_at }}</td>
	    			<td>{{ number_format($p->amount) }}</td>
	    			<td>{{ $p->note }}</td>
	    		</tr>
	    		@endforeach
	    	</tbody>
    	</table>
	</div>
</div> --}}
<script type="text/javascript">
    function edittrip(id) {
        // body...
      //    $.get(url+'/patient/search/'+id, {}, function (data) {
      //   localStorage['searchedpatient']= JSON.stringify(data);

      // });
      var url = "{!! url('/') !!}";
      window.location.href = url+"/trips/"+id+"/edit";
    }
   function checkcheckbox() {
    // console.log("clicked");
    var o=false;
     var inps = document.getElementsByName('selected[]');
        for (var i = 0; i <inps.length; i++) {
        var inp=inps[i];
            // console.log(inp.value);
        if(inp.checked==true)
            {
                document.getElementById("myForm").submit();
                o=true;
            }
            else{
              console.log(inp.checked);  
            }
        }
        if(o==false){
        alert("لایەنی کەم یەک بار دیاری بکە");}
    }
document.getElementById('select-all').onclick = function() {
  var checkboxes = document.getElementsByName('selected[]');
  for (var checkbox of checkboxes) {
    checkbox.checked = true;
  }
}

</script>
@stop