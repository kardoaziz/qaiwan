@extends('adminlte::page')
@section('head_scripts')
    <!-- DataTables CSS -->
    {{-- {{ HTML::style('css/plugins/dataTables.bootstrap.css') }} --}}
@stop
@section('content')
<style type="text/css">
	#info tr td{
		padding: 0px !important;
	}
	#trips tr td{
		padding: 0px !important;
	}
	.btn-circle {
    width: 20px;
    height: 20px;
    padding: 3px 0px;
    border-radius: 10px;
    text-align: center;
    font-size: 12px;
    line-height: 1.42857;
}

</style>
@php
$trips=App\Trip::whereIn('id',$ids)->get();
$expenses = App\TripExpense::whereIn('trip_id',$ids)->sum('cost');
$fines = App\TripFine::whereIn('trip_id',$ids)->sum('amount');
$gasstotal = App\TripGass::join('trips','trips.id','trip_gasses.trip_id')->whereIn('trip_gasses.trip_id',$ids)->select(DB::raw('(trip_gasses.qty*trip_gasses.price) as gass_total'))->first();
@endphp
<div class="row">
    <div class="col col-md-12">
    <div class="card card-success">
    <div class="card-header">
        <h3>{{ __('main.trips') }}</h3>
    </div>
    <div class="card-body">
    <table id="example" style="width:100%;" class="table table-bordered table-hover ">
            <thead>
                <th>
                #
                </th>
                <th>{{ __('main.date') }}</th>
                <th>{{ __('main.invoice_no') }}</th>
                <th>{{ __('main.price') }}</th>
                <th>{{ __('main.expenses') }}</th>
                <th>{{ __('main.fines') }}</th>
                <th>{{ __('main.gass') }}</th>
                <th>{{ __('main.total') }}</th>
                <th>{{ __('main.finished') }}</th>
                <th>{{ __('main.note') }}</th>
                {{-- <th>{{ __('main.edit') }}</th> --}}
                {{-- <th>{{ __('main.select') }}</th> --}}
            </thead>
            <tbody>
                @foreach($trips as $t)
                @php
                $tt= App\TripGass::join('trips','trips.id','trip_gasses.trip_id')->where('trips.id',$t->id)->select(DB::raw('IFNULL(sum(trip_gasses.qty*trip_gasses.price),0) as total'))->first()->total;
                @endphp
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $t->date}}</td>
                    <td><a href="{{ route('trips.show',$t->id) }}">{{ $t->invoice_no}}</a></td>
                    <td>{{ $t->price}}</td>
                    <td><a href="{{ route('trips.show',$t->id) }}">{{ $t->expenses->sum('cost') }}</a></td>
                    <td><a href="{{ route('trips.show',$t->id) }}">{{ $t->fines->sum('amount') }}</a></td>
                    <td><a href="{{ route('trips.show',$t->id) }}">{{ $tt }}</a></td>
                    <td><a href="{{ route('trips.show',$t->id) }}">{{ $t->price+$t->expenses->sum('cost')-$t->fines->sum('amount')-$tt }}</a></td>
                    <td><input type="checkbox" name="selected[]" @if($t->finished==1) checked @endif></td>
                    <td>{{ $t->note }}</td>

                </tr>
                @endforeach
            </tbody>
        </table>
</div></div></div>
    </div>
    <div class="row">
        <div class="col col-md-12">
            <div class="card card-info">
                <div class="card-header">
                    <h3>{{ __('main.accounting') }}</h3>
                </div>
                <div class="card-body">
                    <table class="table table-bordered">
                        <tbody>
                            <tr>
                                <td>{{ __('main.no trips') }}</td>
                                <td>{{ $trips->count() }}</td>
                            </tr>
                            <tr>
                                <td>{{ __('main.price') }}</td>
                                <td>{{ $trips->sum('price') }}</td>
                            </tr>
                            <tr>
                                <td>{{ __('main.expenses') }}</td>
                                <td>{{ $expenses }}</td>
                            </tr>
                            <tr>
                                <td>{{ __('main.fines') }}</td>
                                <td>{{ $fines }}</td>
                            </tr>
                             <tr>
                                <td>{{ __('main.gass') }}</td>
                                <td>{{ $gasstotal->gass_total }}</td>
                            </tr>
                            <tr>
                                <td>{{ __('main.total') }}</td>
                                <td>{{ $trips->sum('price')+$expenses-$fines-$gasstotal->gass_total }}</td>
                            </tr>
                            <tr>
                                <td>{{ __('main.accounting') }}</td>
                                <td><form method="post" id="myForm" action="{{ route('accountant.finishaccounting') }}">
                                      <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                    {{csrf_field()}}
                                    @foreach($ids as $i)
                                    <input type="hidden" name="ids[]" value="{{ $i }}">
                                    @endforeach
                                    <button type="button" class="btn btn-success" onclick="checkcheckbox()"><i class="fa fa-fw fa-check"></i>{{ __('main.mark as finish') }}</button></form>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
</div>
<script type="text/javascript">
    function checkcheckbox() {
    // console.log("clicked");
    var o=false;
     var inps = document.getElementsByName('selected[]');
        for (var i = 0; i <inps.length; i++) {
        var inp=inps[i];
            // console.log(inp.value);
        if(inp.checked==true)
            {
                document.getElementById("myForm").submit();
                o=true;
            }
            else{
              console.log(inp.checked);  
            }
        }
        if(o==false){
        alert("لایەنی کەم یەک بار دیاری بکە");}
    }
</script>
@stop