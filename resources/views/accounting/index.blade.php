@extends('adminlte::page')
@section('head_scripts')
    <!-- DataTables CSS -->
    {{-- {{ HTML::style('css/plugins/dataTables.bootstrap.css') }} --}}
@stop
@section('content')
<style type="text/css">
    .badge{
        font-size: 17px;
    }
</style>

<h3>{{ __('main.accounting') }}</h3>
<div class="card card-light">
    <div class="card-header bg-danger text-center">
        {{ __('main.accounting') }}
    </div>
    <div class="card-body">
        <div class="table-responsive">
            @php
                    $drivers = App\Drive::select(DB::raw('IFNULL((select sum(amount) from drive_payments where drive_id=drives.id),0) as payments'),DB::raw('IFNULL((select sum(price) from trips where drive_id=drives.id),0) as trips'),'driver_id','truck_id',
                        DB::raw('IFNULL((select sum(cost) from trip_expenses, trips where trips.id=trip_expenses.trip_id and trips.drive_id=drives.id),0) as expenses'),DB::raw('IFNULL((select sum(amount) from trip_fines, trips where trips.id=trip_fines.trip_id and trips.drive_id=drives.id),0) as fines'),'id','status','old_balance')->get();
                    @endphp
        <table id="example" class="table table-hover" >
                <thead class="table-danger">
                    <tr>
                        <th>#</th>
                        <th>{{ __('main.name') }}</th>
                        <th>{{ __('main.code') }}</th>
                        <th>{{ __('main.status') }}</th>
                        <th>{{ __('main.old balance') }}</th>
                        <th>{{ __('main.trips') }}</th>
                        <th>{{ __('main.payments') }}</th>
                        <th>{{ __('main.expenses') }}</th>
                        <th>{{ __('main.fine') }}</th>
                        <th>{{ __('main.balance') }}</th>
                        <th>{{ __('main.note') }}</th>
                        <th>{{ __('main.details') }}</th>
                    </tr>
                </thead>
                <tbody>
                    
                    @foreach($drivers as $d)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $d->driver->name }}</td>
                        <td>{{ $d->truck->code }}</td>
                        <td>
                            @if($d->status == "active")
                            <span class = "badge badge-success">{{ __('main.'.$d->status) }}</span>
                            @else
                            <span class = "badge badge-danger">{{ __('main.'.$d->status) }}</span>
                            @endif
                        </td>
                        <td>{{ number_format($d->old_balance) }}</td>
                        <td>{{ number_format($d->trips) }}</td>
                        <td>{{ number_format($d->payments) }}</td>
                        <td> {{ number_format($d->expenses) }}</td>
                        <td>{{ number_format($d->fines) }}</td>
                        <td>{{ number_format($d->old_balance+$d->trips + $d->expenses - $d->fines - $d->payments) }}</td>
                        <td>{{ $d->note }}</td>
                        <td>
                           <form method="GET" action="{{ route('accounting.show',$d->id) }}">
                                            {{ csrf_field() }}
  <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                            <div class="form-group">
                                                <input type="submit" class="btn btn-info btn-sm " value="{{ __('main.details') }}" >

                                            </div>
                                        </form>
                        </td>
                        
                    </tr>
                     @endforeach
                </tbody>
                <!-- <tfoot>
                    <tr>
                        <td colspan="6">{{ __('main.total') }}</td>
                        <td colspan="6">{{ $drivers->sum('price') }}</td>
                    </tr>
                </tfoot> -->
            </table>
        </div>
    </div>
</div>
@stop
