@extends('adminlte::page')
@section('head_scripts')
    <!-- DataTables CSS -->
    {{-- {{ HTML::style('css/plugins/dataTables.bootstrap.css') }} --}}
@stop
@section('content')
<style type="text/css">
	td{
		text-align: center;
	}
	th {
		text-align: center !important;
	}
</style>

<div class="card card-primary">
            <div class="card-header with-border text-center">
              {{ __('main.new fine') }}
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form role="form" action="{{ route('fines.store') }}" method="POST">
            	    {{csrf_field()}}
          <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
              <div class="card-body">
                @php
                $lastDate=App\FineDates::orderBy('id', 'DESC')->first();
                @endphp
                <div class="table-responsive">
                <table class="table table-hover">
                    <thead class="thead-dark">
                        <tr>
                            <th>{{ __('main.unit') }}</th>
                            <th>{{ __('main.normal') }}</th>
                            <th>{{ __('main.fine if higher') }}</th>
                            <!-- <th>{{ __('main.price') }}</th> -->
                        </tr>
                    </thead>
                    @if ( $lastDate!=null)
                    @php
                    $lastPrice=App\Fines::where('date_id',$lastDate->id)->get();
                  
                    @endphp
                @foreach ($lastPrice as $l)

                   
                            <tr>
                                <td>
                                    <div class="form-group">
                                    <input type="text" readonly class="form-control"  value="{{ $l->unit }}" name="unit[]" required>
                                   <!--  <input type="text" class="form-control"  value="{{ $l->unit }}" disabled> -->
                                </div></td>
                                <td>
                                    <div class="form-group">
                                    <input type="number" class="form-control"  value="{{ $l->normal }}" name="normal[]" required>
                                   <!--  <input type="text " class="form-control"  value="{{ $l->normal }}" disabled> -->
                                    </div></td>
                                    <td>
                                    <div class="form-group">
                                    <input type="number" class="form-control"  value="{{ $l->fine }}" name="fine[]" required>
                                    </div></td>
                            </tr>

                       @endforeach 
                       @else
                        <tr>
                                <td>
                                    <div class="form-group">
                                    <input type="text" readonly class="form-control"  value="ton" name="unit[]">
                                   <!--  <input type="text" class="form-control"  value="ton" disabled> -->
                                </div></td>
                                <td>
                                    <div class="form-group">
                                    <input type="text" class="form-control"  value="0" name="normal[]">
                                   <!--  <input type="text " class="form-control"  value="0" disabled> -->
                                    </div></td>
                                    <td>
                                    <div class="form-group">
                                    <input type="number" class="form-control"  value="0" name="fine[]">
                                    </div></td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="form-group">
                                    <input type="text" readonly class="form-control"  value="kilo" name="unit[]">
                                   <!--  <input type="text" class="form-control"  value="0" disabled> -->
                                </div></td>
                                <td>
                                    <div class="form-group">
                                    <input type="number" class="form-control"  value="0" name="normal[]">
                                   <!--  <input type="text " class="form-control"  value="0" disabled> -->
                                    </div></td>
                                    <td>
                                    <div class="form-group">
                                    <input type="number" class="form-control"  value="0" name="fine[]">
                                    </div></td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="form-group">
                                    <input type="text" readonly class="form-control"  value="litr" name="unit[]">
                                   <!--  <input type="text" class="form-control"  value="0" disabled> -->
                                </div></td>
                                <td>
                                    <div class="form-group">
                                    <input type="number" class="form-control"  value="0" name="normal[]">
                                   <!--  <input type="text " class="form-control"  value="0" disabled> -->
                                    </div></td>
                                    <td>
                                    <div class="form-group">
                                    <input type="number" class="form-control"  value="0" name="fine[]">
                                    </div></td>
                            </tr>
                       @endif 

                  </table>
                </div>
              </div>
              <!-- /.box-body -->

              <div class="card-footer">
                <button type="submit" class="btn btn-success"><i class="fa fa-fw fa-save"></i>{{ __('main.save') }}</button>
              </div>
            </form>
          </div>
@stop
