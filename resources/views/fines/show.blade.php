
@extends('adminlte::page')
@section('head_scripts')
    <!-- DataTables CSS -->
    {{-- {{ HTML::style('css/plugins/dataTables.bootstrap.css') }} --}}
@stop
@section('content')

<div class="card card-light">
    <div class="card-header text-center">
        <h3>{{ __('main.fines') }} {{ __('main.from') }} {{$data->created_at->format('بەرواری : Y/m/d | کات : h:i A')}}</h3>
    </div>
    <div class="card-body">
        <div class="table-responsive">
        <table id="example" class="table table-hover" >
                <thead class="thead-dark">
                    <tr>
                        <th>#</th>
                        <th>{{ __('main.unit') }}</th>
                        <th>{{ __('main.normal') }}</th>
                        <th>{{ __('main.fine if higher') }}</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($fines as $d)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{$d->unit}}</td>
                        <td>{{$d->normal}}</td>
                        <td>{{$d->fine}}</td>
                    </tr>
                     @endforeach
                </tbody>
            </table>
        </div>
        </div>
</div>
@stop
