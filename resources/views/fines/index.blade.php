@extends('adminlte::page')
@section('head_scripts')
    <!-- DataTables CSS -->
    {{-- {{ HTML::style('css/plugins/dataTables.bootstrap.css') }} --}}
@stop
@section('content')
<h3>{{ __('main.fine prices') }}</h3>
<div class="card card-light">
    <div class="card-header">
         @can('add_fine_price')<a  href="{{ route('fines.create') }}"><button type="button" class="btn btn-success"><i class="fa fa-fw fa-plus"></i>{{ __('main.new') }}</button></a>@endcan
    </div>
    <div class="card-body">
        <div class="table-responsive">
        <table id="example" class="table table-hover" >
                <thead class="table-active">
                    <tr>
                        <th>#</th>
                        <th>{{ __('main.date') }}</th>

                        <th>{{ __('main.show') }}</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach(App\FineDates::orderby('id','desc')->get() as $d)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ Carbon\Carbon::parse($d->date)->format('d/m/Y')}}</td>

                        <td>
                           <form method="GET" action="{{ route('fines.show',$d->id) }}">
                                            {{ csrf_field() }}
  <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                            <div class="form-group">
                                                <input type="submit" class="btn btn-info btn-sm " value="{{ __('main.show') }}" >

                                            </div>
                                        </form></td>

                    </tr>
                     @endforeach
                </tbody>
            </table>
        </div>
        </div>
</div>
@stop
