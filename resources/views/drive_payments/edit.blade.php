@extends('adminlte::page')
@section('head_scripts')
    <!-- DataTables CSS -->
    {{-- {{ HTML::style('css/plugins/dataTables.bootstrap.css') }} --}}
@stop
@section('content')
<style type="text/css">
  td{
    text-align: center;
  }
  th {
    text-align: center !important;
  }
</style>

<div class="card card-primary">
            <div class="card-header with-border text-center">
              {{ __('main.update payment') }}
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form role="form" action="{{ route('drive_payments.update',$payment->id) }}" method="POST">
            	    {{csrf_field()}}
                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
<input type="hidden" name="_method" value="PUT">
              <div class="card-body">
                <div class="form-group">
                  <label >{{ __('main.name') }}</label>
                  <input type="hidden" name="drive_id" id="drive_id" value="{{ $payment->drive_id }}">
                  <input list="driverslist" name="drives" id="drives" class="form-control" onchange="selectdriver(this.value)" value="{{ $payment->drive_id }}"required >
                <datalist id="driverslist">
                  @foreach (App\Drive::all() as $d)
                <option value="{{$d->id}}-{{ $d->driver->name }} - {{$d->truck->code}}">{{$d->driver->name}} - {{$d->truck->code}}</option>
                            @endforeach
                </datalist>
                </div>
              <div class="form-group">
                  <label @cannot('choose_snduq') style="display: none;" @endif>{{ __('main.snduq') }}</label>
                  {{-- <input type="hidden" name="drive_id" id="drive_id"> --}}
                  {{-- <input list="driverslist" name="drives" id="drives" class="form-control" onchange="selectdriver(this.value)" required> --}}
                <select class="form-control" name="snduq_id"  @cannot('choose_snduq') style="display: none;" @endif value="{{ $payment->snduq_id }}">
                  @foreach (App\Snduq::all() as $d)
                <option value="{{$d->id}}" @if($d->default==1) selected @endif>{{$d->name}}</option>
                            @endforeach
                </select>
                </div>
                
                <div class="form-group">
                  <input class="form-control" type="text" name="driver" id="driver" disabled value="{{ $payment->driven->driver->name }}">
                </div>
                {{-- <div class="form-group">
                  <label >{{ __('main.code') }}</label>
                  <input type="text" class="form-control"  placeholder="{{ __('main.code') }}" name="code">

                </div> --}}
                 <div class="form-group">
                  <label >{{ __('main.date') }}</label>
                  <input type="date" class="form-control"  placeholder="{{ __('main.date') }}" name="date" value="{{ $payment->date }}" required>

                </div>
              {{-- </div> --}}
                 <div class="form-group">
                  <label >{{ __('main.amount') }}</label>
                  <input type="number" step="any" min="0" class="form-control"  placeholder="{{ __('main.amount') }}" name="amount" value="{{ $payment->amount }}" required onblur="update('amount',this.value,{{ App\DollarPrice::orderby('created_at','desc')->first()->price }})">

                </div>
                <div class="form-group">
                  <label >{{ __('main.dollar') }}</label>
                  <input type="number" step="any" id="dollar" min="0" class="form-control"  placeholder="{{ __('main.dollar') }}" name="dollar" value="{{ $payment->dollar }}" required onblur="update('dollar',this.value,{{ App\DollarPrice::orderby('created_at','desc')->first()->price }})">

                </div>
                 <div class="form-group">
                  <label >{{ __('main.note') }}</label>
                  <input type="text" class="form-control"  placeholder="{{ __('main.note') }}" name="note" value="{{ $payment->note }}">

                </div>
               
              </div>
              <!-- /.box-body -->

              <div class="card-footer">
                <button type="submit" class="btn btn-success"><i class="fa fa-fw fa-save"></i>{{ __('main.save') }}</button>
              </div>
            </form>
          </div>
          <script type="text/javascript">
              function update(field,val,dollar) {
        // body...
        if(field=='amount'){
            document.getElementById('dollar').value = (val/dollar);
        }
        else if(field=='dollar')
        {
           document.getElementById('amount').value =(val*dollar);
        }
      }
             function selectdriver(id) {

$('#drive_id').val(id.split("-")[0]);
$('#driver').val(id.split("-")[1]);
}
          </script>
          @stop