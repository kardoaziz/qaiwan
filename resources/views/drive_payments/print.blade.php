@extends('adminlte::page')
@section('head_scripts')
    <!-- DataTables CSS -->
    {{-- {{ HTML::style('css/plugins/dataTables.bootstrap.css') }} --}}
@stop


@section('content')
<style type="text/css">
    hr {
        margin-top: 0px;
    }
    .content-wrapper {
    background: #fff !important;
}
  @media print {
        .noprint{
            display: none;
        }
        }
</style>
    <div class="container ">
        <div class="row">
            <table class="table table-borderless" >
                <tr>
                    <td  class="text-left">
                        <img src="{{asset('img/logo.png')}}" style="width:300px; height:140px;"></td>
                    <td><br><h5 class="text-left" style="margin-bottom: 0px;">{{ __('main.code')}} : {{$payment->id}}</h5>
                        <br>
                    <h5 class="text-left">{{ __('main.date')}} : {{$payment->date}}</h5></td>
                </tr>
               
            </table>
           
        </div>
        <hr>
        @php
        $user=Auth::user();
        @endphp

        <div class="container">
            <table class="table table-borderless" >
                <tr>
                    <td><h5 class="text-left">{{ __('main.name')}} : {{$payment->driven->driver->name}}</h5></td>
                </tr>
                <tr>
                    <td><h5 class="text-left">{{ __('main.truck')}} : {{$payment->driven->truck->code}}</h5></td>
                </tr>
                <tr>
                    <td><h5 class="text-left">{{ __('main.amount')}} : {{$payment->amount}}</h5></td>
                </tr>
                <tr>
                    <td><h5 class="text-left">{{ __('main.note')}} : {{$payment->note}}</h5></td>
                </tr>
            </table>
        </div>
        <br>
        <br>
        <hr>
        <br>
        <br>
        <div class="container">
            <table class="table table-borderless" >
                <tr>
                    <td><p class="text-left">{{ __('main.sign')}} <br> {{ __('main.name')}} : {{$payment->driven->driver->name}}
                        <br> {{ __('main.phone')}} : {{$payment->driven->driver->phone}}</p> </td>
                    <td><p class="text-right">{{ __('main.sign')}} <br>{{ __('main.user')}} : {{$user->name}}</p></td>
                </tr>

            </table>
        </div>
        <hr>
        <div class="row">
            <table class="table table-borderless" >
                <tr>
                    <td  class="text-left">
                        <img src="{{asset('img/logo.png')}}" style="width:300px; height:140px;"></td>
                    <td><br><h5 class="text-left" style="margin-bottom: 0px;">{{ __('main.code')}} : {{$payment->id}}</h5>
                        <br>
                    <h5 class="text-left">{{ __('main.date')}} : {{$payment->date}}</h5></td>
                </tr>
               
            </table>
           
        </div>
        <hr>
        <div class="container">
            <table class="table table-borderless" >
                <tr>
                    <td><h5 class="text-left">{{ __('main.name')}} : {{$payment->driven->driver->name}}</h5></td>
                </tr>
                <tr>
                    <td><h5 class="text-left">{{ __('main.truck')}} : {{$payment->driven->truck->code}}</h5></td>
                </tr>
                <tr>
                    <td><h5 class="text-left">{{ __('main.amount')}} : {{$payment->amount}}</h5></td>
                </tr>
                <tr>
                    <td><h5 class="text-left">{{ __('main.note')}} : {{$payment->note}}</h5></td>
                </tr>
            </table>
        </div>
        <hr>
        <div class="container">
            <table class="table table-borderless" >
                <tr>
                    <td><p class="text-left">{{ __('main.sign')}} <br> {{ __('main.name')}} : {{$payment->driven->driver->name}}
                        <br> {{ __('main.phone')}} : {{$payment->driven->driver->phone}}</p> </td>
                        <td><p class="text-right">{{ __('main.sign')}} <br>{{ __('main.user')}} : {{$user->name}}</p></td>
                </tr>

            </table>
        </div>
        <hr>
    </div>

@stop
