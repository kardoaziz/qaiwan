@extends('adminlte::page')
@section('head_scripts')
    <!-- DataTables CSS -->
    {{-- {{ HTML::style('css/plugins/dataTables.bootstrap.css') }} --}}
@stop
@section('content')
<h3>{{ __('main.suppliers') }}</h3>
<div class="card card-light">
    <div class="card-header bg-info">
        @can('add_supplier')<a  href="{{ route('suppliers.create') }}"><button type="button" class="btn btn-light"><i class="fa fa-fw fa-plus"></i>{{ __('main.new') }}</button></a>@endcan
    </div>
    <div class="card-body">
        <div class="table-responsive">
<table id="example" class="table table-hover">
        <thead class="table-light">
            <tr>
                <th>#</th>
                <th>{{ __('main.name') }}</th>
                <th>{{ __('main.phone') }}</th>
                <th>{{ __('main.email') }}</th>
                <th>{{ __('main.address') }}</th>
                <th>{{ __('main.note') }}</th>
                 @can('edit_supplier')<th>{{ __('main.edit') }}</th>@endcan
                @can('delete_supplier') <th>{{ __('main.delete') }}</th>@endcan
            </tr>
        </thead>
        <tbody>
        	@foreach(App\Suppliers::all() as $d)
            <tr>
                <td>{{ $loop->iteration }}</td>
                <td>{{ $d->name }}</td>
                <td>{{ $d->phone }}</td>
                <td>{{ $d->email }}</td>
                <td>{{ $d->address }}</td>
                <td>{{ $d->note }}</td>
                @can('edit_supplier') <td>
                   <form method="GET" action="{{ route('suppliers.edit',$d->id) }}">
                                    {{ csrf_field() }}
  <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                    <div class="form-group">
                                        <input type="submit" class="btn btn-info btn-sm " value="{{ __('main.edit') }}" >

                                    </div>
                                </form></td>@endcan
                 @can('delete_supplier')<td>
                                <form action="{{ route('suppliers.destroy',$d->id) }}" method="post">
                         {{ csrf_field() }}
                                <input type="hidden" name="_method" value="DELETE"  >
  <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                    <a style="padding: 0px;
                      font-size: 12px;
                      color: red;" role="menuitem" tabindex="-1"  onclick="return confirm('{{ __('main.are you sure') }}')">
                      <button type="submit" class="btn btn-sm btn-danger"><i class="fa fa-fw fa-trash"></i></button>
                    </a>
                  </form>
                  </td>@endcan
            </tr>
             @endforeach
        </tbody>
    </table>
        </div>

    </div>
</div>
@stop
