@extends('adminlte::page')
@section('head_scripts')
    <!-- DataTables CSS -->
    {{-- {{ HTML::style('css/plugins/dataTables.bootstrap.css') }} --}}
@stop
@section('content')
<style type="text/css">
	td{
		text-align: center;
	}
	th {
		text-align: center !important;
	}
</style>

<div class="card card-primary">
            <div class="card-header with-border text-center">
              {{ __('main.update supplier') }}
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form role="form" action="{{ route('suppliers.update',$supplier->id) }}" method="POST">
            	     {{csrf_field()}}
                     <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
          <input type="hidden" name="_method" value="PUT">
              <div class="card-body">
                <div class="form-group">
                  <label >{{ __('main.name') }}</label>
                  <input type="text" class="form-control"  placeholder="{{ __('main.name') }}" name="name" required value="{{ $supplier->name }}" required>
                </div>
                <div class="form-group">
                  <label >{{ __('main.phone') }}</label>
                  <input type="text" class="form-control"  placeholder="{{ __('main.phone') }}" name="phone" value="{{ $supplier->phone }}" required>
                </div>
                <div class="form-group">
                  <label >{{ __('main.email') }}</label>
                  <input type="email" class="form-control"  placeholder="{{ __('main.email') }}" name="email" value="{{ $supplier->email }}">

                </div> 
                <div class="form-group">
                  <label >{{ __('main.address') }}</label>
                  <input type="text" class="form-control"  placeholder="{{ __('main.address') }}" name="address" value="{{ $supplier->address }}">

                </div>
                <div class="form-group">
                  <label >{{ __('main.previous_balance') }}</label>
                  <input type="number" class="form-control"  placeholder="{{ __('main.previous_balance') }}" name="previous_balance" value="0" required value="{{ $supplier->previous_balance }}">

                </div>
                <div class="form-group">
                  <label >{{ __('main.note') }}</label>
                  <input type="text" class="form-control"  placeholder="{{ __('main.note') }}" name="note" value="{{ $supplier->note }}">

                </div>
               
              </div>
              <!-- /.box-body -->

              <div class="card-footer">
                <button type="submit" class="btn btn-success"><i class="fa fa-fw fa-save"></i>{{ __('main.save') }}</button>
              </div>
            </form>
          </div>
@stop