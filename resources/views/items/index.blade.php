@extends('adminlte::page')
@section('head_scripts')
    <!-- DataTables CSS -->
    {{-- {{ HTML::style('css/plugins/dataTables.bootstrap.css') }} --}}
@stop
@section('content')
<h3>{{ __('main.items') }}</h3>
<div class="card card-light">
    <div class="card-header bg-info">
         @can('add_item')
         <a  href="{{ route('items.create') }}"><button type="button" class="btn btn-light"><i class="fa fa-fw fa-plus"></i>{{ __('main.new') }}</button></a>
         @endcan
        <a  href="{{ route('items.alert',1) }}"><button type="button" class="btn btn-light"><i class="fa fa-fw fa-info-circle"></i>{{ __('main.alerted') }}</button></a>
         <a  href="{{ route('items.projectparts',1) }}"><button type="button" class="btn btn-light"><i class="fa fa-fw fa-info-circle"></i>{{ __('main.projectparts') }}</button></a>
    </div>
    <div class="card-body">
        <div class="table-responsive">
        <table id="example" class="table table-hover" >
                <thead class="table-primary">
                    <tr>
                        <th>#</th>
                        <th>{{ __('main.name') }}</th>
                        <th>{{ __('main.model') }}</th>
                        <th>{{ __('main.size') }}</th>
                        <th>{{ __('main.code') }}</th>
                        <th>{{ __('main.location') }}</th>
                        <th>{{ __('main.description') }}</th>
                        <th>{{ __('main.stock') }}</th>
                        <th>{{ __('main.avg cost') }}</th>
                        <th>{{ __('main.avg cost $') }}</th>
                        <th>{{ __('main.stock cost') }}</th>
                        <th>{{ __('main.stock cost $') }}</th>
                        <th>{{ __('main.note') }}</th>
                         @can('edit_item')<th>{{ __('main.edit') }}</th>@endcan
                        @can('delete_item') <th>{{ __('main.delete') }}</th>@endcan
                    </tr>
                </thead>
                <tbody>
                    @php
                        if(isset($projectparts)) $items = App\Items::where('sell_price','>',0)->get();
                        else $items = App\Items::all();
                    @endphp
                    @foreach($items as $d)
                    @php
                         $stock = App\PurchaseItem::where('item_id',$d->id)->sum('qty')-App\ServiceItems::where('item_id',$d->id)->sum('qty')-App\ServiceOutItems::where('item_id',$d->id)->sum('qty');
                        // echo $stock;
                         $total = App\PurchaseItem::where('item_id',$d->id)->select(DB::raw('IFNULL(sum(qty*price),0) as total'),DB::raw('IFNULL(sum(qty),0) as qty'))->first();
                         $totald = App\PurchaseItem::where('item_id',$d->id)->select(DB::raw('IFNULL(sum(qty*dollar),0) as total'),DB::raw('IFNULL(sum(qty),0) as qty'))->first();
                         // echo $total;
                         $d->stock = $stock;
                         if($total->qty>0)
                          {
                            $d->avg_price = $total->total/$total->qty;
                            $d->avg_priced = $totald->total/$total->qty;
                          $d->total_cost = ($total->total/$total->qty)*$stock;
                          $d->total_costd = ($totald->total/$total->qty)*$stock;
                        }
                         else
                          {$d->avg_price = 0;
                            $d->total_cost=0;}

                        @endphp
                        @if((isset($alert) && $stock<= $d->alert_qty) || empty($alert))
                    <tr>

                        <td>{{ $loop->iteration }}</td>
                        <td @if($stock<0 || $stock<= $d->alert_qty) style="color:red; font-weight: bold;" @endif>{{ $d->name }}</td>
                        <td>@if(isset( $d->itemModel)) {{ $d->itemModel->name }} @endif</td>
                        <td>@if(isset($d->itemSize)){{$d->itemSize->name }}@endif</td>
                        <td>{{ $d->code }}</td>
                        <td>{{ $d->location }}</td>
                        <td>{{ $d->description }}</td>
                        <td @if($stock<0 || $stock<= $d->alert_qty) style="color:red; font-weight: bold;" @endif>{{ $stock }}</td>
                        <td>{{ number_format($d->avg_price)}}</td>
                        <td>{{ number_format($d->avg_priced) }}</td>
                        <td @if($stock<0 || $stock<= $d->alert_qty) style="color:red; font-weight: bold;" @endif>{{ number_format( $d->total_cost,2) }} </td>
                        <td @if($stock<0 || $stock<= $d->alert_qty) style="color:red; font-weight: bold;" @endif>{{ number_format( $d->total_costd,2) }} </td>
                        <td>{{ $d->note }}</td>
                         @can('edit_item')<td>
                           <form method="GET" action="{{ route('items.edit',$d->id) }}">
                                            {{ csrf_field() }}
  <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                            <div class="form-group">
                                                <input type="submit" class="btn btn-info btn-sm " value="{{ __('main.edit') }}" >

                                            </div>
                                        </form></td>@endcan
                       @can('delete_item')  <td>
                                        <form action="{{ route('items.destroy',$d->id) }}" method="post">
                                 {{ csrf_field() }}
                                   <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                        <input type="hidden" name="_method" value="DELETE"  >

                            <a style="padding: 0px;
                              font-size: 12px;
                              color: red;" role="menuitem" tabindex="-1"  onclick="return confirm('{{ __('main.are you sure') }}')">
                              <button type="submit" class="btn btn-sm btn-danger"><i class="fa fa-fw fa-trash"></i></button>
                            </a>
                          </form>
                          </td>@endcan
                    </tr>
                    @endif
                     @endforeach

                </tbody>
                <tfoot>
                    <tr>
                         <td colspan="7">
                             {{ __('main.total') }}
                         </td>
                         <td  class="text-center">{{ $items->sum('stock') }}</td>
                         <td  class="text-center"></td>
                         <td  class="text-center"></td>
                         <td  class="text-center">{{ number_format($items->sum('total_cost'),2) }}</td>
                         <td  class="text-center">{{ number_format($items->sum('total_costd'),2) }}</td>
                         <td  class="text-center"></td>
                         <td colspan="2"></td>
                     </tr>
                </tfoot>
            </table>
        </div>
        </div>
</div>
@stop
