@extends('adminlte::page')
@section('head_scripts')
    <!-- DataTables CSS -->
    {{-- {{ HTML::style('css/plugins/dataTables.bootstrap.css') }} --}}
@stop
@section('content')
<style type="text/css">
	td{
		text-align: center;
	}
	th {
		text-align: center !important;
	}
</style>

<div class="card card-primary">
            <div class="card-header with-border text-center">
              {{ __('main.new item') }}
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form role="form" action="{{ route('items.store') }}" method="POST">
            	    {{csrf_field()}}
          <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
              <div class="card-body">
                <div class="form-group">
                  <label >{{ __('main.name') }}</label>
                  <input type="text" class="form-control"  placeholder="{{ __('main.name') }}" name="name" required>
                </div>

                <div class="form-group">
                    <label >{{ __('main.model') }}</label>
                    <input type="hidden" name="model_id" id="model_id" >
                    <input list="modellist" name="model"  class="form-control" onblur="getModelName(this.value)" required>
                  <datalist id="modellist">
                    @foreach(App\ItemModel::all() as $c)
                    <option value="{{ $c->id }}-{{ $c->name }}">{{ $c->name }} </option>
                    @endforeach
                  </datalist>
                  </div>
                  <div class="form-group">
                    <input type="text" name="model_name" id="model_name" disabled="">
                   </div>
                  <div class="form-group">
                    <label >{{ __('main.size') }}</label>
                    <input type="hidden" name="size_id"  id="size_id">
                    <input list="sizelist" name="sizes" class="form-control" onblur="getSizeName(this.value)" required>
                  <datalist id="sizelist">
                    @foreach(App\ItemSize::all() as $c2)
                    <option value="{{ $c2->id }}-{{ $c2->name }}">{{ $c2->name }} </option>
                    @endforeach
                  </datalist>
                  </div>
                   <div class="form-group">
                    <input type="text" name="size_name" id="size_name" disabled="">
                   </div>
                <div class="form-group">
                  <label >{{ __('main.code') }}</label>
                  <input type="text" class="form-control"  placeholder="{{ __('main.code') }}" name="code" required>

                </div>
                 <div class="form-group">
                  <label >{{ __('main.location') }}</label>
                  <input type="text" class="form-control"  placeholder="{{ __('main.location') }}" name="location" required>

                </div>
                <div class="form-group">
                  <label >{{ __('main.selling price') }}</label>
                  <input type="number" step="any" class="form-control"  placeholder="{{ __('main.selling price') }}" name="sell_price" value="0" required>

                </div>
              {{-- </div> --}}
                 <div class="form-group">
                  <label >{{ __('main.alert qty') }}</label>
                  <input type="number" step="0.01" min="0" class="form-control"  placeholder="{{ __('main.alert_qty') }}" name="alert_qty">

                </div>
                 <div class="form-group">
                  <label >{{ __('main.description') }}</label>
                  <input type="text" class="form-control"  placeholder="{{ __('main.description') }}" name="description">

                </div>

              </div>
              <!-- /.box-body -->

              <div class="card-footer">
                <button type="submit" class="btn btn-success"><i class="fa fa-fw fa-save"></i>{{ __('main.save') }}</button>
              </div>
            </form>
          </div>
          <script type="text/javascript">
            function getModelName(id) {
              console.log(id);
              $('#model_id').val(id.split("-")[0]);
              $('#model_name').val(id.split("-")[1]);
              // body...
            }
            function getSizeName(id) {
              console.log(id);
              $('#size_id').val(id.split("-")[0]);
             $('#size_name').val(id.split("-")[1]);
              // body...
            }
          </script>
@stop
