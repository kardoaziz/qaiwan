@extends('adminlte::page')
@section('head_scripts')
    <!-- DataTables CSS -->
    {{-- {{ HTML::style('css/plugins/dataTables.bootstrap.css') }} --}}
@stop
@section('content')
<style type="text/css">
	td{
		text-align: center;
	}
	th {
		text-align: center !important;
	}
</style>

<div class="card card-primary">
            <div class="card-header with-border text-center">
              {{ __('main.update item') }}
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form role="form" action="{{ route('items.update',$item->id) }}" method="POST">
            	     {{csrf_field()}}
                     <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
          <input type="hidden" name="_method" value="PUT">
              <div class="card-body">
                <div class="form-group">
                  <label >{{ __('main.name') }}</label>
                  <input type="text" class="form-control"  placeholder="{{ __('main.name') }}" name="name" value="{{ $item->name }}" required>
                </div>
                <div class="form-group">
                    <label >{{ __('main.model') }}</label>
                    <input list="modellist" name="model_id" value="{{ $item->model_id }}" class="form-control" onchange="hideList(this)" required>
                  <datalist id="modellist">
                    @foreach(App\ItemModel::all() as $c)
                    <option value="{{ $c->id }}">{{ $c->name }} </option>
                    @endforeach
                  </datalist>
                  </div>

                  <div class="form-group">
                    <label >{{ __('main.size') }}</label>
                    <input list="sizelist" name="size_id" value="{{ $item->size_id }}" class="form-control" onchange="hideList(this)" required>
                  <datalist id="sizelist">
                    @foreach(App\ItemSize::all() as $c2)
                    <option value="{{ $c2->id }}">{{ $c2->name }} </option>
                    @endforeach
                  </datalist>
                  </div>

                <div class="form-group">
                  <label >{{ __('main.code') }}</label>
                  <input type="text" class="form-control"  placeholder="{{ __('main.code') }}" name="code" value="{{ $item->code }}" required>

                </div>
                 <div class="form-group">
                  <label >{{ __('main.location') }}</label>
                  <input type="text" class="form-control"  placeholder="{{ __('main.location') }}" name="location" value="{{ $item->location }}" required>

                </div>
                 <div class="form-group">
                  <label >{{ __('main.selling price') }}</label>
                  <input type="number" step="any" class="form-control"  placeholder="{{ __('main.selling price') }}" name="sell_price" value="{{ $item->sell_price }}" required>

                 <div class="form-group">
                  <label >{{ __('main.alert qty') }}</label>
                  <input type="number" step="0.01" min="0" class="form-control"  placeholder="{{ __('main.alert_qty') }}" name="alert_qty" value="{{ $item->alert_qty }}">

                </div>
                 <div class="form-group">
                  <label >{{ __('main.description') }}</label>
                  <input type="text" class="form-control"  placeholder="{{ __('main.description') }}" name="description" value="{{ $item->description }}">

                </div>

              </div>
              <!-- /.box-body -->

              <div class="card-footer">
                <button type="submit" class="btn btn-success"><i class="fa fa-fw fa-save"></i>{{ __('main.save') }}</button>
              </div>
            </form>
          </div>
@stop
