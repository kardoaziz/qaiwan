@extends('adminlte::page')
@section('head_scripts')
    <!-- DataTables CSS -->
    {{-- {{ HTML::style('css/plugins/dataTables.bootstrap.css') }} --}}
@stop
@section('content')
<style type="text/css">
	td{
		text-align: center;
	}
	th {
		text-align: center !important;
	}
</style>


 <div class="card card-success">
 	<div class="card-header bg-info">
 		 @can('add_item')<a class="btn btn-light noprint" data-toggle="modal" data-target="#new-expense-modal" style="font-size: 18px;float:right;color:grey !important;">
                <i class="fa fa-plus"></i> {{ __("main.new") }}
              </a>@endcan
 	</div>
 	<div class="card-body">
        <div class="table-responsive">
<table id="example" class="table table-hover" >
        <thead class="table-secondary">
            <tr>
                <th>#</th>
                <th>{{ __('main.truck') }}</th>
                <th>{{ __('main.date') }}</th>
                <th>{{ __('main.cost') }}</th>
                <th>{{ __('main.dollar') }}</th>
                <th>{{ __('main.note') }}</th>
               {{--  <th>{{ __('main.license_no') }}</th>
                <th>{{ __('main.license_expire') }}</th>
                <th>{{ __('main.note') }}</th> --}}
               @can('edit_driver')<th>{{ __('main.edit') }}</th>@endcan
                @can('delete_driver')<th>{{ __('main.delete') }}</th>@endcan
            </tr>
        </thead>
        <tbody>

        	@foreach(App\Expenses::orderby('created_at','desc')->get() as $d)
            <tr>
                <td>{{ $loop->iteration }}</td>
                <td>{{ $d->driven->truck->code }}</td>
                <td>{{ $d->date }}</td>
                <td>{{ $d->cost }}</td>
                <td>{{ $d->dollar }}</td>
                <td>{{ $d->note }}</td>
                {{-- <td>{{ $d->license_no }}</td>
                <td>
                    @if( $d->license_expire && $today_date >= $d->license_expire)
                    <div style="width:100%; color:red; border:1px solid red; text-align: center;font-weight: bold;">
                        {{ $d->license_expire }}
                    </div>
                    @else
                    {{ $d->license_expire }}
                    @endif
                </td>
                <td>{{ $d->note }}</td> --}}
               @can('edit_driver') <td>
                   <form method="GET" action="{{ route('expenses.edit',$d->id) }}">
                                    {{ csrf_field() }}
  <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                    <div class="form-group">
                                        <input type="submit" class="btn btn-info btn-sm " value="{{ __('main.edit') }}" >

                                    </div>
                                </form></td>@endcan
              @can('delete_driver')  <td>
                                <form action="{{ route('expenses.destroy',$d->id) }}" method="post">
                         {{ csrf_field() }}
                           <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                <input type="hidden" name="_method" value="DELETE"  >

                    <a style="padding: 0px;
                      font-size: 12px;
                      color: red;" role="menuitem" tabindex="-1"  onclick="return confirm('{{ __('main.are you sure') }}')">
                      <button type="submit" class="btn btn-sm btn-danger"><i class="fa fa-fw fa-trash"></i></button>
                    </a>
                  </form>
                  </td>@endcan
            </tr>
             @endforeach
        </tbody>
    </table>
        </div>
    </div>
    </div>


<div class="modal fade" id="new-expense-modal" >
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" align="center"><b> {{ __("main.new") }}  {{ __("main.expense") }}</b></h4>
      </div>
      <div class="modal-body" style="overflow-y: scroll;">
        <form action="{{ route('expenses.store') }}" method="POST" >
                {{csrf_field()}}
          <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
          

          <div class="form-group">
            <label for="exampleInputEmail1"> {{ __("main.date") }}</label>
              <input type="date" id="date" name="date" class="form-control" placeholder="date" required >
          </div>
           <div class="form-group">
                  <label >{{ __('main.name') }}</label>
                  <input type="hidden" name="drive_id" id="drive_id">
                  <input list="driverslist" name="drives" id="drives" class="form-control" onchange="selectdriver(this.value)" required>
                <datalist id="driverslist">
                  @foreach (App\Drive::all() as $d)
                <option value="{{$d->id}}-{{ $d->driver->name }} - {{$d->truck->code}}">{{$d->driver->name}} - {{$d->truck->code}}</option>
                            @endforeach
                </datalist>
                </div>
          <div class="form-group">
            <label for="exampleInputEmail1" @cannot('choose_snduq') style="display: none;" @endif> {{ __("main.snduq") }}</label>
              <select class="form-control" name="snduq_id"  @cannot('choose_snduq') style="display: none;" @endif >
                  @foreach (App\Snduq::all() as $d)
                <option value="{{$d->id}}" @if($d->default==1) selected @endif>{{$d->name}}</option>
                            @endforeach
                </select>
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1"> {{ __("main.cost") }}</label>
              <input type="number" step="any" id="cost" name="cost" class="form-control" placeholder="{{ __('main.cost') }}" onblur="update('cost',this.value,{{ App\DollarPrice::orderby('created_at','desc')->first()->price }})">
          </div>
           <div class="form-group">
            <label for="exampleInputEmail1"> {{ __("main.dollar") }}</label>
              <input type="number" id="dollar" step="any" id="dollar" name="dollar" class="form-control" placeholder="{{ __('main.dollar') }}"  value="0" onblur="update('dollar',this.value,{{ App\DollarPrice::orderby('created_at','desc')->first()->price }})">
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1"> {{ __("main.invoice_no") }}</label>
              <input type="text" step="any" id="invoice_no" name="invoice_no" class="form-control" placeholder="{{ __('main.invoice_no') }}" >
          </div>
          
          <div class="form-group">
            <label for="exampleInputEmail1"> {{ __("main.note") }}</label>
              <input type="text" id="note" name="note" class="form-control" placeholder="{{ __('main.note') }}"  >
          </div>


          <button type="submit" class="btn btn-primary"> {{ __("main.add") }}</button>
        </form>
      </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default pull-left" data-dismiss="modal"> {{ __("main.cancel") }}</button>

          </div>
    </div>
  </div>
</div>
        </div>
    </div>
     <script type="text/javascript">
     	function update(field,val,dollar) {
     		// body...
     		if(field=='cost'){
			      document.getElementById('dollar').value = (val/dollar);
			  }
			  else if(field=='dollar')
			  {
			     document.getElementById('cost').value =(val*dollar);
			  }
     	}
             function selectdriver(id) {

$('#drive_id').val(id.split("-")[0]);
$('#driver').val(id.split("-")[1]);
}
          </script>
@stop