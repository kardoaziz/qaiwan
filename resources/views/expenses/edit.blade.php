@extends('adminlte::page')
@section('head_scripts')
    <!-- DataTables CSS -->
    {{-- {{ HTML::style('css/plugins/dataTables.bootstrap.css') }} --}}
@stop
@section('content')
<style type="text/css">
	td{
		text-align: center;
	}
	th {
		text-align: center !important;
	}
</style>


	 <div class="modal-content">
      <div class="modal-header">
        
        <h4 class="modal-title" align="center"><b> {{ __("main.update") }}  {{ __("main.expense") }}</b></h4>
      </div>
      <div class="modal-body" style="overflow-y: scroll;">
        <form action="{{ route('expenses.update',$expense->id) }}" method="POST" >
                {{csrf_field()}}
                  <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
<input type="hidden" name="_method" value="PUT">          

          <div class="form-group">
            <label for="exampleInputEmail1"> {{ __("main.date") }}</label>
              <input type="date" id="date" name="date" class="form-control" placeholder="date" required value="{{ $expense->date }}">
          </div>
           <div class="form-group">
                  <label >{{ __('main.name') }}</label>
                  <input type="hidden" name="drive_id" id="drive_id" value="{{ $expense->drive_id }}">
                  <input list="driverslist" name="drives" id="drives" class="form-control" onchange="selectdriver(this.value)" required value="{{ $expense->drive_id }}">
                <datalist id="driverslist">
                  @foreach (App\Drive::all() as $d)
                <option value="{{$d->id}}-{{ $d->driver->name }} - {{$d->truck->code}}">{{$d->driver->name}} - {{$d->truck->code}}</option>
                            @endforeach
                </datalist>
                </div>
          <div class="form-group">
            <label for="exampleInputEmail1" @cannot('choose_snduq') style="display: none;" @endif> {{ __("main.snduq") }}</label>
              <select class="form-control" name="snduq_id"  @cannot('choose_snduq') style="display: none;" @endif value="{{ $expense->snduq_id }}">
                  @foreach (App\Snduq::all() as $d)
                <option value="{{$d->id}}" @if($d->default==1) selected @endif>{{$d->name}}</option>
                            @endforeach
                </select>
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1"> {{ __("main.cost") }}</label>
              <input type="number" step="any" id="cost" name="cost" class="form-control" placeholder="{{ __('main.cost') }}" value="{{ $expense->cost }}" onblur="update('cost',this.value,{{ App\DollarPrice::orderby('created_at','desc')->first()->price }})">
          </div>
           <div class="form-group">
            <label for="exampleInputEmail1"> {{ __("main.dollar") }}</label>
              <input type="number" step="any" id="dollar" name="dollar" class="form-control" placeholder="{{ __('main.dollar') }}"  value="{{ $expense->dollar }}" onblur="update('dollar',this.value,{{ App\DollarPrice::orderby('created_at','desc')->first()->price }})">
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1"> {{ __("main.invoice_no") }}</label>
              <input type="text" step="any" id="invoice_no" name="invoice_no" class="form-control" placeholder="{{ __('main.invoice_no') }}" value="{{ $expense->invoice_no }}" >
          </div>
          
          <div class="form-group">
            <label for="exampleInputEmail1"> {{ __("main.note") }}</label>
              <input type="text" id="note" name="note" class="form-control" placeholder="{{ __('main.note') }}" value="{{ $expense->note }}" >
          </div>


          <button type="submit" class="btn btn-primary"> {{ __("main.add") }}</button>
        </form>
      </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default pull-left" data-dismiss="modal"> {{ __("main.cancel") }}</button>

          </div>
    </div>
<script type="text/javascript">
	function update(field,val,dollar) {
     		// body...
     		if(field=='cost'){
			      document.getElementById('dollar').value = (val/dollar);
			  }
			  else if(field=='dollar')
			  {
			     document.getElementById('cost').value =(val*dollar);
			  }
     	}
     	 function selectdriver(id) {

$('#drive_id').val(id.split("-")[0]);
$('#driver').val(id.split("-")[1]);
}
</script>
@stop