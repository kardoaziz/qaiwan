@extends('adminlte::page')
@section('head_scripts')
    <!-- DataTables CSS -->
    {{-- {{ HTML::style('css/plugins/dataTables.bootstrap.css') }} --}}
@stop
@section('content')
<style type="text/css">
  td{
    text-align: center;
  }
  th {
    text-align: center !important;
  }
</style>

<div class="card card-primary">
            <div class="card-header with-border text-center">
              {{ __('main.update payment') }}
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form role="form" action="{{ route('banzin_to_trucks.update',$payment->id) }}" method="POST">
            	    {{csrf_field()}}
                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
<input type="hidden" name="_method" value="PUT">
              <div class="card-body">
                <div class="form-group">
                  <label >{{ __('main.project') }}</label>
                  <input type="hidden" name="project_id" id="project_id" value="{{ $payment->project_id }}">

                  <select  name="projects" id="projects" class="form-control js-example-basic-single " onchange="selectdriver(this.value)" >
                {{-- <datalist id="projectslist"> --}}
                  @foreach (App\Projects::all() as $d)
                <option value="{{$d->id}}-{{ $d->name }}" @if($d->id==$payment->project_id) selected @endif >{{ $d->id }}-{{$d->name}} </option>
                            @endforeach
                </select>
                </div>


                <div class="form-group">
                  <input class="form-control" type="text" name="project" id="project" disabled value="{{ $payment->project->name }}">
                </div>
                {{-- <div class="form-group">
                  <label >{{ __('main.code') }}</label>
                  <input type="text" class="form-control"  placeholder="{{ __('main.code') }}" name="code">

                </div> --}}
                 <div class="form-group">
                  <label >{{ __('main.date') }}</label>
                  <input type="date" class="form-control"  placeholder="{{ __('main.date') }}" name="date" value="{{ $payment->date }}">

                </div>
                <div class="form-group">
                  <label >{{ __('main.plate') }}</label>
                  <input type="text" class="form-control"  placeholder="{{ __('main.plate') }}" name="plate" value="{{ $payment->plate }}">

                </div>
                 <div class="form-group">
                  <label >{{ __('main.name') }}</label>
                  <input type="text" class="form-control"  placeholder="{{ __('main.name') }}" name="name" value="{{ $payment->name }}">

                </div>
              {{-- </div> --}}
                 <div class="form-group">
                  <label >{{ __('main.amount') }}</label>
                  <input type="number" step="0.01" min="0" class="form-control"  placeholder="{{ __('main.amount') }}" name="amount" value="{{ $payment->qty }}">

                </div>
                <div class="form-group">
                  <label >{{ __('main.price') }}</label>
                  <input type="number" step="0.01" min="0" class="form-control"  placeholder="{{ __('main.price') }}" name="price" id="price" value="{{ $payment->price }}">
                 <input type="number" step="0.01" min="0" class="form-control"  value="{{ $payment->qty * $payment->price }}" name="total" id="total" readonly>

                </div>
                 <div class="form-group">
                  <label >{{ __('main.note') }}</label>
                  <input type="text" class="form-control"  placeholder="{{ __('main.note') }}" name="note" value="{{ $payment->note }}">

                </div>

              </div>
              <!-- /.box-body -->

              <div class="card-footer">
                <button type="submit" class="btn btn-success"><i class="fa fa-fw fa-save"></i>{{ __('main.save') }}</button>
              </div>
            </form>
          </div>
          <script type="text/javascript">
             function selectdriver(id) {

$('#project_id').val(id.split("-")[0]);
$('#project').val(id.split("-")[1]);
}
          </script>
          @stop
