@extends('adminlte::page')
@section('head_scripts')
    <!-- DataTables CSS -->
    {{-- {{ HTML::style('css/plugins/dataTables.bootstrap.css') }} --}}
@stop
@section('content')
<style type="text/css">
	td{
		text-align: center;
	}
	th {
		text-align: center !important;
	}
</style>

@php
    $purchaseAmount=App\banzinQtyPurchase::get()->sum("qty");
    $sellAmount=App\banzinQtyToTrucks::get()->sum("qty");
    $totalAmount=$purchaseAmount - $sellAmount;
    echo $totalAmount;
@endphp

<div class="card card-primary">
            <div class="card-header with-border text-center">
              {{ __('main.new banzin to trucks') }}
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form role="form" action="{{ route('banzin_to_trucks.store') }}" method="POST">
            	    {{csrf_field()}}
          <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
              <div class="card-body">
                <div class="form-group">
                  <label >{{ __('main.project') }}</label>
                  <input type="hidden" name="project_id" id="project_id">
                  <select  name="projects" id="projects" class="form-control js-example-basic-single" onchange="selectdriver(this.value)">
                {{-- <datalist id="projectslist"> --}}
                  @foreach (App\Projects::all() as $d)
                <option value="{{$d->id}}-{{ $d->name }}">{{ $d->id }}-{{$d->name}}</option>
                            @endforeach
                {{-- </datalist> --}}
                </select>
                </div>


                <div class="form-group">
                  <input class="form-control" type="text" name="project" id="project" disabled>
                </div>
                {{-- <div class="form-group">
                  <label >{{ __('main.code') }}</label>
                  <input type="text" class="form-control"  placeholder="{{ __('main.code') }}" name="code">

                </div> --}}
                 <div class="form-group">
                  <label >{{ __('main.date') }}</label>
                  <input type="date" class="form-control"  placeholder="{{ __('main.date') }}" name="date">

                </div>
                 <div class="form-group">
                  <label >{{ __('main.plate') }}</label>
                  <input type="text" class="form-control"  placeholder="{{ __('main.plate') }}" name="plate">

                </div>
                 <div class="form-group">
                  <label >{{ __('main.name') }}</label>
                  <input type="text" class="form-control"  placeholder="{{ __('main.name') }}" name="name">

                </div>
              {{-- </div> --}}
                 <div class="form-group">
                  <label >{{ __('main.amount') }}</label>
                  <input type="number" step="0.01" min="0" class="form-control" onchange="valChanged()"  placeholder="{{ __('main.amount') }}" name="amount" id="amount">
                 <input type="number" step="0.01" min="0" class="form-control"  value="{{$totalAmount}}" name="total" id="total" readonly>

                </div>
                <div class="form-group">
                  <label >{{ __('main.price') }}</label>
                  <input type="number" step="0.01" min="0" class="form-control"  placeholder="{{ __('main.price') }}" name="price" id="price">
                 <input type="number" step="0.01" min="0" class="form-control"  value="" name="total" id="total" readonly>

                </div>
                 <div class="form-group">
                  <label >{{ __('main.note') }}</label>
                  <input type="text" class="form-control"  placeholder="{{ __('main.note') }}" name="note">

                </div>

              </div>
              <!-- /.box-body -->

              <div class="card-footer">
                @if ($totalAmount<=0)
                <div class="alert alert-danger">
                    <p>{{__('main.banzin not available')}}</p>
                </div>
                @else
                <button type="submit" class="btn btn-success"><i class="fa fa-fw fa-save"></i>{{ __('main.save') }}</button>
                @endif
            </div>
            </form>
          </div>

          <script type="text/javascript">
             function selectdriver(id) {

                $('#project_id').val(id.split("-")[0]);
                $('#project').val(id.split("-")[1]);
                }
            function valChanged(){
              
                var amount=document.getElementById('amount').value;
                var total=document.getElementById('total').value;
                console.log(amount);
                console.log(total);
                if( amount > {{ $totalAmount }} ){
                    alert('ببورە ئەو بڕە بەردەست نییە ');
                    document.getElementById('amount').value='0';
                }
            }
          </script>
@stop
