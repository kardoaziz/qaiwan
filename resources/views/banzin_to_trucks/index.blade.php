@extends('adminlte::page')
@section('head_scripts')
    <!-- DataTables CSS -->
    {{-- {{ HTML::style('css/plugins/dataTables.bootstrap.css') }} --}}
@stop
@section('content')
<h3>{{ __('main.banzin to projects') }}</h3>
<div class="card card-light">
    <div class="card-header bg-dark">
        <div class="row">
        <div class="col-md-4  text-center"><a  href="{{ route('banzin_to_trucks.create') }}"><button type="button" class="btn btn-light"><i class="fa fa-fw fa-plus"></i>{{ __('main.new') }}</button></a>
        </div>
        <div class="col-md-4  text-center">
    <div class="small-box text-center bg-warning  " style="">
        @php
            $purchaseAmount=App\banzinQtyPurchase::get()->sum("qty");
            $sellAmount=App\banzinQtyToTrucks::get()->sum("qty");
            $totalAmount=$purchaseAmount - $sellAmount;
        @endphp

        <div class="inner">
            <h3 >{{ $totalAmount }}</h3>

          <p style="font-weight: bold;">{{ __('main.gass available') }}</p>
        </div>
        <div class="icon">
          <i class="fas fa-fw fa-fire"></i>
        </div>
       {{--  <a href="{{ route('gass_purchase.index') }}" class="small-box-footer">
          {{ __('main.visit') }} <i class="fa fa-arrow-circle-left"></i>
        </a> --}}
      </div>
</div></div>

    </div>
    <div class="card-body">
        <div class="table-responsive">
        <table id="example" class="table table-hover" >
                <thead class="table-light">
                    <tr>
                        <th>#</th>
                        <th>{{ __('main.project') }}</th>
                        <th>{{ __('main.plate') }}</th>
                        <th>{{ __('main.name') }}</th>
                        <th>{{ __('main.qty') }}</th>
                        <th>{{ __('main.price') }}</th>
                        <th>{{ __('main.total') }}</th>
                        <th>{{ __('main.date') }}</th>
                        <th>{{ __('main.note') }}</th>
                        <th>{{ __('main.edit') }}</th>
                        <th>{{ __('main.print') }}</th>
                        <th>{{ __('main.delete') }}</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach(App\banzinQtyToTrucks::all() as $d)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $d->project->name }}</td>
                        <td>{{ $d->plate }}</td>
                        <td>{{ $d->name }}</td>
                        <td>{{ $d->qty }}</td>
                        <td>{{ $d->price }}</td>
                        <td>{{ number_format($d->qty*$d->price) }}</td>
                        <td>{{ $d->date }}</td>
                        <td>{{ $d->note }}</td>
                        <td>
                            <form method="GET" action="{{ route('banzin_to_trucks.edit',$d->id) }}">
                                             {{ csrf_field() }}
  <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                             <div class="form-group">
                                                 <input type="submit" class="btn btn-info btn-sm " value="{{ __('main.edit') }}" >

                                             </div>
                                         </form></td>
                           <td>
                                            <form method="GET" action="{{ route('banzin_to_trucks.show',$d->id) }}">
                                                             {{ csrf_field() }}
  <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                                             <div class="form-group">
                                                                 <input type="submit" class="btn btn-info btn-sm " value="{{ __('main.print') }}" >

                                                             </div>
                                                         </form></td>
                        <td>
                                        <form action="{{ route('banzin_to_trucks.destroy',$d->id) }}" method="post">
                                 {{ csrf_field() }}
                                   <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                        <input type="hidden" name="_method" value="DELETE"  >

                            <a style="padding: 0px;
                              font-size: 12px;
                              color: red;" role="menuitem" tabindex="-1"  onclick="return confirm('{{ __('main.are you sure') }}')">
                              <button type="submit" class="btn btn-sm btn-danger"><i class="fa fa-fw fa-trash"></i></button>
                            </a>
                          </form>
                          </td>
                    </tr>
                     @endforeach
                </tbody>
            </table>
        </div>
        </div>
</div>
@stop
