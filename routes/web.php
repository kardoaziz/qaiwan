<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes(['register' => false]);
// Route::get('/', 'HomeController@index')->name('home');
Route::get('/', function () {
    if(Auth::check()){
        // if(Auth::user()->isPending() || Auth::user()->isDisabled()) {
            Auth::logout();
            return redirect('/login')->with('success', 'Please Login');
        // }
        //return view('welcome');
    }
    return redirect('/home');
});
Route::group(['middleware' => ['auth']], function() {
Route::resource('users', 'UserController');
Route::resource('roles', 'RoleController');

Route::resource('permissions', 'PermissionController');
Route::resource('drivers', 'DriverController');
Route::resource('trucks', 'TrucksController');
Route::resource('drives', 'DriveController');
Route::resource('items', 'ItemsController');
Route::resource('suppliers', 'SuppliersController');
Route::resource('warehouses', 'WarehouseController');
Route::resource('purchases', 'PurchaseController');
Route::resource('services', 'ServicesController');
Route::resource('checks', 'ChecksController');
Route::resource('snduq', 'SnduqController');
Route::resource('expenses', 'ExpensesController');
Route::resource('slfa', 'SlfaController');
Route::resource('purchase_items', 'PurchaseItemController');
Route::resource('service_items', 'ServiceItemsController');
Route::resource('check_items', 'CheckItemsController');
Route::resource('accounting','AccountingController');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/expired_license', 'DriverController@expired')->name('drivers.expired');
Route::get('/serviceouts/{id}', 'ServiceOutController@services')->name('serviceOut.services');
Route::post('/update/purchase_item', 'PurchaseItemController@updatee')->name('purchase_items.updatee');
Route::post('/trip/getPrice', 'TripController@getPrice')->name('trip.getprice');
Route::post('/trip/accounting', 'AccountingController@showaccounting')->name('accountant.showaccounting');
Route::post('/trip/finishaccounting', 'AccountingController@finishaccounting')->name('accountant.finishaccounting');
Route::post('/update/service_item', 'ServiceItemsController@updatee')->name('service_items.updatee');
Route::post('/update/service_out_item', 'ServiceOutItemsController@updatee')->name('service_out_items.updatee');
Route::post('/update/check_item', 'CheckItemsController@updatee')->name('check_items.updatee');
Route::get('/trucks/expired_reg/{id}', 'TrucksController@expired_registeration')->name('trucks.expired_registeration');
Route::get('/trucks/expired_permit/{id}', 'TrucksController@expired_permit')->name('trucks.expired_permit');
Route::get('/purchases/items/{id}', 'PurchaseController@items')->name('purchases.items');
Route::get('/services/items/{id}', 'ServicesController@items')->name('services.items');
Route::get('/serviceout/items/{id}', 'ServiceOutController@items')->name('serviceOut.items');
Route::get('/cheecks/items/{id}', 'ChecksController@items')->name('checks.items');
Route::get('/items/alert/{id}', 'ItemsController@alertItems')->name('items.alert');
Route::get('/items/projectparts/{id}', 'ItemsController@projectItems')->name('items.projectparts');
Route::resource('locations','LocationsController');
Route::resource('serviceOut','ServiceOutController');
Route::resource('service_out_items','ServiceOutItemsController');
Route::resource('projects','ProjectsController');
Route::resource('projectPayment','ProjectPaymentController');
Route::get('/projectPayments/{id}', 'ProjectPaymentController@payments')->name('projectpayments.payments');
Route::resource('fines','FinesController');
Route::resource('locationCost','LocationCostController');
Route::resource('materials','MaterialController');
Route::resource('expense_type','ExpenseTypeController');
Route::resource('trips','TripController');
Route::resource('transfers','BankTransferController');
Route::resource('dollars','DollarPriceController');
Route::resource('itemModel','ItemModelController');
Route::resource('itemSize','ItemSizeController');
Route::resource('drive_payments','DrivePaymentController');
Route::resource('gass_to_trucks','GassQtyToTrucksController');
Route::resource('gass_purchase','GassQtyPurchaseController');
Route::resource('banzin_to_trucks','BanzinQtyToTrucksController');
Route::resource('banzin_purchase','BanzinQtyPurchaseController');
Route::post('monthlyReport','ReportsController@monthly')->name('reports.monthly');
Route::post('transfer/updatestatus','BankTransferController@updatee')->name('transfers.updatee');
Route::get('monthlyReport','ReportsController@monthly')->name('reports.monthly');

Route::get('clear_cache', function () {

    \Artisan::call('migrate');
    \Artisan::call('cache:clear');

    dd("Cache is cleared");

});
});



