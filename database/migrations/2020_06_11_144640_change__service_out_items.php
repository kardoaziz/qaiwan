<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeServiceOutItems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('service_out_items', function (Blueprint $table) {
            //
            $table->renameColumn('dolar','dollar');
            $table->renameColumn('dolar_price','dollar_price');
        });
         Schema::table('service_outs', function (Blueprint $table) {
            //
            $table->renameColumn('dolar','dollar');
            $table->renameColumn('dolar_price','dollar_price');
        });
         Schema::table('project_payments', function (Blueprint $table) {
            //
            $table->renameColumn('dolar','dollar');
            $table->renameColumn('dolar_price','dollar_price');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
