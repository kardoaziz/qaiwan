<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeAmount extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        // Schema::table('drive_payments', function (Blueprint $table) {
        //     //
        //     $table->decimal('amount',12,2)->default(0)->change();
        // });
        Schema::table('expenses', function (Blueprint $table) {
            //
            $table->decimal('cost',12,2)->default(0)->change();
        });
         Schema::table('drives', function (Blueprint $table) {
            //
            $table->decimal('old_balance',12,2)->default(0)->change();
        });
         Schema::table('fines', function (Blueprint $table) {
            //
            $table->decimal('fine',12,2)->default(0)->change();
        });
          Schema::table('purchase_items', function (Blueprint $table) {
            //
            $table->decimal('price',12,2)->default(0)->change();
        });
          Schema::table('service_items', function (Blueprint $table) {
            //
            $table->decimal('price',12,2)->default(0)->change();
        });
           Schema::table('slfas', function (Blueprint $table) {
            //
            $table->decimal('amount',12,2)->default(0)->change();
        });
           Schema::table('snduqs', function (Blueprint $table) {
            //
            $table->decimal('initial_balance',12,2)->default(0)->change();
        });
           Schema::table('trip_expenses', function (Blueprint $table) {
            //
            $table->decimal('cost',12,2)->default(0)->change();
        });
            Schema::table('trip_fines', function (Blueprint $table) {
            //
            $table->decimal('amount',12,2)->default(0)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
