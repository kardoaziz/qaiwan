<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTripGassesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trip_gasses', function (Blueprint $table) {
            $table->id();
            $table->integer('trip_id');
            $table->string('invoice_no');
            $table->integer('qty');
            $table->integer('price');
            $table->dateTime('date');
            $table->string('location')->nullable();
            $table->longText('note')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trip_gasses');
    }
}
