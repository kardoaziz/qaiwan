<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBankTransfersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bank_transfers', function (Blueprint $table) {
            $table->id();
            $table->integer('bank_id_from');
            $table->integer('bank_id_to');
            $table->float('amount',12,10);
            $table->float('dollar',12,10);
            $table->decimal('dollar_price',8,2);
            $table->date('date');
            $table->date('recieved_date')->nullable();
            $table->string('status')->default("pending");
            $table->integer('note')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bank_transfers');
    }
}
