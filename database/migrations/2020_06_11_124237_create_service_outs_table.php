<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServiceOutsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('service_outs', function (Blueprint $table) {
            $table->id();
            $table->integer('id_project')->unsigned()->nullable();
            $table->date('date')->nullable();
            $table->float('fee')->nullable();
            $table->longText('note')->nullable();
            $table->text('plate')->nullable();
            $table->text('driver_name')->nullable();
            $table->text('car_type')->nullable();
            $table->longText('problem_type')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('service_outs');
    }
}
