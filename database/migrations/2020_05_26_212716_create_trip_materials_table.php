<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTripMaterialsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trip_materials', function (Blueprint $table) {
            $table->id();
            $table->integer('trip_id');
            $table->integer('material_id');
            $table->string('invoice_no');
            $table->integer('qty_barkrdn');
            $table->integer('qty_dagrtn');
            $table->dateTime('date_barkrdn');
            $table->dateTime('date_dagrtn');
            $table->longText('note')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trip_materials');
    }
}
