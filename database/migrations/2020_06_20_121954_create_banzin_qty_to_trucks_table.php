<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBanzinQtyToTrucksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('banzin_qty_to_trucks', function (Blueprint $table) {
            $table->id();
            $table->integer('project_id')->unsigned();
            $table->string('plate');
            $table->string('name')->nullable();
            $table->date('date')->nullable();
            $table->text('note')->nullable();
            $table->double('qty', 8, 2)->nullable()->default(0);
            $table->double('price', 8, 2)->nullable()->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('banzin_qty_to_trucks');
    }
}
