<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDollar extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('slfas', function (Blueprint $table) {
            //
            $table->decimal('dollar',12,2)->default(0);
            $table->decimal('dollar_price',12,2)->default(0);
        });
        Schema::table('snduqs', function (Blueprint $table) {
            //
            $table->decimal('dollar',12,2)->default(0);
            $table->decimal('dollar_price',12,2)->default(0);
        });
         Schema::table('drive_payments', function (Blueprint $table) {
            //
            $table->decimal('dollar',12,2)->default(0);
            $table->decimal('dollar_price',12,2)->default(0);
        });
         Schema::table('expenses', function (Blueprint $table) {
            //
            $table->decimal('dollar',12,2)->default(0);
            $table->decimal('dollar_price',12,2)->default(0);
        }); 
         
        Schema::table('trip_expenses', function (Blueprint $table) {
            //
            $table->decimal('dollar',12,2)->default(0);
            $table->decimal('dollar_price',12,2)->default(0);
        });
        Schema::table('trip_fines', function (Blueprint $table) {
            //
            $table->decimal('dollar',12,2)->default(0);
            $table->decimal('dollar_price',12,2)->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
