<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeDecimals extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('expenses', function (Blueprint $table) {
            //
            $table->float('cost',12,8)->default(0)->nullable()->change();
            $table->float('dollar',12,8)->default(0)->nullable()->change();
            // $table->float('dollar',12,8)->default(0)->nullable()->change();
        });
       
        Schema::table('drive_payments', function (Blueprint $table) {
            //
            $table->float('amount',12,8)->default(0)->nullable()->change();
            $table->float('dollar',12,8)->default(0)->nullable()->change();
            // $table->float('dollar',12,8)->default(0)->nullable()->change();
        });
       
        
        
           Schema::table('slfas', function (Blueprint $table) {
            //
            $table->float('amount',12,8)->default(0)->nullable()->change();
            $table->float('dollar',12,8)->default(0)->nullable()->change();
        });
           Schema::table('snduqs', function (Blueprint $table) {
            //
            $table->float('initial_balance',12,8)->default(0)->nullable()->change();
        });
           
           
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
