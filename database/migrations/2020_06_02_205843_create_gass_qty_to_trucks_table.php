<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGassQtyToTrucksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gass_qty_to_trucks', function (Blueprint $table) {
            $table->id();
            $table->integer('drive_id')->unsigned();
            $table->date('date')->nullable();
            $table->text('note')->nullable();
            $table->double('qty', 8, 2)->nullable()->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gass_qty_to_trucks');
    }
}
