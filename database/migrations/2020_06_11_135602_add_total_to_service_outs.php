<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTotalToServiceOuts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('service_outs', function (Blueprint $table) {
            $table->float('total')->nullable();
            $table->float('dolar_price')->nullable();
            $table->float('dolar')->nullable();
            $table->float('dolar_fee')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('service_outs', function (Blueprint $table) {
            //
        });
    }
}
